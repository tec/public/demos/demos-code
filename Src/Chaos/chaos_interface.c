/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Chaos: Interface
 *
 * This file contains the implementation of the interface of Chaos.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static chaos_state_t chaos_state;

static uint8_t       internal_modulation = CHAOS_INTERFACE_MODULATION; // Internal state for the radio modulation
static int8_t        internal_power      = CHAOS_INTERFACE_POWER;      // Internal state for power (can be adapted from the upper layer)
static uint8_t       internal_band       = CHAOS_INTERFACE_RF_BAND;    // Internal state for the frequency band (can be adapted from the upper layer)


/* Functions -----------------------------------------------------------------*/

void chaos_start(uint8_t*               payload,
                 uint8_t                size,
                 chaos_merge_fn_t       merge_fn,
                 chaos_termination_fn_t termination_fn,
                 chaos_filter_fn_t      filter_fn,
                 chaos_initiate_type_t  initiate_type,
                 uint64_t               starttime_hs,
                 uint64_t               endtime_hs,
                 bool                   include_timestamp,
                 chaos_timesync_type_t  update_starttime,
                 bool                   update_hstimer_offset)
{
    // Wake up radio in case it is in sleep mode
    if (radio_wakeup()) {
        LOG_WARNING("Radio was in sleep mode");
    }

    // Check requested services
    if (update_hstimer_offset && (update_starttime == CHAOS_TIMESYNC_NONE) ) {
        LOG_WARNING("Hstimer offset will not be updated because update_starttime is not set");
        update_hstimer_offset = false;
    }
    if (update_hstimer_offset && !include_timestamp) {
        LOG_WARNING("Cannot update hstimer offset without timestamp");
        update_hstimer_offset = false;
    }

    // Check inputs
    if (size > CHAOS_MAX_PAYLOAD_SIZE(include_timestamp)) {
        size = CHAOS_MAX_PAYLOAD_SIZE(include_timestamp);
        LOG_ERROR("Pkt len too big -> pkt will be truncated");
    }
    if (!payload)        { LOG_ERROR("Payload is zero"); }
    if (!merge_fn)       { LOG_ERROR("Merging function not given"); }
    if (!termination_fn) { LOG_ERROR("Termination function not given"); }

    // Disable interrupts which might cause issues with the timing
#if CHAOS_DISABLE_INTERRUPTS
    HAL_SuspendTick();
    HAL_NVIC_DisableIRQ(TIM1_UP_TIM16_IRQn);
    SUSPEND_SYSTICK();
#endif /* CHAOS_DISABLE_INTERRUPTS */

    // Initialise the state
    memset(&chaos_state, 0, sizeof(chaos_state_t));
    chaos_state.payload                    = payload;
    chaos_state.size                       = size;
    chaos_state.merge_fn                   = merge_fn;
    chaos_state.termination_fn             = termination_fn;
    chaos_state.filter_fn                  = filter_fn;
    chaos_state.initiate_type              = initiate_type;
    chaos_state.starttime_hs               = starttime_hs;
    chaos_state.endtime_hs                 = endtime_hs;
    chaos_state.include_timestamp          = include_timestamp;
    chaos_state.update_starttime           = update_starttime;
    chaos_state.update_hstimer_offset      = update_hstimer_offset;
    chaos_state.first_message_received     = false;
    chaos_state.message_slot_count         = 0;
    chaos_state.result                     = CHAOS_PACKET_NOT_COMPLETE;
    chaos_state.packets_differ             = false;
    chaos_state.tx_slot_counter            = 0;
    chaos_state.timeout_counter            = CHAOS_TIMEOUT + (random_rand32() % CHAOS_TIMEOUT_WINDOW);
    chaos_state.no_update_slot_counter     = 0;
    chaos_state.nr_tx_success              = 0;
    chaos_state.nr_tx_timeout              = 0;
    chaos_state.nr_rx_done                 = 0;
    chaos_state.nr_rx_crc_error            = 0;
    chaos_state.nr_rx_success              = 0;
    chaos_state.nr_rx_timeout              = 0;
    chaos_state.incomplete_packet_received = false;
    chaos_state.stop                       = false;
    chaos_state.stopped                    = false;

    // Initialise the radio
#if !CHAOS_RADIO_USE_SLEEP
    chaos_radio_init(&chaos_state);
#endif

    // Prepare the first message slot
    chaos_prepare_first_message_slot(&chaos_state);
}

bool chaos_stop()
{
    // Tell Chaos to not schedule the next slot
    if (!chaos_state.stopped) {
        chaos_state.stop = true;

        // Wait until Chaos finished the current slot
        while (!chaos_state.stopped) {}
    }

    // Stop the timers used to schedule transmissions and timeouts
    hs_timer_schedule_stop();
    hs_timer_timeout_stop();

    // Reset the radio callbacks
    radio_set_rx_callback(NULL);
    radio_set_tx_callback(NULL);
    radio_set_timeout_callback(NULL);

    // Put the radio in standby mode
#if CHAOS_RADIO_USE_SLEEP
    radio_sleep(CHAOS_RADIO_SLEEP_WARM);
#else
    radio_standby();
#endif

    // Re-enable interrupts
#if CHAOS_DISABLE_INTERRUPTS
    RESUME_SYSTICK();
    HAL_NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
    HAL_ResumeTick();
#endif /* CHAOS_DISABLE_INTERRUPTS */

    // Prevent division by zero when printing stats
    uint16_t tot_rx = chaos_state.nr_rx_success + chaos_state.nr_rx_timeout; // CRC error restarts reception or directly calls a timeout
    uint16_t tot_tx = chaos_state.nr_tx_success + chaos_state.nr_tx_timeout;
    if (tot_rx == 0) { tot_rx = 1; }
    if (tot_tx == 0) { tot_tx = 1; }
    LOG_VERBOSE("Rcvd in %2hhu slots (%3u%%) and sent in %2hhu slots (%3u%%)",
                chaos_state.nr_rx_success, (uint16_t)chaos_state.nr_rx_success * 100 / tot_rx,
                chaos_state.nr_tx_success, (uint16_t)chaos_state.nr_tx_success * 100 / tot_tx);

    return chaos_state.result != CHAOS_PACKET_NOT_COMPLETE;
}

inline void chaos_set_tx_power(int8_t power)
{
    if ( (RADIO_MIN_POWER <= power) && (power <= RADIO_MAX_POWER) ) {
        internal_power = power;
    } else {
        LOG_WARNING("Ignored invalid TX power %hhi", power);
    }
}

inline int8_t chaos_get_tx_power()
{
    return internal_power;
}

inline void chaos_set_modulation(uint8_t modulation)
{
    if (modulation < RADIO_NUM_MODULATIONS) {
        internal_modulation = modulation;
    } else {
        LOG_WARNING("Ignored invalid modulation %hhu", modulation);
    }
}

inline uint8_t chaos_get_modulation()
{
    return internal_modulation;
}

inline void chaos_set_band(uint8_t band)
{
    if (band < RADIO_NUM_BANDS) {
        internal_band = band;
    } else {
        LOG_WARNING("Ignored invalid frequency band %hhu", band);
    }
}

inline uint8_t chaos_get_band()
{
    return internal_band;
}

inline bool chaos_has_transmitted()
{
    return (chaos_state.nr_tx_success + chaos_state.nr_tx_timeout) > 0;
}
