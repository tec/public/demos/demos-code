/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Chaos: Interface
 *
 *  This file contains the tunable constants and the definition of the interface of Chaos.
 *  Only the functions defined in this file should be used to interact with Chaos.
 *
 */

#ifndef __CHAOS_INTERFACE_H
#define __CHAOS_INTERFACE_H


/* --- definitions --- */

/* Number of message slots a node should keep listening in the KEEP_LISTENING state without receiving new information before terminating.
 * Must be bigger than 0. */
#ifndef CHAOS_KEEP_LISTENING_TIMEOUT
#define CHAOS_KEEP_LISTENING_TIMEOUT            10
#endif

/* The node will retransmit its message if it did not receive a message for a certain number of slots.
 * This number is randomly chosen in [CHAOS_TIMEOUT, CHAOS_TIMEOUT + CHAOS_TIMEOUT_WINDOW - 1]. */
#ifndef CHAOS_TIMEOUT
#define CHAOS_TIMEOUT                           3
#endif
#ifndef CHAOS_TIMEOUT_WINDOW
#define CHAOS_TIMEOUT_WINDOW                    3
#endif

/* Defines the time interval in which messages are sent / received.
 * Must be constant among all nodes.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_MESSAGE_INTERVAL_HS
#define CHAOS_MESSAGE_INTERVAL_HS               HS_TIMER_MS_TO_TICKS(100)
#endif

/* Time reserved to prepare the radio for the next slot.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_RADIO_TIME_BUFFER_HS
#define CHAOS_RADIO_TIME_BUFFER_HS              HS_TIMER_MS_TO_TICKS(1)
#endif

/* Defines how much earlier a node should start listening.
 * Must be smaller than CHAOS_RADIO_TIME_BUFFER_HS.
 * CHAOS_RADIO_TIME_BUFFER_HS - CHAOS_RADIO_RECEIVE_OFFSET_HS needs to be long enough to prepare the radio for receiving a message.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_RADIO_RECEIVE_OFFSET_HS
#define CHAOS_RADIO_RECEIVE_OFFSET_HS           1
#endif

/* Defines how long a node should listen for a Chaos message before giving up.
 * Must be smaller than CHAOS_MESSAGE_INTERVAL_HS.
 * CHAOS_MESSAGE_INTERVAL_HS - CHAOS_RADIO_RECEIVE_TIMEOUT_HS needs to be long enough to complete the packet processing and to prepare for the next slot.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_RADIO_RECEIVE_TIMEOUT_HS
#define CHAOS_RADIO_RECEIVE_TIMEOUT_HS          HS_TIMER_MS_TO_TICKS(10)
#endif

/* Defines how long a node should listen if it is not initiating the flood and if the starttime should be adapted using the received packets.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_RADIO_RECEIVE_TIMEOUT_LONG_HS
#define CHAOS_RADIO_RECEIVE_TIMEOUT_LONG_HS     HS_TIMER_MS_TO_TICKS(400)
#endif

/* Minimum time required to restart listening after having received a message that is rejected.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_RADIO_MIN_RECEIVE_TIME_HS
#define CHAOS_RADIO_MIN_RECEIVE_TIME_HS         HS_TIMER_MS_TO_TICKS(65)
#endif

/* Determines the radio modulation, transmit power (in dBm), and frequency band that are used for sending messages.
 * See radio_constants.c and radio_set_config_tx in radio_helpers.c */
#ifndef CHAOS_INTERFACE_MODULATION
#define CHAOS_INTERFACE_MODULATION              7
#endif
#ifndef CHAOS_INTERFACE_POWER
#define CHAOS_INTERFACE_POWER                   0
#endif
#ifndef CHAOS_INTERFACE_RF_BAND
#define CHAOS_INTERFACE_RF_BAND                 RADIO_DEFAULT_BAND
#endif

/* Whether the radio will be put into sleep mode between the messages */
#ifndef CHAOS_RADIO_USE_SLEEP
#define CHAOS_RADIO_USE_SLEEP                   0
#endif

/* If CHAOS_RADIO_USE_SLEEP is 1:
 * The radio will be put into warm sleep mode if set, otherwise cold sleep will be used. */
#ifndef CHAOS_RADIO_SLEEP_WARM
#define CHAOS_RADIO_SLEEP_WARM                  true
#endif

/* Number of times to retransmit a packet after it is complete */
#ifndef CHAOS_NR_TRANSMIT_AFTER_CHAOS_COMPLETE
#define CHAOS_NR_TRANSMIT_AFTER_CHAOS_COMPLETE  5
#endif

/* If initiate_type == CHAOS_INITIATE_WITH_OTHERS:
 * Probability, with which a node sends in the first message slot.
 * (Unit: percent) */
#ifndef CHAOS_INITIATE_PROBABILITY
#define CHAOS_INITIATE_PROBABILITY              25
#endif

/* Whether Chaos will print some debug messages during the operation.
 * LOG_LEVEL needs to be set to LOG_LEVEL_VERBOSE. */
#ifndef CHAOS_DEBUG_PRINTS
#define CHAOS_DEBUG_PRINTS                      0
#endif

/* Maximum amount the starttime is increased by when update_starttime is set.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_MAX_CLOCK_CORRECTION_BACKWARDS_HS
#define CHAOS_MAX_CLOCK_CORRECTION_BACKWARDS_HS HS_TIMER_US_TO_TICKS(0)
#endif

/* Maximum amount the starttime is decreased by when update_starttime is set.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_MAX_CLOCK_CORRECTION_FORWARD_HS
#define CHAOS_MAX_CLOCK_CORRECTION_FORWARD_HS   HS_TIMER_US_TO_TICKS(40)
#endif

/* Checks whether the given clock change is within [-CHAOS_MAX_CLOCK_CORRECTION_BACKWARDS_HS, CHAOS_MAX_CLOCK_CORRECTION_FORWARD_HS] */
#define CHAOS_CHECK_CLOCK_CORRECTION(change_hs) ( ((change_hs) > - CHAOS_MAX_CLOCK_CORRECTION_BACKWARDS_HS) && ((change_hs) < CHAOS_MAX_CLOCK_CORRECTION_FORWARD_HS) )

/* Every Chaos packet is sent CHAOS_HSTIMER_TRIGGER_DELAY_HS hstimer ticks earlier than stated by the timestamp in the packet.
 * This compensates the delay between the hstimer interrupt and the triggering of the NSS pin.
 * See gloria_constants.h (specifically the description of GLORIA_HSTIMER_TRIGGER_DELAY) for further information.
 * (Unit: hstimer ticks) */
#ifndef CHAOS_HSTIMER_TRIGGER_DELAY_HS
#define CHAOS_HSTIMER_TRIGGER_DELAY_HS          36
#endif

/* Whether some interrupts are disabled which might cause timing issues */
#ifndef CHAOS_DISABLE_INTERRUPTS
#define CHAOS_DISABLE_INTERRUPTS                1
#endif

/* Decouples Chaos and Gloria. */
#define CHAOS_TX_SYNC(modulation)               gloria_timings[modulation].txSync


/* --- function prototypes --- */

/* Starts Chaos. If several nodes initiate Chaos together, their start times need to be synchronized.
 *   payload:               Pointer to the payload.
 *                          The buffer containing it must have a size of at least the maximum packet size value returned by merge_fn.
 *   size:                  Current size of the payload.
 *                          The maximum supported payload size is CHAOS_MAX_PAYLOAD_SIZE(has_timestamp).
 *   merge_fn:              Function used to merge two packets.
 *   termination_fn:        Function indicating whether a packet is complete.
 *   filter_fn:             Function used to filter out packets.
 *                          Can be set to NULL if not used.
 *   initiate_type:         Initiation strategy for Chaos.
 *   starttime_hs:          Exact time when to start sending. If 0: start immediately. (Unit: hstimer ticks)
 *   endtime_hs:            Time until when Chaos should run. (Unit: hstimer ticks)
 *   include_timestamp:     Indicates whether a timestamp should be appended to the end of the header.
 *                          The timestamp indicates the sending time of a packet. (Unit: hstimer ticks)
 *   update_starttime:      Indicates whether the starttime should be updated using the received packets.
 *   update_hstimer_offset: Indicates whether the offset of the hstimer should be adapted using timestamps included in the received packets (if they contain a timestamp).
 *                          This only has an effect if update_starttime is set.
 */
void    chaos_start(uint8_t*            payload,
                 uint8_t                size,
                 chaos_merge_fn_t       merge_fn,
                 chaos_termination_fn_t termination_fn,
                 chaos_filter_fn_t      filter_fn,
                 chaos_initiate_type_t  initiate_type,
                 uint64_t               starttime_hs,
                 uint64_t               endtime_hs,
                 bool                   include_timestamp,
                 chaos_timesync_type_t  update_starttime,
                 bool                   update_hstimer_offset);

/* Stop Chaos if it did not terminate itself.
 *
 *   Return value: True if the resulting packet is complete (according to termination_fn). */
bool    chaos_stop();

/* Sets the transmission power of the radio (in dBm). */
void    chaos_set_tx_power(int8_t power);

int8_t  chaos_get_tx_power();

/* Sets the modulation configuration of the radio.
 *   modulation:    Flora modulation option (see radio_constants.c)
 */
void    chaos_set_modulation(uint8_t modulation);

/* Returns the currently used modulation (valid options are defined in radio_constants.c). */
uint8_t chaos_get_modulation();

/* Sets the frequency and bandwidth config of the radio (valid options are defined in radio_constants.c)
 *   band:          Flora band (see radio_constants.c)
 */
void    chaos_set_band(uint8_t band);

/* Returns the currently used frequency band. (Unit: Flora band options as defined in radio_constants.c) */
uint8_t chaos_get_band();

/* Returns whether the node has already transmitted during the current Chaos round. */
bool    chaos_has_transmitted();


/*** Parameter Checks **************************************************/

#if CHAOS_KEEP_LISTENING_TIMEOUT <= 0
#error "CHAOS_KEEP_LISTENING_TIMEOUT must be bigger than 0"
#endif

#if CHAOS_DEBUG_PRINTS && (LOG_LEVEL > LOG_LEVEL_VERBOSE)
#error "Cannot print extra information as log level is too high"
#endif

#if CHAOS_HSTIMER_TRIGGER_DELAY_HS != GLORIA_HSTIMER_TRIGGER_DELAY
#error "Update CHAOS_HSTIMER_TRIGGER_DELAY_HS to correct value from Flora"
#endif

#endif /* __CHAOS_INTERFACE_H */
