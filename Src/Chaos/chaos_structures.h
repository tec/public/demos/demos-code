/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Chaos: Structures
 *
 * This file contains the definitions of the types and structures used in Chaos.
 *
 */

#ifndef __CHAOS_STRUCTURES_H
#define __CHAOS_STRUCTURES_H


/* --- definitions --- */

/* Calculate the maximum payload size. has_timestamp needs to be boolean. */
#define CHAOS_MAX_PAYLOAD_SIZE(has_timestamp)   (255 - ( (has_timestamp) ? CHAOS_HEADER_LENGTH_WITH_TS : CHAOS_HEADER_LENGTH))
#define CHAOS_HEADER_LENGTH_WITH_TS             (CHAOS_HEADER_LENGTH + CHAOS_TIMESTAMP_LENGTH)                                 // length of the Chaos header, with timestamp
#define CHAOS_HEADER_LENGTH                     2                                                                              // length of the Chaos header
#define CHAOS_TIMESTAMP_LENGTH                  8                                                                              // length of the timestamp to send with sync floods


/* --- typedefs --- */

/* Enum indicating the state of a packet.
 * Note: In the last two states, the chaos_termination_fn is not executed anymore.
 *   CHAOS_PACKET_NOT_COMPLETE:                   Not all nodes have yet contributed to the packet.
 *   CHAOS_PACKET_COMPLETE:                       All nodes contributed to the packet.
 *   CHAOS_PACKET_COMPLETE_BUT_KEEP_LISTENING:    All known nodes contributed to the packet, but there might be additional unknown nodes which did not yet contribute.
 *                                                The final packet is retransmitted if the node receives a packet which is not yet complete.
 *   CHAOS_PACKET_COMPLETE_BUT_KEEP_TRANSMITTING: The packet is complete but the node keeps sending and receiving as if the packet would not yet be complete.
 */
typedef enum {
    CHAOS_PACKET_NOT_COMPLETE,
    CHAOS_PACKET_COMPLETE,
    CHAOS_PACKET_COMPLETE_BUT_KEEP_LISTENING,
    CHAOS_PACKET_COMPLETE_BUT_KEEP_TRANSMITTING
} chaos_termination_result_t;

/* Enum indicating how Chaos should be started.
 *   CHAOS_INITIATE_ALONE:                        Always transmit in the first slot.
 *   CHAOS_INITIATE_WITH_OTHERS:                  Transmit with a given probability.
 *   CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT: First wait for others to start or until timeout expires, thereafter join others.
 *   CHAOS_DO_NOT_INITIATE:                       Wait for others to start.
 */
typedef enum {
    CHAOS_INITIATE_ALONE,
    CHAOS_INITIATE_WITH_OTHERS,
    CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT,
    CHAOS_DO_NOT_INITIATE,
} chaos_initiate_type_t;

/* Enum indicating how Chaos should adjust the startup time upon receiving a packet.
 *   CHAOS_TIMESYNC_NONE:    Do not adjust the startup time.
 *   CHAOS_TIMESYNC_BOUNDED: Adjust the startup time only if the change is
 *                           within [-CHAOS_MAX_CLOCK_CORRECTION_BACKWARDS_HS, CHAOS_MAX_CLOCK_CORRECTION_FORWARDS_HS].
 *   CHAOS_TIMESYNC_FULL:    Adjust the startup time.
 */
typedef enum {
    CHAOS_TIMESYNC_NONE,
    CHAOS_TIMESYNC_BOUNDED,
    CHAOS_TIMESYNC_FULL,
} chaos_timesync_type_t;

/* Used by Chaos to merge a local with a received packet.
 *   payload:        Pointer to the payload of the local packet
 *   size:           Size of the payload of the local packet
 *   payload_rx:     Pointer to the payload of the received packet
 *   size_rx:        Size of the payload of the received packet
 *
 *   Return value: True if the local packet should be transmitted in the next slot (e.g. if the two packets are not identical).
 *
 * The resulting packet is written to *payload. It shall not be bigger than:
 *   CHAOS_MAX_PAYLOAD_SIZE(1) if a timestamp is     included in the packet
 *   CHAOS_MAX_PAYLOAD_SIZE(0) if a timestamp is not included in the packet
 */
typedef bool (*chaos_merge_fn_t)(uint8_t* payload, uint8_t size, const uint8_t* payload_rx, uint8_t size_rx);

/* Used by Chaos to determine whether the packet is complete.
 *   payload: Pointer to the payload of the packet
 *   size:    Size of the payload
 *
 *   Return value: A chaos_termination_result_t indicating whether the packet is complete.
 *
 * Note: If the function returns CHAOS_PACKET_COMPLETE_BUT_KEEP_LISTENING,
 *       then the function will also be called with the received packet as an argument.
 *       Therefore, one should be careful when using internal state in this function. */
typedef chaos_termination_result_t (*chaos_termination_fn_t)(const uint8_t* payload, uint8_t size);

/* Used to filter out Chaos packets. Filtered packets are not used to update the starttime.
 *   packet:    Pointer to the local packet including header and timestamp (if present)
 *   size:      Size of the local packet including header and timestamp (if present)
 *   packet_rx: Pointer to the received packet
 *   size_rx:   Size of the received packet
 *
 *   Return value: True if the packet should be kept. */
typedef bool (*chaos_filter_fn_t)(const uint8_t* packet, uint8_t size, const uint8_t* packet_rx, uint8_t size_rx);


/* Header of a Chaos packet. */
typedef struct __attribute__((__packed__, __aligned__(1))){
    uint8_t protocol_id : 4;
    uint8_t type        : 3;
    uint8_t sync        : 1;
    uint8_t message_slot_count;
    uint8_t timestamp[CHAOS_TIMESTAMP_LENGTH];
} chaos_header_t;

/* Struct containing the state of Chaos. */
typedef struct {
    // Variables given at the start
    uint8_t*                   payload;
    uint8_t                    size;
    chaos_merge_fn_t           merge_fn;
    chaos_termination_fn_t     termination_fn;
    chaos_filter_fn_t          filter_fn;
    chaos_initiate_type_t      initiate_type;
    uint64_t                   starttime_hs;
    uint64_t                   endtime_hs;
    bool                       include_timestamp;
    chaos_timesync_type_t      update_starttime;
    bool                       update_hstimer_offset;
    // Bool indicating whether a message was received
    bool                       first_message_received;
    // Number of the current message slot
    uint8_t                    message_slot_count;
    // Termination result of the current packet
    chaos_termination_result_t result;
    // Bool indicating whether a packet different from the local one was received in the last RX slot
    bool                       packets_differ;
    // Number of consecutive message slots in which a message was sent
    uint8_t                    tx_slot_counter;
    // Number of consecutive RX message slots in which no packet different from the local one was received
    uint8_t                    no_update_slot_counter;
    // Counter indicating how many slots to wait before retransmitting the message
    uint8_t                    timeout_counter;
    // Bool indicating whether an incomplete packet was received in the last RX slot.
    // Only used if result == CHAOS_PACKET_COMPLETE_BUT_KEEP_LISTENING.
    bool                       incomplete_packet_received;
    // Bool indicating that Chaos should stop
    bool                       stop;
    // Bool indicating that Chaos stopped
    volatile bool              stopped;
    uint8_t                    nr_tx_success;
    uint8_t                    nr_tx_timeout;
    uint8_t                    nr_rx_done;
    uint8_t                    nr_rx_crc_error;
    uint8_t                    nr_rx_success;
    uint8_t                    nr_rx_timeout;
} chaos_state_t;

#endif /* __CHAOS_STRUCTURES_H */
