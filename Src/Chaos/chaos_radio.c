/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Chaos: Radio
 *
 * This file contains the functions which interact with the radio driver.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static chaos_state_t* chaos_state;
static chaos_header_t chaos_header;
static uint64_t       slot_start_timestamp_hs;


/* Static functions ----------------------------------------------------------*/

/* Pointers to the callback functions specified by chaos_tx() and chaos_rx() */
static void (*finished_tx_callback)();
static void (*finished_rx_callback)(bool received, uint8_t* packet, uint8_t size);

static void chaos_radio_rx_callback(uint8_t* payload, uint16_t size, int16_t rssi, int8_t snr, bool crc_error);


/* Functions -----------------------------------------------------------------*/

/* This function gets executed upon an RX timeout. */
void chaos_radio_rx_timeout_callback(bool crc_error)
{
#if CHAOS_DEBUG_PRINTS
    LOG_VERBOSE("Chaos: RX timeout");
#endif /* CHAOS_DEBUG_PRINTS */

    // Workaround: Sometimes, after a TX finished callback, a timeout interrupt is fired. To prevent such an interrupt from stopping the receive slot before it even started, the receive slot is restarted
    if (hs_timer_get_current_timestamp() < (slot_start_timestamp_hs - CHAOS_RADIO_RECEIVE_OFFSET_HS) ) {
        LOG_VERBOSE("RX timeout ignored");
        radio_set_timeout_callback(chaos_radio_rx_timeout_callback);
        radio_set_rx_callback(chaos_radio_rx_callback);

        if (!chaos_state->first_message_received                                         &&
            (chaos_state->initiate_type == CHAOS_DO_NOT_INITIATE                      ||
             chaos_state->initiate_type == CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT  )  ) {
            radio_receive_scheduled(slot_start_timestamp_hs - CHAOS_RADIO_RECEIVE_OFFSET_HS, MIN(CHAOS_RADIO_RECEIVE_TIMEOUT_LONG_HS, chaos_state->endtime_hs - hs_timer_get_current_timestamp() ) );
        } else {
            radio_receive_scheduled(slot_start_timestamp_hs - CHAOS_RADIO_RECEIVE_OFFSET_HS, CHAOS_RADIO_RECEIVE_TIMEOUT_HS);
        }
        return;
    }

#if CHAOS_RADIO_USE_SLEEP
    radio_sleep(CHAOS_RADIO_SLEEP_WARM);
#else
    //radio_standby();
#endif

    chaos_state->nr_rx_timeout++;
    radio_set_timeout_callback(NULL);
    finished_rx_callback(false, NULL, 0);
}

/* This function gets executed upon a TX timeout. */
void chaos_radio_tx_timeout_callback(bool crc_error)
{
#if CHAOS_DEBUG_PRINTS
    LOG_ERROR("Chaos: TX timeout");
#endif /* CHAOS_DEBUG_PRINTS */

#if CHAOS_RADIO_USE_SLEEP
    radio_sleep(CHAOS_RADIO_SLEEP_WARM);
#else
    //radio_standby();
#endif

    chaos_state->nr_tx_timeout++;
    finished_tx_callback();
}

/* Continue listening for messages. */
void chaos_radio_keep_listening()
{
    uint64_t end_timestamp_hs;
    if (!chaos_state->first_message_received                                         &&
        (chaos_state->initiate_type == CHAOS_DO_NOT_INITIATE                      ||
         chaos_state->initiate_type == CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT  )  ) {
        // Listen for a longer time in case the node is out-of-sync with the initiating node
        end_timestamp_hs = MIN(slot_start_timestamp_hs + CHAOS_RADIO_RECEIVE_TIMEOUT_LONG_HS, chaos_state->endtime_hs);
    } else {
        end_timestamp_hs = slot_start_timestamp_hs + CHAOS_RADIO_RECEIVE_TIMEOUT_HS;
    }

    if ( (hs_timer_get_current_timestamp() + CHAOS_RADIO_MIN_RECEIVE_TIME_HS) < end_timestamp_hs) {
        radio_set_rx_callback(chaos_radio_rx_callback);
        radio_set_timeout_callback(chaos_radio_rx_timeout_callback);
        radio_receive(end_timestamp_hs - hs_timer_get_current_timestamp());
    } else {
        chaos_radio_rx_timeout_callback(false);
    }
}

/* This function gets executed after having received a message. */
void chaos_radio_rx_callback(uint8_t* payload, uint16_t size, int16_t rssi, int8_t snr, bool crc_error)
{
    chaos_state->nr_rx_done++;

    chaos_header_t* hdr     = (chaos_header_t*)payload;
    uint8_t         hdr_len = hdr->sync ? CHAOS_HEADER_LENGTH_WITH_TS : CHAOS_HEADER_LENGTH;

    // Filter out invalid packets
    if (crc_error || (size < hdr_len) ) {
#if CHAOS_DEBUG_PRINTS
        LOG_WARNING("Invalid pkt discarded (CRC or len %hhu incorrect)", size);
#endif /* CHAOS_DEBUG_PRINTS */

        chaos_state->nr_rx_crc_error++;
        chaos_radio_keep_listening();
        return;
    } else if (!PROTOCOL_CHECK_ID(payload[0], CHAOS)                                                                      ||
               (chaos_state->filter_fn && !chaos_state->filter_fn(chaos_state->payload, chaos_state->size, payload, size))  ) {
#if CHAOS_DEBUG_PRINTS
        LOG_VERBOSE("Pkt filtered");
#endif /* CHAOS_DEBUG_PRINTS */

        chaos_radio_keep_listening();
        return;
    }

#if CHAOS_RADIO_USE_SLEEP
    radio_sleep(CHAOS_RADIO_SLEEP_WARM);
#else
    //radio_standby();
#endif

    // Update the starttime and the hstimer offset if necessary
    if ( (chaos_state->update_starttime != CHAOS_TIMESYNC_NONE) && (chaos_state->result == CHAOS_PACKET_NOT_COMPLETE) ) {
        // Determine TX time according to own clock
        uint64_t rx_time_hs = radio_get_last_sync_timestamp() - CHAOS_TX_SYNC(chaos_get_modulation());

        if (chaos_state->update_hstimer_offset && hdr->sync) {
            // The offset of the hstimer should be updated and there is a timestamp inside the packet
            // --> The starttime should be modified to match the one of the sender and the offset of the hstimer should be modified accordingly

            // Extract the timestamp of the packet
            uint64_t rx_timestamp_hs;
            memcpy((uint8_t*)&rx_timestamp_hs, hdr->timestamp, CHAOS_TIMESTAMP_LENGTH);

            // Compute the required change of the hstimer offset
            double change_hs;
            if (rx_time_hs < rx_timestamp_hs) {
                change_hs =   (double)(rx_timestamp_hs - rx_time_hs);
            } else {
                change_hs = - (double)(rx_time_hs      - rx_timestamp_hs);
            }

            // Only update the offset and the starttime if either update_starttime is CHAOS_TIMESYNC_FULL or
            // if the change is within [-CHAOS_MAX_CLOCK_CORRECTION_BACKWARDS_HS, CHAOS_MAX_CLOCK_CORRECTION_FORWARD_HS]
            if (  (chaos_state->update_starttime == CHAOS_TIMESYNC_FULL)                                                ||
                ( (chaos_state->update_starttime == CHAOS_TIMESYNC_BOUNDED) && CHAOS_CHECK_CLOCK_CORRECTION(change_hs) )  ) {
                // Compute the starttime of the sender and adapt it
                chaos_state->starttime_hs       = rx_timestamp_hs - hdr->message_slot_count * CHAOS_MESSAGE_INTERVAL_HS;
                chaos_state->message_slot_count = hdr->message_slot_count;

                // Change the hstimer offset as computed
                hs_timer_adapt_offset(change_hs);
                if        (ABS(change_hs) >  SYNC_THRESHOLD_CHAOS_WARN_HS) {
                    LOG_WARNING("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t)change_hs));
                } else if (ABS(change_hs) >= SYNC_THRESHOLD_CHAOS_INFO_HS) {
                    LOG_VERBOSE("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t)change_hs));
                }
            }
        } else {
            // Either the hstimer should not be adapted or the packet contains no timestamp
            // --> Only change the starttime such that the node starts its slots at the same time as the sender

            // To avoid casting uint64 timestamps to int64 (which might cause issues in the unlikely case that they are bigger than 2^63 - 1),
            // the two timestamps are first subtracted before casting them. This however requires to first determine which one is bigger.
            int64_t change_hs;
            if (slot_start_timestamp_hs < rx_time_hs) {
                change_hs =   (int64_t)(rx_time_hs              - slot_start_timestamp_hs);
            } else {
                change_hs = - (int64_t)(slot_start_timestamp_hs - rx_time_hs);
            }

            // Correct the starttime if the received message is from a different message slot
            change_hs += (chaos_state->message_slot_count - (int64_t)hdr->message_slot_count) * (int64_t)CHAOS_MESSAGE_INTERVAL_HS;

            // Only update the offset and the starttime if either update_starttime is CHAOS_TIMESYNC_FULL or
            // if the change is within [-CHAOS_MAX_CLOCK_CORRECTION_BACKWARDS_HS, CHAOS_MAX_CLOCK_CORRECTION_FORWARDS_HS]
            if (  (chaos_state->update_starttime == CHAOS_TIMESYNC_FULL)                                                 ||
                ( (chaos_state->update_starttime == CHAOS_TIMESYNC_BOUNDED) && CHAOS_CHECK_CLOCK_CORRECTION(-change_hs) )  ) {

                // The starttime is not updated if it would lead to a negative starttime since uint64 cannot represent this
                if ( (change_hs > 0) || (chaos_state->starttime_hs >= ABS(change_hs) ) ) {
                    chaos_state->starttime_hs += change_hs;
                }

                // Adopt the message slot count from the received message
                chaos_state->message_slot_count = hdr->message_slot_count;
            }
        }
    }

#if CHAOS_DEBUG_PRINTS
    LOG_VERBOSE("RX finished");
#endif /* CHAOS_DEBUG_PRINTS */

    chaos_state->nr_rx_success++;
    finished_rx_callback(true, payload + hdr_len, size - hdr_len);
}

/* This function gets executed after having transmitted a message. */
void chaos_radio_tx_callback()
{
#if CHAOS_DEBUG_PRINTS
    LOG_VERBOSE("TX finished");
#endif /* CHAOS_DEBUG_PRINTS */

#if CHAOS_RADIO_USE_SLEEP
    radio_sleep(CHAOS_RADIO_SLEEP_WARM);
#else
    //radio_standby();
#endif

    chaos_state->nr_tx_success++;
    finished_tx_callback();
}

/* Initialises the radio. */
void chaos_radio_init(chaos_state_t *state)
{
    // Set radio configs
    uint8_t pkt_len = CHAOS_HEADER_LENGTH + state->include_timestamp * CHAOS_TIMESTAMP_LENGTH + state->size;
    radio_set_config(chaos_get_modulation(), chaos_get_band(), chaos_get_tx_power(), pkt_len);

    // Initialize variables
    memset(&chaos_header, 0, sizeof(chaos_header_t));
    chaos_header.protocol_id = PROTOCOL_ID_CHAOS;
    chaos_header.type        = 0;
    chaos_header.sync        = state->include_timestamp;

    slot_start_timestamp_hs = 0;
}

/* Compute the next slot start time. (Unit: hstimer ticks) */
static inline uint64_t next_slot_start_time(chaos_state_t* state)
{
    uint64_t now    = hs_timer_get_current_timestamp();
    uint64_t result = state->starttime_hs + (state->message_slot_count * CHAOS_MESSAGE_INTERVAL_HS);

    if (result < (now + CHAOS_RADIO_TIME_BUFFER_HS) ) {
        // Unless the node was listening for multiple slots, it should not be late for a slot
        if (!(chaos_state->initiate_type == CHAOS_DO_NOT_INITIATE || chaos_state->initiate_type == CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT) ||
            chaos_state->first_message_received                                                                                                   ) {
            LOG_ERROR("Too late for slot %hhu", state->message_slot_count);
        }

        state->message_slot_count = (now + CHAOS_RADIO_TIME_BUFFER_HS - state->starttime_hs + CHAOS_MESSAGE_INTERVAL_HS - 1) / (CHAOS_MESSAGE_INTERVAL_HS);
        result                    = state->starttime_hs + (state->message_slot_count * CHAOS_MESSAGE_INTERVAL_HS);
    }

#if CHAOS_DEBUG_PRINTS
    LOG_VERBOSE("Slot number: %hhu (%llums)", state->message_slot_count, HS_TIMER_TICKS_TO_MS(now));
#endif /* CHAOS_DEBUG_PRINTS */

    return result;
}

void chaos_tx(chaos_state_t *state, void (*callback)())
{
    chaos_state = state;
#if CHAOS_RADIO_USE_SLEEP
    radio_wakeup();
    chaos_radio_init(state);
#endif /* CHAOS_RADIO_USE_SLEEP */

    // Prepare the radio
    radio_set_irq_mode(IRQ_MODE_TX);
    finished_tx_callback = callback;
    radio_set_timeout_callback(chaos_radio_tx_timeout_callback);
    radio_set_tx_callback(chaos_radio_tx_callback);
    slot_start_timestamp_hs = next_slot_start_time(state);

    // Update the header
    chaos_header.message_slot_count = state->message_slot_count;
    memcpy(&chaos_header.timestamp, &slot_start_timestamp_hs, CHAOS_TIMESTAMP_LENGTH);

    // Set the payload
    uint8_t hdr_len = (state->include_timestamp) ? CHAOS_HEADER_LENGTH_WITH_TS : CHAOS_HEADER_LENGTH;
    radio_set_payload_chunk((uint8_t*)&chaos_header,       0,     hdr_len, false);
    radio_set_payload_chunk(state->payload,          hdr_len, state->size, true);

    // Add an offset to the transmission start timestamp in order to be consistent with Gloria
    slot_start_timestamp_hs -= CHAOS_HSTIMER_TRIGGER_DELAY_HS;

    // Schedule the TX slot
    radio_transmit_scheduled(0, 0, slot_start_timestamp_hs);
}

void chaos_rx(chaos_state_t *state, void (*callback)(bool received, uint8_t* packet, uint8_t size))
{
    chaos_state = state;
#if CHAOS_RADIO_USE_SLEEP
    radio_wakeup();
    chaos_radio_init(state);
#endif /* CHAOS_RADIO_USE_SLEEP */

    // Prepare the radio
    radio_set_irq_mode(IRQ_MODE_RX_CRC);
    finished_rx_callback = callback;
    radio_set_timeout_callback(chaos_radio_rx_timeout_callback);
    radio_set_rx_callback(chaos_radio_rx_callback);
    slot_start_timestamp_hs = next_slot_start_time(state);

    // Schedule RX slot
    if (!chaos_state->first_message_received                                         &&
        (chaos_state->initiate_type == CHAOS_DO_NOT_INITIATE                      ||
         chaos_state->initiate_type == CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT  )  ) {
        radio_receive_scheduled(slot_start_timestamp_hs - CHAOS_RADIO_RECEIVE_OFFSET_HS, MIN(CHAOS_RADIO_RECEIVE_TIMEOUT_LONG_HS, chaos_state->endtime_hs - hs_timer_get_current_timestamp() ) );
    } else {
        radio_receive_scheduled(slot_start_timestamp_hs - CHAOS_RADIO_RECEIVE_OFFSET_HS, CHAOS_RADIO_RECEIVE_TIMEOUT_HS);
    }
}
