/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Chaos: Logic
 *
 * This file contains the logic of Chaos.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static chaos_state_t* chaos_state;


/* Static functions ----------------------------------------------------------*/
static void chaos_process_tx_callback();
static void chaos_process_rx_callback(bool received, uint8_t* packet, uint8_t size);


/* Functions -----------------------------------------------------------------*/

/* Causes the node to randomly choose whether to transmit or to receive in the next slot.
 * The node sends with probability CHAOS_INITIATE_PROBABILITY. */
static inline void chaos_rxtx_probabilistic()
{
    if ( (random_rand32() % 100) < CHAOS_INITIATE_PROBABILITY) {
        chaos_tx(chaos_state, chaos_process_tx_callback);
    } else {
        chaos_rx(chaos_state, chaos_process_rx_callback);
    }
}

void chaos_prepare_first_message_slot(chaos_state_t *state)
{
    chaos_state = state;
    switch (state->initiate_type) {
        case CHAOS_INITIATE_ALONE:
            // Send in the first slot if the current node is the only one initiating
            chaos_tx(chaos_state, chaos_process_tx_callback);
            break;
        case CHAOS_INITIATE_WITH_OTHERS:
            // Send with probability CHAOS_INITIATE_PROBABILITY in the first slot
            chaos_rxtx_probabilistic();
            break;
        case CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT:
        case CHAOS_DO_NOT_INITIATE:
            // Receive in the first slot
            chaos_rx(chaos_state, chaos_process_rx_callback);
            break;
    }
}

/* Determines whether the node should receive or listen in the next slot. */
static void chaos_prepare_next_message_slot(bool finished_rx)
{
    chaos_state->message_slot_count++;

    // Stop if chaos_stop was called
    if (chaos_state->stop) {
        chaos_state->stopped = true;
        return;
    }

    switch (chaos_state->result) {
        case CHAOS_PACKET_COMPLETE_BUT_KEEP_TRANSMITTING:
        case CHAOS_PACKET_NOT_COMPLETE:
            // Chaos is initiated with others but no message was received yet
            // --> Send with probability CHAOS_INITIATE_PROBABILITY
            if (finished_rx                                                                  &&
                !chaos_state->first_message_received                                         &&
                (chaos_state->initiate_type == CHAOS_INITIATE_WITH_OTHERS                 ||
                 chaos_state->initiate_type == CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT  )  ) {
                chaos_rxtx_probabilistic();
            } else {
                // The Chaos packet is not yet complete
                // --> Transmit if a packet containing new information was received or if the timeout counter reaches zero
                if ( (finished_rx && chaos_state->packets_differ) || (chaos_state->timeout_counter == 0) ) {
                    chaos_tx(chaos_state, chaos_process_tx_callback);
                } else {
                    chaos_rx(chaos_state, chaos_process_rx_callback);
                }
            }
            break;

        case CHAOS_PACKET_COMPLETE:
            // The packet is complete
            // --> Transmit for CHAOS_NR_TRANSMIT_AFTER_CHAOS_COMPLETE consecutive rounds, then finish
            if (chaos_state->tx_slot_counter < CHAOS_NR_TRANSMIT_AFTER_CHAOS_COMPLETE) {
                chaos_tx(chaos_state, chaos_process_tx_callback);
            } else {
                chaos_state->stopped = true;
            }
            break;

        case CHAOS_PACKET_COMPLETE_BUT_KEEP_LISTENING:
            // The packet is complete but the node should keep listening
            // --> Transmit for CHAOS_NR_TRANSMIT_AFTER_CHAOS_COMPLETE,
            // Then listen until no incomplete packet was observed for CHAOS_KEEP_LISTENING_TIMEOUT consecutive rounds
            if (chaos_state->tx_slot_counter < CHAOS_NR_TRANSMIT_AFTER_CHAOS_COMPLETE) {
                chaos_tx(chaos_state, chaos_process_tx_callback);
            } else if (chaos_state->incomplete_packet_received) {
                // An incomplete packet was received
                // The complete packet is only retransmitted with a certain probability to increase capture probability
                chaos_rxtx_probabilistic();
                // Reset in any case even if did not send
                chaos_state->no_update_slot_counter = 0;
            } else if (chaos_state->no_update_slot_counter < CHAOS_KEEP_LISTENING_TIMEOUT) {
                chaos_rx(chaos_state, chaos_process_rx_callback);
            } else {
                chaos_state->stopped = true;
            }
            break;
    }
}

/* This function is executed after having transmitted a packet. */
void chaos_process_tx_callback()
{
    // Update the counter indicating how many consecutive messages were sent
    chaos_state->tx_slot_counter++;

    // Reset RX counters
    chaos_state->timeout_counter            = CHAOS_TIMEOUT + (random_rand32() % CHAOS_TIMEOUT_WINDOW);
    chaos_state->no_update_slot_counter     = 0;
    chaos_state->incomplete_packet_received = false;

    // Prepare next slot
    chaos_prepare_next_message_slot(false);
}

/* This function is executed after finishing a receive slot. */
void chaos_process_rx_callback(bool received, uint8_t* packet, uint8_t size)
{
    // Reset TX counter if the packet is not complete yet
    if (chaos_state->result == CHAOS_PACKET_NOT_COMPLETE) {
        chaos_state->tx_slot_counter = 0;
    }

    if (received) {
        chaos_state->first_message_received = true;
        // If a packet was received: merge with the local packet
        chaos_state->packets_differ         = chaos_state->merge_fn(chaos_state->payload, chaos_state->size, packet, size);

        // Check if the returned packet size is valid
        if (chaos_state->size > CHAOS_MAX_PAYLOAD_SIZE(chaos_state->include_timestamp)) {
            chaos_state->size = CHAOS_MAX_PAYLOAD_SIZE(chaos_state->include_timestamp);
            LOG_ERROR("Pkt len returned by merge_fn is too big -> pkt will be truncated");
        }

        // Determine whether the packet is complete
        chaos_termination_result_t result_prev = chaos_state->result;
        chaos_state->result                    = chaos_state->termination_fn(chaos_state->payload, chaos_state->size);

        if ( (result_prev == CHAOS_PACKET_NOT_COMPLETE) && (chaos_state->result > CHAOS_PACKET_NOT_COMPLETE) ) {
            LOG_VERBOSE("Completed in slot %2hhu", chaos_state->message_slot_count);
        }
        if (chaos_state->result == CHAOS_PACKET_COMPLETE_BUT_KEEP_LISTENING) {
            chaos_state->incomplete_packet_received = (chaos_state->termination_fn(packet, size) == CHAOS_PACKET_NOT_COMPLETE);
        }

        // Update counters
        chaos_state->timeout_counter        = CHAOS_TIMEOUT + (random_rand32() % CHAOS_TIMEOUT_WINDOW);
        chaos_state->no_update_slot_counter = chaos_state->packets_differ ? 0 : (chaos_state->no_update_slot_counter + 1);
    } else {
        chaos_state->packets_differ             = false;
        chaos_state->incomplete_packet_received = false;

        // Update counters
        chaos_state->no_update_slot_counter += 1;

        if ( (chaos_state->result == CHAOS_PACKET_COMPLETE_BUT_KEEP_LISTENING) || !chaos_state->first_message_received) {
            // Timeout is not in use, as TX will only be initiated by the reception of an incomplete packet or no message has been initially received yet
        } else if (chaos_state->timeout_counter == 0) {
            LOG_ERROR("Timeout counter should be decremented but already is 0 after slot %hhu", chaos_state->message_slot_count);
        } else {
            chaos_state->timeout_counter    -= 1;
        }
    }

    // Prepare next slot
    chaos_prepare_next_message_slot(true);
}
