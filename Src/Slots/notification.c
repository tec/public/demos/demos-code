/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Notification
 *
 * This code implements the Notification slot, which is used to send and receive information on other discovered networks.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static comm_state_t*     comm_state;
static discover_packet_t d_packet;


/* Functions -----------------------------------------------------------------*/

/* Filters packets from other networks. */
bool notification_filter_fn(const uint8_t* gloria_hdr, uint8_t hdr_len, const uint8_t* payload, uint8_t size)
{
    discover_packet_t* pkt = (discover_packet_t*)payload;

#if DROP_NODE_PACKETS_ENABLE
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(pkt->id_src) && ( (random_rand32() % 100) < DROP_NODE_PACKETS_PROBABILITY)) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (USED_RADIO_TX_POWER == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */

    // Check packet size
    if (size != sizeof(discover_packet_t)) {
        LOG_VERBOSE("Wrong size (%hhuB)", size);
        return false;
    }

    // Check if the packet is a Discover packet
    if (pkt->type != PACKET_TYPE_DISCOVER) {
        LOG_VERBOSE("Wrong type (rcvd type %hhu but expected %hhu)", pkt->type, PACKET_TYPE_DISCOVER);
        return false;
    }

    // Check whether the timestamp of the packet is from the current slot
    uint64_t pkt_timestamp = extract_timestamp(gloria_hdr, hdr_len + size);
    if (pkt_timestamp == 0) {
        LOG_WARNING("Failed to extract timestamp");
        return false;
    } else if ( (pkt_timestamp - LPTIMER_TICKS_TO_HS_TIMER(comm_state->t_slot) ) > LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP) ) {
        LOG_VERBOSE("Wrong timestamp");
        return false;
    }

    return true;
}

void run_notification_slot(comm_state_t* state)
{
    // Store the pointer to the comm_state such that the packet filter function has access to it
    comm_state = state;

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    gloria_set_tx_power(USED_RADIO_TX_POWER);

    // Adjust RxBoosted
    radio_set_rx_gain(USED_RADIO_TX_POWER > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

#if CHANGE_FRQ_BAND_ENABLE
    // Set the frequency band
    gloria_set_band(USED_RADIO_FRQ_BAND(state->freq_band));
#endif /* CHANGE_FRQ_BAND_ENABLE */

    // Check whether a network has been discovered
    if (state->net_discovered) {
        // Transmit a Discover packet

        // Initialise the Discover packet
        d_packet.type   = PACKET_TYPE_DISCOVER;
        d_packet.id_src = state->id_discovered;
        d_packet.T      = state->T_d;
        d_packet.r      = state->r_d;
        d_packet.t      = state->t_d;

        int64_t delta_rx = (int64_t)lptimer_now() - state->t_d_rx;
#if D_LOG_TIMESTAMPS
        if ( (delta_rx < 0) || (delta_rx > (NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) ) {
            LOG_ERROR("Expected (small) positive delta but encountered %lli for lp=%llu/t_rx=%llu (%lluus)", delta_rx, lptimer_now(), state->t_d_rx, LPTIMER_TICKS_TO_US(state->t_d_rx));
        } else if (state->t_d > (2 * NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) {
            LOG_ERROR("Corrupted timestamp t=%llu", state->t_d);
        } else {
#else
        {
#endif /* D_LOG_TIMESTAMPS */
            d_packet.t += delta_rx;
        }
#if DISCOVERY_ADD_NETWORK_SIZE
        d_packet.N      = state->N_d;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */

#if D_LOG_TIMESTAMPS
        LOG_VERBOSE("N info: t=%lluus/t_rx=%lluus/T=%hhu", LPTIMER_TICKS_TO_US(d_packet.t), LPTIMER_TICKS_TO_US(state->t_d_rx), d_packet.T);
#endif /* D_LOG_TIMESTAMPS */

        // Start the Gloria flood
        gloria_start(true,
                     (uint8_t*)&d_packet,
                     sizeof(discover_packet_t),
                     GLORIA_MAX_RETRANSMISSIONS,
                     N_USE_AS_REFERENCE);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
        gloria_stop();
        LOG_VERBOSE("N sent from leader %2hhu", d_packet.id_src);
    } else {
        // Receive notification
        LOG_VERBOSE("Listening for N");

        gloria_set_pkt_filter(notification_filter_fn);
        gloria_start(false,
                     (uint8_t*)&d_packet,
                     sizeof(discover_packet_t),
                     GLORIA_MAX_RETRANSMISSIONS,
                     true);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP + GLORIA_GUARD_TIME_LP);

        // Update the schedule if one was received
        if (gloria_stop()) {
            LOG_INFO("N rcvd from %hhu", d_packet.id_src);

#if N_USE_AS_REFERENCE
            // Correct the clock offset
            uint64_t timestamp_rx;
            if (gloria_get_received_timestamp((uint8_t *) &timestamp_rx)) {
                //LOG_VERBOSE("Successfully synced to N");
            } else {
                LOG_WARNING("Failed to sync to N");
            }

            // Adjust HS timer
            double delta;
            if (gloria_get_t_ref_hs() < timestamp_rx) {
                delta = (double) (timestamp_rx - gloria_get_t_ref_hs());
            } else {
                delta = -(double) (gloria_get_t_ref_hs() - timestamp_rx);
            }

            hs_timer_adapt_offset(delta);
            if (ABS(delta) > SYNC_THRESHOLD_GLORIA_WARN_HS) {
                LOG_WARNING("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t) delta));
            } else if (ABS(delta) >= SYNC_THRESHOLD_GLORIA_INFO_HS) {
                LOG_VERBOSE("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t) delta));
            }

            // Sync LP to HS timer
            update_clock_offset();
#endif /* N_USE_AS_REFERENCE */

            // Store the local time when the packet was received - ATTENTION: gloria_get_t_ref() cannot be used directly, as its timestamp is occasionally corrupted
            state->t_d_rx         = corrected_to_uncorrected_lp_timestamp(HS_TIMER_TICKS_TO_LPTIMER(gloria_get_t_ref_hs()));

            // Extract discovered network parameters
            state->net_discovered = true;
            state->id_discovered  = d_packet.id_src;
            state->T_d            = d_packet.T;
            state->r_d            = d_packet.r;
            state->t_d            = d_packet.t + D_TX_DELAY_LP(gloria_get_rx_index());
#if DISCOVERY_ADD_NETWORK_SIZE
            state->N_d            = d_packet.N;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */

#if D_LOG_TIMESTAMPS
            LOG_VERBOSE("N info: t=%lluus/t_rx=%lluus/T=%hhu/lp=%lluus", LPTIMER_TICKS_TO_US(state->t_d), LPTIMER_TICKS_TO_US(state->t_d_rx), state->T_d, LPTIMER_TICKS_TO_US(lptimer_now()));

            int64_t delta_rx = (int64_t)lptimer_now() - state->t_d_rx;
            if ( (delta_rx < 0) || (delta_rx > (NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) ) {
                LOG_ERROR("Expected (small) positive delta but encountered %lli for lp=%llu/t_rx=%llu (%lluus)", delta_rx, lptimer_now(), state->t_d_rx, LPTIMER_TICKS_TO_US(state->t_d_rx));
            }
#endif /* D_LOG_TIMESTAMPS */

            switch (state->network_state) {
                case STATE_NETWORK_FORMATION:
                    LOG_INFO("Rcvd N from leader %2hhu", d_packet.id_src);
                    enter_normal(state);
                    break;
                default:
                    break;
            }
        } else {
            //LOG_VERBOSE("No N rcvd");
        }
    }

    switch (state->network_state) {
        default:
            break;
    }
}
