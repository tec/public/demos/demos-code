/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Discovery
 *
 * This code implements the Discovery slot.
 * This slot is used to discover other networks and to initiate the merging process.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static comm_state_t*     comm_state;
static discover_packet_t d_packet;


/* Functions -----------------------------------------------------------------*/

/* Filters packets which cannot be used for discovery. */
bool discovery_filter_fn(const uint8_t* gloria_hdr, uint8_t hdr_len, const uint8_t* payload, uint8_t size)
{
    discover_packet_t* pkt = (discover_packet_t*)payload;

#if DROP_NODE_PACKETS_ENABLE
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(pkt->id_src) && ( (random_rand32() % 100) < DROP_NODE_PACKETS_PROBABILITY)) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (USED_RADIO_TX_POWER == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */

    // Check packet size
    if (size != sizeof(discover_packet_t)) {
        LOG_VERBOSE("Wrong size (%hhuB)", size);
        return false;
    }

    // Check if the packet is a Discover packet
    if (pkt->type != PACKET_TYPE_DISCOVER) {
        LOG_VERBOSE("Wrong type (rcvd type %hhu but expected %hhu)", pkt->type, PACKET_TYPE_DISCOVER);
        return false;
    } else if (pkt->id_src == comm_state->p) {
        // We ignore packets from the same network; however, this should never happen according to the Discovery schedule
        LOG_ERROR("D ignored from own network");
        return false;
    }

    return true;
}

inline void prepare_discovery(comm_state_t* state)
{
    if (state->disc_prepared) { return; }

    // Re-set discovery information
    state->net_discovered = false;
    memset(state->S_disc, 0, NR_SLOTS_D);

#if D_NOTIFY_ABOUT_BEST_DISCOVERED_NET
    state->id_discovered = 0;
#endif /* D_NOTIFY_ABOUT_BEST_DISCOVERED_NET */

    if (!is_flags_empty(state->M)) {
        // Prepare discovery schedule
        discovery_distribute(state);
    }

    state->disc_prepared = true;
}

void run_discovery_slot(comm_state_t* state)
{
    // Store the pointer to the comm_state such that the packet filter function has access to it
    comm_state = state;

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    gloria_set_tx_power(USED_RADIO_TX_POWER);

    // Adjust RxBoosted
    radio_set_rx_gain(USED_RADIO_TX_POWER > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

    // Set the frequency band and revert after the slot
#if CHANGE_FRQ_BAND_ENABLE
    gloria_set_band(USED_RADIO_FRQ_BAND(FRQ_BAND_DISCOVERY));
#else
    gloria_set_band(FRQ_BAND_DISCOVERY);
#endif /* CHANGE_FRQ_BAND_ENABLE */

    if (state->slot_next.sender & D_SCHEDULE_MASK_TX) {
        // Broadcast beacon

        // Prepare the packet
        d_packet.type   = PACKET_TYPE_DISCOVER;
        d_packet.id_src = state->p;
        d_packet.T      = state->T;
        d_packet.r      = state->r;
        d_packet.t      = lptimer_now_corrected() - state->t_round;
#if DISCOVERY_ADD_NETWORK_SIZE
        d_packet.N      = state->N;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */

#if D_LOG_TIMESTAMPS
        LOG_VERBOSE("D info: t=%lluus/T=%hhu", LPTIMER_TICKS_TO_US(d_packet.t), d_packet.T);
#endif /* D_LOG_TIMESTAMPS */

        // Start initiating a Gloria flood
        gloria_start(true,
                     (uint8_t*)&d_packet,
                     sizeof(discover_packet_t),
                     D_NR_BEACONS_PER_SLOT,
                     false);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
        gloria_stop();
        //LOG_VERBOSE("D sent");
    } else {
        // Listen for beacon

        // Start listening for Gloria floods
        gloria_set_pkt_filter(discovery_filter_fn);
        gloria_start(false,
                     (uint8_t*)&d_packet,
                     sizeof(discover_packet_t),
                     D_NR_BEACONS_PER_SLOT,
                     true);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP + GLORIA_GUARD_TIME_LP + D_EXTRA_LISTEN_LP);

        // Process the packet if one was received
        if (gloria_stop()) {
#if D_NOTIFY_ABOUT_BEST_DISCOVERED_NET
            if (IS_DOMINATED(comm_state->p, comm_state->N, d_packet.id_src, d_packet.N) && IS_DOMINATED(d_packet.id_src, d_packet.N, comm_state->id_discovered, comm_state->N_d) ) {
                LOG_VERBOSE("D ignored from %2hhu < %2hhu", d_packet.id_src, comm_state->id_discovered);
            } else
#endif /* D_NOTIFY_ABOUT_BEST_DISCOVERED_NET */
            if (IS_DOMINATED(comm_state->p, comm_state->N, d_packet.id_src, d_packet.N) ) {
                LOG_VERBOSE("D rcvd    from %2hhu", d_packet.id_src);

                // ATTENTION: gloria_get_t_ref() cannot be used directly, as its timestamp is occasionally corrupted
                comm_state->t_d_rx         = corrected_to_uncorrected_lp_timestamp(HS_TIMER_TICKS_TO_LPTIMER(gloria_get_t_ref_hs()));

                // Extract discovered network parameters
                comm_state->net_discovered = true;
                comm_state->id_discovered  = d_packet.id_src;
                comm_state->T_d            = d_packet.T;
                comm_state->r_d            = d_packet.r;
                comm_state->t_d            = d_packet.t + D_TX_DELAY_LP(gloria_get_rx_index());
#if DISCOVERY_ADD_NETWORK_SIZE
                comm_state->N_d            = d_packet.N;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */

#if D_LOG_TIMESTAMPS
                LOG_VERBOSE("D info: t=%lluus/t_rx=%lluus/T=%hhu/lp=%lluus", LPTIMER_TICKS_TO_US(comm_state->t_d), LPTIMER_TICKS_TO_US(comm_state->t_d_rx), comm_state->T_d, LPTIMER_TICKS_TO_US(lptimer_now()));

                int64_t delta_rx = (int64_t)lptimer_now() - state->t_d_rx;
                if ( (delta_rx < 0) || (delta_rx > (NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) ) {
                    LOG_ERROR("Expected (small) positive delta but encountered %lli for lp=%llu/t_rx=%llu (%lluus)", delta_rx, lptimer_now(), comm_state->t_d_rx, LPTIMER_TICKS_TO_US(comm_state->t_d_rx));
                }
#endif /* D_LOG_TIMESTAMPS */

                switch (state->network_state) {
                    default:
                        break;
                }
            } else if (comm_state->p > d_packet.id_src) {
                LOG_VERBOSE("D ignored from %2hhu", d_packet.id_src);
            }
        } else {
            //LOG_VERBOSE("No D rcvd");

            switch (state->network_state) {
                default:
                    break;
            }
        }
    }

    // Revert the frequency band
    gloria_set_band(comm_state->freq_band);
}
