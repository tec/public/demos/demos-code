/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Voting
 *
 * This code implements the Voting slot.
 * In this slot, the votes and requests for the next schedule is aggregated.
 * This slot is also used for requesting to join a network or for agreeing to merge with some other network.
 *
 */

#include "main.h"


/* Global variables ---------------------------------------------------------*/
uint32_t use_q_a      = V_ELECT_BASED_ON_MAJORITY;
uint32_t use_q_r      = V_ELECT_BASED_ON_POSITIVE_VOTES_FRAC;
uint32_t use_q_a_plus = V_ELECT_BASED_ON_POSITIVE_VOTES_NUM;


/* Static variables ----------------------------------------------------------*/
static comm_state_t* comm_state;
static vote_packet_t v_packet;
static uint16_t      pkt_rcvd_cnt;
static uint16_t      pkt_merged_cnt;
static uint16_t      filter_applied_cnt;
static uint16_t      filter_passed_cnt;


/* Functions -----------------------------------------------------------------*/

/* Returns the request value of a specific node stored in a Vote packet.
 *   pkt:     Pointer to the Vote packet to look into.
 *   node_id: ID of the node of which the request value is returned.
 */
int8_t get_request_value(const vote_packet_t* pkt, node_id_t node_id)
{
    // The offset of the target value in bits
    uint32_t offset = (node_id - 1) * V_NR_REQ_BITS;
    // Extract the byte containing the bits of the current value
    uint16_t val    = pkt->D[offset / 8];

    // If some bits of the current request value are in the next byte: Append that byte to the current temporary value
    if ( ( (offset % 8) + V_NR_REQ_BITS) > 8) {
        val += ( (uint16_t)pkt->D[ (offset / 8) + 1] ) << 8;
    }

    // Shift the bits of the target ID to remove the offset
    int8_t target_value  = (int8_t)(val >> (offset % 8) );
    // Extract only bits of the target ID
           target_value &= (1 << V_NR_REQ_BITS) - 1;
    // Add the minimum value to obtain the negative values
    return target_value + SLOT_COUNT_NEUTRAL_ELEMENT;
}

/* Extracts all the request values and writes the result into an array.
 *   pkt:    Vote packet from which the request values are taken.
 *   result: Pointer to an array of size MAX_NR_NODES. The result is written to this array.
 */
static void get_request_values(vote_packet_t* pkt, int8_t* result)
{
    uint32_t  offset  = 0;
    node_id_t node_id = 0;

    for (uint32_t i = 0; i < V_PACKET_NR_REQ_BYTES; i++) {
        uint16_t curr_byte = pkt->D[i];

        while ( (offset < 8) && (node_id < MAX_NR_NODES) ) {
            // If some bits of the current request value are in the next byte, append that byte to the current temporary value
            if ( (offset + V_NR_REQ_BITS) > 8) {
                curr_byte += ( (uint16_t)pkt->D[i + 1] ) << 8;
            }

            // Shift the bits of the current request value to remove the offset
            int8_t target_value  = (int8_t)(curr_byte >> offset);
            // Extract only bits of the target ID
                   target_value &= (1 << V_NR_REQ_BITS) - 1;
            // Add the minimal value to obtain the negative values
            result[node_id]      = target_value + SLOT_COUNT_NEUTRAL_ELEMENT;

            // Increase the counters
            node_id++;
            offset += V_NR_REQ_BITS;
        }

        // Prepare the offset for the next byte
        offset %= 8;
    }
}

/* Sets the request value of a specific node stored in a Vote packet if it is higher than the previous value.
 *   pkt:       Pointer to the Vote packet to look into.
 *   node_id:   Node ID of which the request value is to be set.
 *   new_value: New request value in the interval [SLOT_COUNT_REQ_MIN, SLOT_COUNT_REQ_MAX].
 *
 *   Return value: True if request was changed
 */
static bool set_request_value(vote_packet_t* pkt, node_id_t node_id, int8_t new_value)
{
    // The offset in bits the target value has
    uint32_t offset = (node_id - 1) * V_NR_REQ_BITS;
    // Copy the byte containing the request value to a temporary value
    uint16_t val    = pkt->D[offset / 8];
    // If some bits of the request value are contained in the next byte: Append this byte to the temporary value
    if ( ( (offset % 8) + V_NR_REQ_BITS) > 8) {
        val += ( (uint16_t)pkt->D[offset / 8 + 1] ) << 8;
    }

    // The mask, in which only the bits of the request value of the node are set
    uint16_t mask            = ( (1 << V_NR_REQ_BITS) - 1) << (offset % 8);

    // Extract the previous value
    uint16_t conv_prev_value = val & mask;

    // Add -SLOT_COUNT_NEUTRAL_ELEMENT to the new value in order to obtain a non-negative integer. The result is shifted to the right position
    uint16_t conv_new_value  = ( (uint16_t) ((int16_t)new_value - SLOT_COUNT_NEUTRAL_ELEMENT) ) << (offset % 8);

    // Only update the value if the value has not been set or is new
    if ( (conv_prev_value == conv_new_value) || (new_value == SLOT_COUNT_NEUTRAL_ELEMENT) ) {
        // No action required, as previous value will be retained
        return false;
    } else if (conv_prev_value == 0) {
        // Clear the bits corresponding to the request value of the node
        val &= ~mask;

        // Set the bits corresponding to the request value of the node
        val += conv_new_value;

        // Write the temporary value back into the array
        pkt->D[offset / 8] = val & 0x00FF;
        // If some bits of the value are contained in the next byte: Write that one back as well
        if ( ( (offset % 8) + V_NR_REQ_BITS) > 8) {
            pkt->D[ (offset / 8) + 1] = val >> 8;
        }

        return true;
    } else {
        LOG_ERROR("Attempt to change request for node %2hhu from %3u to %3u", node_id, conv_prev_value, conv_new_value);
        return false;
    }
}

/* Efficient merging implementation leveraging that SLOT_COUNT_NEUTRAL_ELEMENT is represented through 0. */
static bool merge_request_values(vote_packet_t* pkt, vote_packet_t* pkt_rx)
{
    /* Legacy: Unoptimized variant (~25us less efficient with default network settings)
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
        changed |= set_request_value(pkt, i + 1, get_request_value(pkt_rx, i + 1));
    }*/

    bool changed = false;
    uint32_t ofs = 0;
    for (uint32_t i = 0; i < V_PACKET_NR_REQ_BYTES; i++) {
        // Combine previous and received request
        uint16_t prev_request = pkt->D[i];
        uint16_t rx_request   = pkt_rx->D[i];
        if (i < (V_PACKET_NR_REQ_BYTES - 1)) {
            prev_request += (pkt->D[i+1]    << 8);
            rx_request   += (pkt_rx->D[i+1] << 8);
        }

        // Generate mask for existing requests as well as relevant received information
        uint16_t prev_request_mask = 0;
        uint16_t rx_request_mask   = 0;
        while (ofs < 8) {
            uint16_t request_mask  = ((1 << V_NR_REQ_BITS) - 1) << ofs;
            prev_request_mask     += request_mask * ( (prev_request & request_mask) > 0);
            rx_request_mask       += request_mask * ( (rx_request   & request_mask) > 0);
            ofs                   += V_NR_REQ_BITS;
        }

        // Reduce received information to the scope of the current request byte
        rx_request &= rx_request_mask;

        // Sanity check: No different existing information is permitted
        if ( (rx_request & prev_request_mask) ^ (prev_request & rx_request_mask) ) {
            LOG_WARNING("Attempt to change request starting at index %hhu from %u to %u", i, prev_request, rx_request & prev_request_mask);
            return false;
        }

        // Check if new information is contained
        rx_request &= ~prev_request_mask;
        if (rx_request > 0) {
            // Store the new requests back into the current packet
            pkt->D[i] |= rx_request & 0x00FF;
            if (rx_request & 0xFF00) {
                pkt->D[i+1] |= rx_request >> 8;
            }

            changed = true;
        }

        // Prepare the offset for the next byte
        ofs %= 8;
    }

    return changed;
}

/* Merge two arrays of request values.
 *  array_result: Pointer to an array of size MAX_NR_NODES which will store the results
 *  array_added:  Pointer to an array of size MAX_NR_NODES whose values should be copied
 */
bool merge_request_arrays(int8_t* array_result, const int8_t* array_added) {
    bool changed = false;

    for (uint8_t i = 0; i < MAX_NR_NODES; i++) {
        if (array_result[i] != array_added[i]) {
            if        (array_result[i] == SLOT_COUNT_NEUTRAL_ELEMENT) {
                array_result[i] = array_added[i];
                changed         = true;
            } else if (array_added[i]  != SLOT_COUNT_NEUTRAL_ELEMENT) {
                LOG_WARNING("Arrays contained inconsistent requests for node %hhu: %hhi <-> %hhi", i + 1, array_result[i], array_added[i]);
            }
        }
    }

    return changed;
}

static bool merge_vote_arrays(uint8_t* array_result, const uint8_t* array_added)
{
    bool changed = false;

    for (uint32_t i = 0; i < NR_NODE_FLAG_BYTES; i++) {
        changed         |= (array_added[i] & ~array_result[i]) > 0;
        array_result[i] |=  array_added[i];
    }

    return changed;
}

/* Sets the request value of the node with NODE_ID stored in a Vote packet.
 * The other request values are set to SLOT_COUNT_NEUTRAL_ELEMENT.
 *  pkt:       Pointer to the Vote packet to look into.
 *  new_value: New request value in the interval [SLOT_COUNT_REQ_MIN, SLOT_COUNT_REQ_MAX].
 */
static void init_request_values(vote_packet_t* pkt, int8_t new_value)
{
    // Sets all stored requests to the converted version of SLOT_COUNT_NEUTRAL_ELEMENT
    memset(pkt->D, 0, V_PACKET_NR_REQ_BYTES);

    set_request_value(pkt, NODE_ID, new_value);
}

/* This function sets the vote value of the node with NODE_ID stored in a Vote packet.
 * The other vote values are set to 0.
 *  pkt:       A pointer to the Vote packet to look into.
 *  v:         The new vote value.
 *  p:         The current proposer.
 */
static void init_vote_values(vote_packet_t* pkt, bool v, node_id_t p)
{
    // Set all stored votes to 0
    memset(pkt->V, 0, NR_NODE_FLAG_BYTES);

    if (v && (p > 0)) {
        // Cast vote
        SET_FLAG(pkt->V, NODE_ID);
    }
}

/* Filters all packets but Vote packets. */
static bool voting_message_filter(const uint8_t* packet, const uint8_t size, const uint8_t* packet_rx, const uint8_t size_rx)
{
    uint8_t hdr_len    = (((chaos_header_t*)packet_rx)->sync) ? CHAOS_HEADER_LENGTH_WITH_TS : CHAOS_HEADER_LENGTH;
    vote_packet_t* pkt = (vote_packet_t*)(packet_rx + hdr_len);

#if DROP_NODE_PACKETS_ENABLE
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(pkt->id_src) && ( (random_rand32() % 100) < DROP_NODE_PACKETS_PROBABILITY) ) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (USED_RADIO_TX_POWER == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */
    filter_applied_cnt++;

    // Check packet size and whether the packet contains a timestamp
    if (size_rx != (hdr_len + sizeof(vote_packet_t)) ) {
        LOG_VERBOSE("Wrong size of rcvd pkt (%hhuB)", size_rx);
        return false;
#if V_USE_AS_REFERENCE
    } else if (!((chaos_header_t*)packet_rx)->sync) {
        LOG_ERROR("Rcvd pkt does not contain a timestamp");
        return false;
#endif /* V_USE_AS_REFERENCE */
    } else if (size != sizeof(vote_packet_t)) {
        LOG_ERROR("Wrong size of local pkt (%hhuB)", size);
        return false;
    }

    // Check if the packet is a Vote packet
    if (pkt->type != PACKET_TYPE_VOTE) {
        LOG_VERBOSE("Wrong type (rcvd type %hhu but expected %hhu)", pkt->type, PACKET_TYPE_VOTE);
        return false;
    }

#if VOTING_ADD_PAYLOAD_CRC
    uint32_t crc32_rx = crc32((uint8_t*)pkt, sizeof(vote_packet_t) - sizeof(uint32_t), 0);
    if (crc32_rx != pkt->crc32) {
        LOG_WARNING("Incorrect payload CRC from node %hhu", pkt->id_src);
        return false;
    }
#endif /* VOTING_ADD_PAYLOAD_CRC */

    if (((chaos_header_t*)packet_rx)->sync) {
        // Check whether the timestamp of the packet is from the current slot
        uint64_t pkt_timestamp = extract_timestamp(packet_rx, size_rx);
        if (pkt_timestamp == 0) {
            LOG_WARNING("Failed to extract timestamp from pkt from node %hhu", pkt->id_src);
            return false;
        } else if ((pkt_timestamp - LPTIMER_TICKS_TO_HS_TIMER(comm_state->t_slot)) > LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP * NR_SLOTS_V)) {
            LOG_WARNING("Wrong timestamp from node %hhu", pkt->id_src);
            return false;
        }
    }

    // Packet passed the filter
    filter_passed_cnt++;
    return true;
}

/* Merges Vote packets. */
static bool voting_merge_fn(uint8_t* payload, uint8_t size, const uint8_t* payload_rx, const uint8_t size_rx)
{
    pkt_rcvd_cnt++;

    if (size_rx != sizeof(vote_packet_t)) {
        LOG_ERROR("Wrong size of rcvd pkt (%hhuB)", size_rx);
        return false;
    } else if (size != sizeof(vote_packet_t)) {
        LOG_ERROR("Wrong size of local pkt (%hhuB)", size);
        return false;
    }

    vote_packet_t* pkt    = (vote_packet_t*)payload;
    vote_packet_t* pkt_rx = (vote_packet_t*)payload_rx;
    bool changed          = false;

#if V_ENABLE_LATE_COMMIT
    if (!chaos_has_transmitted() && comm_state->v && (comm_state->p == 0) && (pkt_rx->p > 0) ) {
        comm_state->p = pkt_rx->p;
        pkt->p        = pkt_rx->p;

        // Cast vote for just discovered designated leader
        SET_FLAG(pkt->V, NODE_ID);
    }
#endif /* V_ENABLE_LATE_COMMIT */

    // Check whether packet should be merged
    if ( (pkt_rx->p == pkt->p) || (pkt_rx->p == 0) || (pkt->p == 0) ) {
        pkt_merged_cnt++;

        if (pkt->p == 0) {
            // Adopt known proposer
            // comm_state->p remains as is, as no vote has been cast in this election (late commit may not be possible)
            pkt->p = pkt_rx->p;
        }

        // Merge the votes
        changed |= merge_vote_arrays(pkt->V, pkt_rx->V);

        // Merge the requests
        changed |= merge_request_values(pkt, pkt_rx);

#if VOTING_ADD_NETWORK_SIZE
        // Update the network size
        if (pkt->N_net < pkt_rx->N_net) {
            pkt->N_net = pkt_rx->N_net;
            changed    = true;
        }
#endif /* VOTING_ADD_NETWORK_SIZE */

#if VOTING_ADD_DRIFT_COMPENSATION
        // Merge the min and max drift values
        pkt->drift_min = MIN(pkt->drift_min, pkt_rx->drift_min);
        pkt->drift_max = MAX(pkt->drift_max, pkt_rx->drift_max);
#endif /* VOTING_ADD_DRIFT_COMPENSATION */
    } else {
        LOG_WARNING("Wrong designated leader (rcvd %2hhu but expected %2hhu)", pkt_rx->p, pkt->p);
    }

#if VOTING_ADD_PAYLOAD_CRC
    // Update CRC
    pkt->crc32 = crc32((uint8_t*)pkt, sizeof(vote_packet_t) - sizeof(uint32_t), 0);
#endif /* VOTING_ADD_PAYLOAD_CRC */

    switch (comm_state->network_state) {
        default:
            break;
    }

    return changed;
}

/* Determines whether a Vote packet is complete. */
static chaos_termination_result_t voting_termination_fn(const uint8_t* payload, const uint8_t size)
{
    // Check packet size
    if (size != sizeof(vote_packet_t)) {
        LOG_ERROR("Wrong size of local pkt (%hhuB)", size);
        return CHAOS_PACKET_NOT_COMPLETE;
    }

    // Check if all the expected nodes in the network participated
    vote_packet_t* pkt = (vote_packet_t*)payload;

#if VOTING_ADD_NETWORK_SIZE
    if (get_nr_of_contributors(pkt) < pkt->N_net) {
#else
    if (get_nr_of_contributors(pkt) < comm_state->N) {
#endif /* VOTING_ADD_NETWORK_SIZE */
        return CHAOS_PACKET_NOT_COMPLETE;
    }

    switch (comm_state->network_state) {
        default:
            break;
    }

    return CHAOS_PACKET_COMPLETE_BUT_KEEP_TRANSMITTING;
}

void run_voting_slot(comm_state_t* state)
{
    comm_state         = state;
    pkt_rcvd_cnt       = 0;
    pkt_merged_cnt     = 0;
    filter_applied_cnt = 0;
    filter_passed_cnt  = 0;
    memset(&v_packet, 0, sizeof(vote_packet_t));

    chaos_initiate_type_t initiate_slot = (state->network_state == STATE_RECOVERY) ? CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT : CHAOS_INITIATE_WITH_OTHERS;
    int8_t                curr_request  = 0;

    // Initialize the packet
    v_packet.type   = PACKET_TYPE_VOTE;
    v_packet.id_src = NODE_ID;
    v_packet.p      = state->p;

    // Set vote
    init_vote_values(&v_packet, state->v, state->p);

#if V_YIELD_AFTER_NEW_PROPOSAL
    if (comm_state->is_elected && (comm_state->p > 0) ) {
        // Yield if already elected in previous election
        comm_state->is_elected = false;
        merge_vote_arrays(v_packet.V, comm_state->V_rcvd);
        HAS_YIELDED_IND();
    }
#endif /* V_YIELD_AFTER_NEW_PROPOSAL */

    // Set request
#if S_SCHEDULE_REQUEST_DELTAS
    // Prevent an overflow if the desired change is not within [SLOT_COUNT_REQ_MIN, SLOT_COUNT_REQ_MAX]
    uint8_t curr_nr_slots = schedule_get_nr_slots(state->S, NODE_ID);
    if (state->d > curr_nr_slots) {
        curr_request = MIN(SLOT_COUNT_REQ_MAX,         state->d - curr_nr_slots);
    } else {
        curr_request = MAX(SLOT_COUNT_REQ_MIN, (int8_t)state->d - curr_nr_slots);
    }
#else
    curr_request = MIN(SLOT_COUNT_REQ_MAX, state->d);
#endif /* S_SCHEDULE_REQUEST_DELTAS */
    init_request_values(&v_packet, curr_request);

#if VOTING_ADD_NETWORK_SIZE
    v_packet.N_net = comm_state->N_net;
#endif /* VOTING_ADD_NETWORK_SIZE */

#if VOTING_ADD_DRIFT_COMPENSATION
    double   clock_offset_delta   = get_accumulated_hstimer_correction();
    uint64_t measurement_duration = get_drift_measurement_duration_hs();
    double   computed_drift       = compute_new_drift(clock_offset_delta, measurement_duration);

    v_packet.drift_min = (int16_t)(computed_drift * V_PACKET_DRIFT_SCALE);
    v_packet.drift_max = (int16_t)(computed_drift * V_PACKET_DRIFT_SCALE);
#endif /* VOTING_ADD_DRIFT_COMPENSATION */

#if VOTING_ADD_PAYLOAD_CRC
    v_packet.crc32 = crc32((uint8_t*)&v_packet, sizeof(vote_packet_t) - sizeof(uint32_t), 0);
#endif /* VOTING_ADD_PAYLOAD_CRC */

    if (curr_request) {
#if S_SCHEDULE_REQUEST_DELTAS
        LOG_INFO("Requested change: %2hhi", curr_request);
#else
        //LOG_INFO("Requested slots: %hhu", curr_request);
#endif /* S_SCHEDULE_REQUEST_DELTAS */
    }

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    chaos_set_tx_power(USED_RADIO_TX_POWER);

    // Adjust RxBoosted
    radio_set_rx_gain(USED_RADIO_TX_POWER > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

#if CHANGE_FRQ_BAND_ENABLE
    // Set the frequency band
    chaos_set_band(USED_RADIO_FRQ_BAND(state->freq_band));
#endif /* CHANGE_FRQ_BAND_ENABLE */

    chaos_start((uint8_t*)&v_packet,
                sizeof(vote_packet_t),
                voting_merge_fn,
                voting_termination_fn,
                voting_message_filter,
                initiate_slot,
                LPTIMER_TICKS_TO_HS_TIMER(state->t_slot)                         + CHAOS_SLOT_START_OFFSET_HS,
                LPTIMER_TICKS_TO_HS_TIMER(state->t_slot + CHAOS_DURATION_MAX_LP) + CHAOS_SLOT_START_OFFSET_HS,
#if V_USE_AS_REFERENCE
                true,
                CHAOS_TIMESYNC_FULL,
                true);
#else
                false,
                CHAOS_TIMESYNC_NONE,
                false);
#endif /* V_USE_AS_REFERENCE */

    comm_task_sleep_lp(CHAOS_DURATION_MAX_LP);

    if (chaos_stop()) {
        LOG_INFO("Voting successful");
    } else {
        LOG_INFO("Voting not successful");
    }

    // Process the result of the voting
    uint8_t nr_votes_tot = get_nr_of_contributors(&v_packet);
    uint8_t nr_votes_pos = get_nr_of_set_flags(v_packet.V);

    double  frac_pos_votes  = (double)nr_votes_pos * 100 / nr_votes_tot;
    uint8_t thres_pos_votes = ( (uint16_t)(100 - PARAM_SUCCESS_O2A) * state->N + 99) / 100; // Make sure that threshold is rounded up (as this is not a probabilistic assumption but a fixed fraction)

    bool passed_q_a      = use_q_a      && (nr_votes_pos   > state->N/2);
    bool passed_q_r      = use_q_r      && (frac_pos_votes > V_ELECT_POS_FRAC_THRESHOLD);
    bool passed_q_a_plus = use_q_a_plus && (nr_votes_pos   > thres_pos_votes);

    // OR enabled quora
    if (!(passed_q_a || passed_q_r || passed_q_a_plus) ) {
        if (state->l && (state->p == NODE_ID) ) {
            LOG_VERBOSE("Lost election (%2hhu (%5.1f%%) votes by %2hhu nodes)", nr_votes_pos, frac_pos_votes, state->N);
            IS_FOLLOWING_IND();
        }

        state->l = 0;
    } else if (state->l) {
        LOG_INFO("Won  election (%2hhu (%5.1f%%) votes by %2hhu nodes)", nr_votes_pos, frac_pos_votes, state->N);

        // Mark the node as an elected leader
        state->is_elected    = true;
        state->rcvd_proposal = true;

        // Extract received demand
        get_request_values(&v_packet, state->D_rcvd);

        // Extract received votes
        memcpy(state->V_rcvd, v_packet.V, NR_NODE_FLAG_BYTES);

#if V_YIELD_AFTER_CAST_VOTES
        if ( ( (nr_votes_tot - nr_votes_pos) * 100 / nr_votes_tot) > V_YIELD_AFTER_CAST_VOTES) {
            // Yield if other nodes have already received votes (observed cast votes)
            state->is_elected = false;
            memset(state->V_rcvd, 0, NR_NODE_FLAG_BYTES);
            LOG_INFO("Yielded as encountered %hhu cast votes", nr_votes_tot - nr_votes_pos);
            HAS_YIELDED_IND();
        } else
#endif /* V_YIELD_AFTER_CAST_VOTES */
        {
            IS_ELECTED_IND();
        }
    }

    // Adjust state at the end of the election
    if (state->p > 0) {
        if (state->v) {
            // Store voted node for printing
            state->p_voted = state->p;
            LOG_VERBOSE("Voted in election %hhu for %2hhu", state->e, state->p_voted);
        }

        state->v = false;
    }

    // Copy state from packet to permanent state
    get_request_values(&v_packet, state->D);
#if VOTING_ADD_NETWORK_SIZE
    state->N_net = v_packet.N_net;
#endif /* VOTING_ADD_NETWORK_SIZE */

    // Store received values
    merge_request_arrays(state->D_tot, state->D);
    state->e++;

#if VOTING_ADD_DRIFT_COMPENSATION
    if (!is_schedule_empty(state->S) ) {
        // Update the drift compensation factor such that the drift compensation does not get higher and higher in the network
        update_drift_compensation(computed_drift,
                                  ( (double)v_packet.drift_min / V_PACKET_DRIFT_SCALE),
                                  ( (double)v_packet.drift_max / V_PACKET_DRIFT_SCALE));
    } else {
        // If the node did not yet receive the schedule, we do not update the drift compensation factor
        set_drift_compensation_reference(false);
    }
#endif /* VOTING_ADD_DRIFT_COMPENSATION */

    // Update the offset of the lptimer in case the hstimer was corrected during the Voting slot
    update_clock_offset();

    // Update the network state
    switch (state->network_state) {
        case STATE_NETWORK_FORMATION:
            if (nr_votes_tot > 1) {
                LOG_INFO("Rcvd votes from %hhu nodes", nr_votes_tot - 1);
                enter_normal(state);
            }
            break;
        default:
            break;
    }

    // Correct next slot start time, since Voting uses more than one slot (if NR_SLOTS_V > 1)
    state->t_slot += (NR_SLOTS_V - 1) * SLOT_DURATION_LP;

#if V_SKIP_ELECTION_AFTER_VOTING
    if ( !state->v && ( (use_q_a || use_q_a_plus) && !use_q_r) ) {
        // Skip the remaining elections during the Consensus phase
        uint8_t elections_remaining  = NR_ELECTIONS_PER_ROUND - (state->e - 1);
        state->t_slot               += elections_remaining * (NR_SLOTS_P + NR_SLOTS_V) * SLOT_DURATION_LP;

        LOG_VERBOSE("Skipping remaining %hu elections after voting", elections_remaining);
    }
#elif V_SKIP_ELECTION_AFTER_MAJORITY_VOTING
    if (nr_votes_tot > state->N/2) {
        // Skip the remaining elections during the Consensus phase
        uint8_t elections_remaining  = NR_ELECTIONS_PER_ROUND - (state->e - 1);
        state->t_slot               += elections_remaining * (NR_SLOTS_P + NR_SLOTS_V) * SLOT_DURATION_LP;

        LOG_VERBOSE("Skipping remaining %hu elections after majority voted", elections_remaining);
    }
#endif /* V_SKIP_ELECTION_* */

    // Prevent division by zero when printing debug output
    uint16_t tot_applied = filter_applied_cnt;
    uint16_t tot_rcvd    = pkt_rcvd_cnt;
    if (tot_applied == 0) { tot_applied = 1; }
    if (tot_rcvd    == 0) { tot_rcvd    = 1; }
    LOG_VERBOSE("Filtering: %2hu (%3hu%%) out of %2hu pkts passed", filter_passed_cnt, filter_passed_cnt * 100 / tot_applied, filter_applied_cnt);
    LOG_VERBOSE("Merging:   %2hu (%3hu%%) out of %2hu pkts merged", pkt_merged_cnt,    pkt_merged_cnt    * 100 / tot_rcvd,    pkt_rcvd_cnt);
    if (pkt_rcvd_cnt > filter_applied_cnt) {
        LOG_WARNING("Only applied filtering to %hu (%hu%%) out of %hu pkts and discarded %hu invalid pkts", filter_applied_cnt, filter_applied_cnt * 100 / tot_rcvd, pkt_rcvd_cnt, pkt_rcvd_cnt - filter_applied_cnt);
    }

    uint8_t request_flags[NR_NODE_FLAG_BYTES] = {0};
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
        request_flags[i / 8] |= (get_request_value(&v_packet, i + 1) != SLOT_COUNT_NEUTRAL_ELEMENT) << (i % 8);
    }

    PRINT_FLAGS("Votes collected", request_flags);
    PRINT_FLAGS("Votes received ", v_packet.V);
}
