/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Communication
 *
 * This code implements the Communication slot, which is used to send and receive application data.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static comm_state_t*          comm_state;
static communication_packet_t c_packet;


/* Functions -----------------------------------------------------------------*/

/* Filters packets from other networks. */
bool communication_filter_fn(const uint8_t* gloria_hdr, uint8_t hdr_len, const uint8_t* payload, uint8_t size)
{
    communication_packet_t* pkt = (communication_packet_t*)payload;

#if DROP_NODE_PACKETS_ENABLE
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(pkt->id_src) && ( (random_rand32() % 100) < DROP_NODE_PACKETS_PROBABILITY)) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (USED_RADIO_TX_POWER == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */

    if (size < C_HEADER_LENGTH) {
        LOG_WARNING("Wrong size (%hhuB)", size);
        return false;
    }

    // Check if the sender is correct
    if        (pkt->type != PACKET_TYPE_COMMUNICATION) {
        LOG_ERROR("Wrong type (rcvd type %hhu but expected %hhu)", pkt->type, PACKET_TYPE_COMMUNICATION);
        return false;
#if FILTER_PACKETS_FROM_WRONG_SENDER
    } else if (pkt->id_src != comm_state->slot_next.sender) {
        LOG_ERROR("Wrong sender (rcvd from %2hhu but expected %2hhu)", pkt->id_src, comm_state->slot_next.sender);
        return false;
#endif /* FILTER_PACKETS_FROM_WRONG_SENDER */
#if FILTER_PACKETS_FROM_WRONG_NETWORK
    } else if ( (pkt->id_leader != comm_state->p) || !IS_FLAG_SET(pkt->M, NODE_ID)) {
        LOG_INFO("Wrong source network with leader %2hhu", pkt->id_leader);
        return false;
#endif /* FILTER_PACKETS_FROM_WRONG_NETWORK */
    }

    // Check whether the timestamp of the packet is from the current slot
    uint64_t pkt_timestamp = extract_timestamp(gloria_hdr, hdr_len + size);
    if (pkt_timestamp == 0) {
        LOG_WARNING("Failed to extract timestamp from pkt from node %hhu", pkt->id_src);
        return false;
    } else if ( (pkt_timestamp - LPTIMER_TICKS_TO_HS_TIMER(comm_state->t_slot) ) > LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP)) {
        LOG_WARNING("Wrong timestamp from node %hhu", pkt->id_src);
        return false;
    }

    return true;
}

void run_communication_slot(comm_state_t* state)
{
    // Store the pointer to the comm_state such that the packet filter function has access to it
    comm_state = state;

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    gloria_set_tx_power(USED_RADIO_TX_POWER);

    // Adjust RxBoosted
    radio_set_rx_gain(USED_RADIO_TX_POWER > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

#if CHANGE_FRQ_BAND_ENABLE
    // Set the frequency band
    gloria_set_band(USED_RADIO_FRQ_BAND(state->freq_band));
#endif /* CHANGE_FRQ_BAND_ENABLE */

    // Make sure that schedule is valid
    if ( (state->slot_next.sender == 0) || is_schedule_empty(state->S) )  {
        LOG_ERROR("Tried to execute empty Scheduling slot");
        return;
    }

    if (state->slot_next.sender == NODE_ID) {
        // Broadcast data

        // Prepare the packet
        c_packet.type      = PACKET_TYPE_COMMUNICATION;
        c_packet.id_src    = NODE_ID;
        c_packet.id_leader = state->p;
        memcpy(c_packet.M,       state->M, NR_NODE_FLAG_BYTES);
        memset(c_packet.payload, NODE_ID,  C_PAYLOAD_LENGTH);

        // Start initiating a Gloria flood
        gloria_start(true,
                     (uint8_t*)&c_packet,
                     C_HEADER_LENGTH + C_PAYLOAD_LENGTH,
                     GLORIA_MAX_RETRANSMISSIONS,
                     C_USE_AS_REFERENCE);
        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
        gloria_stop();
        LOG_VERBOSE("C sent");

    } else {
        // Listen for data

        // Start listening for Gloria floods
        gloria_set_pkt_filter(communication_filter_fn);
        gloria_start(false,
                     (uint8_t*)&c_packet,
                     C_HEADER_LENGTH + C_PAYLOAD_LENGTH,
                     GLORIA_MAX_RETRANSMISSIONS,
                     C_USE_AS_REFERENCE);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP + GLORIA_GUARD_TIME_LP);

        // Process the packet if one was received
        if (gloria_stop()) {
            LOG_VERBOSE("C rcvd   from %2hhu with %2hhuB of payload in %hhu hops", c_packet.id_src, gloria_get_payload_len() - C_HEADER_LENGTH, gloria_get_rx_index() + 1);

#if C_USE_AS_REFERENCE
            // Correct the clock offset
            uint64_t timestamp_rx;
            if (gloria_get_received_timestamp((uint8_t*)&timestamp_rx)) {
                //LOG_VERBOSE("Successfully synced to node %i", state->slot_next.sender);
            } else {
                LOG_WARNING("Failed to sync      to node %i", state->slot_next.sender);
            }

            // Adjust HS timer
            double delta;
            if (gloria_get_t_ref_hs() < timestamp_rx) {
                delta =   (double)(timestamp_rx          - gloria_get_t_ref_hs());
            } else {
                delta = - (double)(gloria_get_t_ref_hs() - timestamp_rx);
            }

            hs_timer_adapt_offset(delta);
            if        (ABS(delta) >  SYNC_THRESHOLD_GLORIA_WARN_HS) {
                LOG_WARNING("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t)delta));
            } else if (ABS(delta) >= SYNC_THRESHOLD_GLORIA_INFO_HS) {
                LOG_VERBOSE("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t)delta));
            }

            // Sync LP to HS timer
            update_clock_offset();
#endif /* C_USE_AS_REFERENCE */

            switch (state->network_state) {
                default:
                    break;
            }
        } else {

            switch (state->network_state) {
                case STATE_NORMAL:
                    LOG_VERBOSE("C missed from %2hhu", state->slot_next.sender);
                    break;
                default:
                    break;
            }
        }

        /* Higher-layer application could process the data here using c_packet->payload */
    }
}
