/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Scheduling
 *
 * The leader broadcasts and other nodes receive the schedule in this slot.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static comm_state_t*     comm_state;
static schedule_packet_t s_packet;
static discover_packet_t d_packet;
static uint8_t           rx_packet[MAX(sizeof(schedule_packet_t), sizeof(discover_packet_t))] = {0};


/* Functions -----------------------------------------------------------------*/

/* Filters packets other than Schedule or Discover packets. */
bool scheduling_filter_fn(const uint8_t* gloria_hdr, uint8_t hdr_len, const uint8_t* payload, uint8_t size)
{
#if DROP_NODE_PACKETS_ENABLE
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(pkt->id_src) && ( (random_rand32() % 100) < DROP_NODE_PACKETS_PROBABILITY)) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (USED_RADIO_TX_POWER == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */

    // Check if the packet is a Schedule packet
    if ( ((schedule_packet_t*)payload)->type == PACKET_TYPE_SCHEDULE) {
        schedule_packet_t* pkt = (schedule_packet_t*)payload;

        // Check packet size
        if (size != sizeof(schedule_packet_t)) {
            LOG_VERBOSE("Wrong size (%hhuB)", size);
            return false;
#if FILTER_PACKETS_FROM_WRONG_SENDER
        } else if ( (pkt->id_src != comm_state->p_voted) && (comm_state->p_voted > 0) ) {
            LOG_WARNING("Unexpected sender (rcvd from %2hhu but expected %2hhu (leader) )", pkt->id_src, comm_state->p_voted);
            // We do not reject a schedule because it does come from another leader, as the goal is to communicate as often as possible
#endif /* FILTER_PACKETS_FROM_WRONG_SENDER */
        } else if (!IS_FLAG_SET(pkt->M, NODE_ID)) {
            LOG_INFO("Unexpected recipient (rcvd from leader %2hhu but was not part of the network)", pkt->id_src);
            // We do not reject a schedule because it does come from another network, as the goal is to communicate as often as possible
        }
    } else if ( ((discover_packet_t*)payload)->type == PACKET_TYPE_DISCOVER) {

        // Check packet size
        if (size != sizeof(discover_packet_t)) {
            LOG_VERBOSE("Wrong size (%hhuB)", size);
            return false;
        }
    } else {
        LOG_VERBOSE("Wrong type (rcvd type %hhu but expected {%hhu,%hhu})", ((schedule_packet_t*)payload)->type, PACKET_TYPE_SCHEDULE, PACKET_TYPE_DISCOVER);
        return false;
    }

    // Check whether the timestamp of the packet is from the current slot
    uint64_t pkt_timestamp = extract_timestamp(gloria_hdr, hdr_len + size);
    if (pkt_timestamp == 0) {
        LOG_WARNING("Failed to extract timestamp");
        return false;
    } else if ( (pkt_timestamp - LPTIMER_TICKS_TO_HS_TIMER(comm_state->t_slot) ) > LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP) ) {
        LOG_VERBOSE("Wrong timestamp");
        return false;
    }

    return true;
}

void run_scheduling_slot(comm_state_t* state) {
    // Store the pointer to the comm_state such that the packet filter function has access to it
    comm_state = state;

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    gloria_set_tx_power(USED_RADIO_TX_POWER);

    // Adjust RxBoosted
    radio_set_rx_gain(USED_RADIO_TX_POWER > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

#if CHANGE_FRQ_BAND_ENABLE
    // Set the frequency band
    gloria_set_band(USED_RADIO_FRQ_BAND(state->freq_band));
#endif /* CHANGE_FRQ_BAND_ENABLE */

    // Remember whether the node was instructed to switch network
    bool merge_with_discovered = false;
#if CHANGE_ROUND_PERIOD_ENABLE
    uint8_t T_prev             = state->T;
#endif /* CHANGE_ROUND_PERIOD_ENABLE */

    // Determine next members and empirical network size
    uint8_t N_empirical = 0;
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
        state->M[i / 8] |= (state->D_tot[i] != SLOT_COUNT_NEUTRAL_ELEMENT) << (i % 8);
        N_empirical     += (state->D_tot[i] != SLOT_COUNT_NEUTRAL_ELEMENT);
    }

#if S_SCHEDULE_USE_HISTORY
    schedule_update_history(state->D_tot, state->D_validity, state->D_hist);
#endif /* S_SCHEDULE_USE_HISTORY */

    // Leader broadcasts the schedule
    if (state->is_elected) {
        if (state->net_discovered && IS_DOMINATED(NODE_ID, state->N, state->id_discovered, state->N_d) ) {
            // Notify network to disband
            merge_with_discovered = true;
            state->is_elected     = false;

            // Initialise the Discover packet
            d_packet.type   = PACKET_TYPE_DISCOVER;
            d_packet.id_src = state->id_discovered;
            d_packet.T      = state->T_d;
            d_packet.r      = state->r_d;
            d_packet.t      = state->t_d;

            int64_t delta_rx = (int64_t)lptimer_now() - state->t_d_rx;
#if D_LOG_TIMESTAMPS
            if ( (delta_rx < 0) || (delta_rx > (NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) ) {
                LOG_ERROR("Expected (small) positive delta but encountered %lli for lp=%llu/t_rx=%llu (%lluus)", delta_rx, lptimer_now(), state->t_d_rx, LPTIMER_TICKS_TO_US(state->t_d_rx));
            } else if (state->t_d > (2 * NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) {
                LOG_ERROR("Corrupted timestamp t=%llu", state->t_d);
            } else {
#else
            {
#endif /* D_LOG_TIMESTAMPS */
                d_packet.t += delta_rx;
            }
#if DISCOVERY_ADD_NETWORK_SIZE
            d_packet.N      = state->N_d;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */

#if D_LOG_TIMESTAMPS
            LOG_VERBOSE("N info: t=%lluus/t_rx=%lluus/T=%hhu", LPTIMER_TICKS_TO_US(d_packet.t), LPTIMER_TICKS_TO_US(state->t_d_rx), d_packet.T);
#endif /* D_LOG_TIMESTAMPS */

            // Start the Gloria flood
            gloria_start(true,
                         (uint8_t*)&d_packet,
                         sizeof(discover_packet_t),
                         GLORIA_MAX_RETRANSMISSIONS,
                         1);

            comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
            gloria_stop();
            LOG_VERBOSE("N sent to join leader %2hhu", d_packet.id_src);
        } else {
            // Transmit schedule

#if CHANGE_ROUND_PERIOD_ENABLE
            state->T = (CHANGE_ROUND_PERIOD_PIN_VALUE ? CHANGE_ROUND_PERIOD_PIN_HIGH : CHANGE_ROUND_PERIOD_PIN_LOW);
#endif /* CHANGE_ROUND_PERIOD_ENABLE */

            // Determine next schedule
            schedule_t S_next[NR_SLOTS_C] = {0};
#if S_SCHEDULE_USE_HISTORY
            if(merge_request_arrays(state->D_rcvd, state->D_hist)) {
                LOG_VERBOSE("Included historical demand for scheduling");
            }
#endif /* S_SCHEDULE_USE_HISTORY */
            schedule_compute(state, state->D_rcvd, S_next);

            // Update network size
            state->N = update_network_size(state->N, state->N_net, N_empirical);

            // Set correct state (as might not have been part of the last election during the Consensus phase); makes sure that the leader is again the designated leader in the first election of the next round
            state->p = NODE_ID;
            state->l = true;

            // Initialise the Schedule packet
            s_packet.type   = PACKET_TYPE_SCHEDULE;
            s_packet.id_src = NODE_ID;
            s_packet.T      = state->T;
#if SCHEDULING_ADD_NETWORK_SIZE
            s_packet.N      = state->N;
#endif /* SCHEDULING_ADD_NETWORK_SIZE */
            memcpy(s_packet.M, state->M, NR_NODE_FLAG_BYTES);
#if S_SCHEDULE_COMPRESS
            schedule_pack(S_next, &s_packet);
#else
            memcpy(s_packet.S, S_next, NR_SLOTS_C);
#endif /* S_SCHEDULE_COMPRESS */

            // Update locally
            schedule_update(state, &s_packet);

            // Start the Gloria flood
            gloria_start(true,
                         (uint8_t*)&s_packet,
                         sizeof(schedule_packet_t),
                         GLORIA_MAX_RETRANSMISSIONS,
                         true);

            comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
            gloria_stop();
            LOG_VERBOSE("S sent");
        }
    } else {
        // Receive schedule / notification
        LOG_VERBOSE("Listening for S / N");

        gloria_set_pkt_filter(scheduling_filter_fn);
        gloria_start(false,
                     (uint8_t*)&rx_packet,
                     MAX(sizeof(schedule_packet_t), sizeof(discover_packet_t)),
                     GLORIA_MAX_RETRANSMISSIONS,
                     true);

        comm_task_sleep_lp(S_INIT_TIME_LP + GLORIA_MAX_DURATION_LP + GLORIA_GUARD_TIME_LP);

        // Update the schedule if one was received
        if (gloria_stop()) {
            char* pkt_type_str = "INVALID";
            node_id_t id_src   = 0;
            if        ( ((schedule_packet_t*)rx_packet)->type == PACKET_TYPE_SCHEDULE) {
                pkt_type_str = "S";
                id_src       = ((schedule_packet_t*)rx_packet)->id_src;
            } else if ( ((discover_packet_t*)rx_packet)->type == PACKET_TYPE_DISCOVER) {
                pkt_type_str = "N";
                id_src       = ((discover_packet_t*)rx_packet)->id_src;
            } else {
                LOG_ERROR("Unknown pkt rcvd while waiting for S / N");
            }

            LOG_INFO("%s rcvd  from %2hhu", pkt_type_str, id_src);
            IS_FOLLOWING_IND();

            // Correct the clock offset
            uint64_t timestamp_rx;
            if (gloria_get_received_timestamp((uint8_t *) &timestamp_rx)) {
                //LOG_VERBOSE("Successfully synced to %s", pkt_type_str);
            } else {
                LOG_WARNING("Failed to sync to %s", pkt_type_str);
            }

            // Adjust HS timer
            double delta;
            if (gloria_get_t_ref_hs() < timestamp_rx) {
                delta =  (double) (timestamp_rx          - gloria_get_t_ref_hs());
            } else {
                delta = -(double) (gloria_get_t_ref_hs() - timestamp_rx);
            }

            hs_timer_adapt_offset(delta);
            if (ABS(delta) > SYNC_THRESHOLD_GLORIA_WARN_HS) {
                LOG_WARNING("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t) delta));
            } else if (ABS(delta) >= SYNC_THRESHOLD_GLORIA_INFO_HS) {
                LOG_VERBOSE("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t) delta));
            }

            // Sync LP to HS timer
            update_clock_offset();

            if ( ((schedule_packet_t*)rx_packet)->type == PACKET_TYPE_SCHEDULE) {
                schedule_packet_t* pkt = (schedule_packet_t*)rx_packet;

                state->p = pkt->id_src;
                state->T = pkt->T;
#if SCHEDULING_ADD_NETWORK_SIZE
                state->N = pkt->N;
#endif /* SCHEDULING_ADD_NETWORK_SIZE */

                // Update the node flags
                memcpy(state->M, pkt->M, NR_NODE_FLAG_BYTES);

                // Update schedule based on received packet
                schedule_update(state, pkt);
            } else if ( ((discover_packet_t*)rx_packet)->type == PACKET_TYPE_DISCOVER) {
                discover_packet_t* pkt = (discover_packet_t*)rx_packet;

                // ATTENTION: gloria_get_t_ref() cannot be used directly, as its timestamp is occasionally corrupted
                state->t_d_rx        = corrected_to_uncorrected_lp_timestamp(HS_TIMER_TICKS_TO_LPTIMER(gloria_get_t_ref_hs()));

                state->id_discovered = pkt->id_src;
                state->T_d           = pkt->T;
                state->r_d           = pkt->r;
                state->t_d           = pkt->t + D_TX_DELAY_LP(gloria_get_rx_index());
#if DISCOVERY_ADD_NETWORK_SIZE
                state->N_d           = pkt->N;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */

#if D_LOG_TIMESTAMPS
                LOG_VERBOSE("N info: t=%lluus/t_rx=%lluus/T=%hhu/lp=%lluus", LPTIMER_TICKS_TO_US(state->t_d), LPTIMER_TICKS_TO_US(state->t_d_rx), state->T_d, LPTIMER_TICKS_TO_US(lptimer_now()));

                int64_t delta_rx = (int64_t)lptimer_now() - state->t_d_rx;
                if ( (delta_rx < 0) || (delta_rx > (NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) ) {
                    LOG_ERROR("Expected (small) positive delta but encountered %lli for lp=%llu/t_rx=%llu (%lluus)", delta_rx, lptimer_now(), state->t_d_rx, LPTIMER_TICKS_TO_US(state->t_d_rx));
                }
#endif /* D_LOG_TIMESTAMPS */

                // Disband and merge
                merge_with_discovered = true;
            }

            switch (state->network_state) {
#if ENABLE_RECOVERY_ON_SCHEDULING
                case STATE_RECOVERY:
                    state->l = 0;
                    if        ( ((schedule_packet_t*)rx_packet)->type == PACKET_TYPE_SCHEDULE) {
#if SCHEDULING_ADD_NETWORK_SIZE
                        state->N = ((schedule_packet_t*)rx_packet)->N;
#endif /* SCHEDULING_ADD_NETWORK_SIZE */
                        LOG_INFO("Rcvd S from %hhu", ((schedule_packet_t*)rx_packet)->id_src);
                    } else if ( ((discover_packet_t*)rx_packet)->type == PACKET_TYPE_DISCOVER) {
#if DISCOVERY_ADD_NETWORK_SIZE
                        state->N = ((discover_packet_t*)rx_packet)->N;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */
                        LOG_INFO("Rcvd N from %hhu", ((discover_packet_t*)rx_packet)->id_src);
                    }
                    enter_normal(state);
                    break;
#endif /* ENABLE_RECOVERY_ON_SCHEDULING */
                case STATE_NETWORK_FORMATION:
                    if        ( ((schedule_packet_t*)rx_packet)->type == PACKET_TYPE_SCHEDULE) {
                        LOG_INFO("Rcvd S from %hhu", ((schedule_packet_t*)rx_packet)->id_src);
                    } else if ( ((discover_packet_t*)rx_packet)->type == PACKET_TYPE_DISCOVER) {
                        LOG_INFO("Rcvd N from %hhu", ((discover_packet_t*)rx_packet)->id_src);
                    }
                    enter_normal(state);
                    break;
                default:
                    break;
            }
        } else {
            LOG_WARNING("S missed from %2hhu", state->p_voted);

            // Invalidate schedule
            memset(state->S, 0, NR_SLOTS_C);

            state->N = update_network_size(state->N, state->N_net, N_empirical);

            // Invalidate node flags
            memset(state->M, 0, NR_NODE_FLAG_BYTES);

            // Set leader ID to unknown for printing (not required for protocol logic)
            state->p = 0;

            STATE_RECEIVING_IND();
        }
    }

#if CHANGE_ROUND_PERIOD_ENABLE
    // Adjust clocks after the schedule has been sent so that the packet is accepted by the network
    if (state->T != T_prev) {
        adjust_timers(&state->t_slot, &state->t_round, T_prev, state->T);
    }
#endif /* CHANGE_ROUND_PERIOD_ENABLE */

    // In case information about a discovered network has been distributed and the leader instructed the network to disband, do so
    if (merge_with_discovered) {
        LOG_INFO("Disbanding and joining network of leader %2hhu", state->id_discovered);
        enter_recovery(state, true);
    }

    // Print information about the current schedule
    if (is_schedule_empty(state->S)) {
        LOG_INFO("S for round %3hu: not rcvd", state->r);
    } else {
        LOG_VERBOSE("S for round %3hu: %8x", state->r, crc32(state->S, NR_SLOTS_C, 0));
    }
}
