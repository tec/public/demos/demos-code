/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slots
 *
 * This file contains some helper functions used for analysing received packets and node flags.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static node_id_t rand_sequence[MAX_NR_NODES];
static uint8_t   rand_sequence_len = MAX_NR_NODES;

static volatile uint32_t seed_sequence = SEED_RAND_SEQUENCE;

/* Functions -----------------------------------------------------------------*/

void run_end_of_round(comm_state_t* state)
{
    // Update the duty cycle counters
    comm_task_update_dc();

    LOG_VERBOSE("State at EoR - r=%3hu/N=%2hhu/s=%hhu/d=%hhu/v=%hhu/p=%2hhu/l=%hhu", state->r, state->N, state->network_state, state->d, state->v, state->p, state->l);

    // Prepare variables for new round
    // rcvd_proposal is set below
    state->is_elected    = false;
    state->v             = true;
    state->r++;
    state->e             = 1;

    // Check if node needs to switch to recovery protocol
    if (state->rcvd_proposal) {
        state->r_unsynced    = 0;
        state->rcvd_proposal = false;
    } else {
        if        (state->network_state == STATE_NORMAL) {
            state->r_unsynced++;

            if (state->r_unsynced >= NR_ROUNDS_TO_UNSYNC) {
                enter_recovery(state, false);
            }
        } else if (state->network_state == STATE_RECOVERY) {
            state->r_recovering++;

            if (state->r_recovering >= NR_ROUNDS_TO_RECOVER) {
                // Node starts its own new network
                enter_network_formation(state, true);
            }
        }
    }

#if ENABLE_SEPARATE_FREQ_BANDS
    // Update the frequency band if a leader is known
    if (state->p > 0) {
        state->freq_band = get_designated_freq_band(state->p);
        set_radio_params(USED_RADIO_TX_POWER, state->freq_band);
    }
#endif /* ENABLE_SEPARATE_FREQ_BANDS */

    demand_update(state);

    // FreeRTOS house-keeping

    // Check whether stack usage is not excessive
    if (!rtos_check_stack_usage()) {
        LOG_ERROR("Stack usage exceeded limits");
    }
}

/* Stores a non-repeating sequence of IDs */
void generate_random_seq(uint8_t len)
{
    if (len != MAX_NR_NODES) {
        if (len > MAX_NR_NODES) {
            LOG_ERROR("Cannot generate a non-repeating sequence of length %hhu as only %hhu candidate IDs available", len, MAX_NR_NODES);
            return;
        } else {
            LOG_WARNING("Generating random sequence of length %hhu, but maximum node ID is %hhu", len, MAX_NR_NODES);
        }
    }
    rand_sequence_len = len;

    memset(rand_sequence, 0, rand_sequence_len);
#ifndef HAL_RNG_MODULE_ENABLED
    // Setup random generator - sample RNG once so state can be randomized again afterwards and seed again
    uint32_t prev_state = random_rand32();
#endif /* HAL_RNG_MODULE_ENABLED */

    // Use srand and rand to generate a pseudo-random sequence with a known seed shared across nodes
    LOG_VERBOSE("Seeding sequence with random seed %u", seed_sequence);
    srand(seed_sequence);

    // List method - Part 1
    uint8_t candidates_left = rand_sequence_len;
    node_id_t candidate_list[MAX_NR_NODES];
    for (uint8_t i = 0; i < rand_sequence_len; i++) {
        candidate_list[i] = i + 1;
    }

    for (uint8_t i = 0; i < rand_sequence_len; i++) {
        node_id_t candidate_id = 0;

        // Resampling method
        /*while (candidate_id == 0) {
            // Sample new candidate
            candidate_id = (rand() % rand_sequence_len) + 1;

            // Verify that ID has not been sampled before
            for (uint8_t j = 0; j < i; j++) {
                if (rand_sequence[j] == candidate_id) {
                    candidate_id = 0;
                    break;
                }
            }
        }*/

        // List method - Part 2
        uint8_t candidate_index = rand() % candidates_left;
        candidate_id            = candidate_list[candidate_index];
        candidates_left--;
        for (uint8_t j = candidate_index; j < candidates_left; j++) {
            candidate_list[j] = candidate_list[j+1]; // memcpy might not work as intended
        }

        // Store ID
        rand_sequence[i] = candidate_id;
    }

#ifndef HAL_RNG_MODULE_ENABLED
    // Randomize RNG again
    srand(prev_state);
#endif /* HAL_RNG_MODULE_ENABLED */

    LOG_VERBOSE("Generated random sequence: %8x", crc32(rand_sequence, sizeof(rand_sequence), 0));
}

node_id_t get_designated_leader(uint16_t r, uint8_t e)
{
    if (e == 1) {
        // Designated leader is leader from last round
        return 0;
    }

    uint16_t seq_index  = (NR_ELECTIONS_PER_ROUND - 1) * r + e;
    node_id_t leader_id = rand_sequence[seq_index % rand_sequence_len];

    //LOG_VERBOSE("Sampled designated leader %2hhu in election %hhu of round %3hu", leader_id, e, r);
    return leader_id;
}

uint8_t get_designated_freq_band(node_id_t leader)
{
#if ENABLE_SEPARATE_FREQ_BANDS
    // Get the leader's index in the random sequence
    uint32_t leader_idx;
    for (leader_idx = 0; (leader_idx < rand_sequence_len) && (rand_sequence[leader_idx] != leader); leader_idx++) {}

    if (leader_idx == rand_sequence_len) {
        LOG_ERROR("Failed to find leader in random sequence");
        return USED_RADIO_FRQ_BAND(FRQ_BAND_NORMAL);
    }

    return FREQ_BANDS[leader_idx % sizeof(FREQ_BANDS)];
#else
    return USED_RADIO_FRQ_BAND(FRQ_BAND_NORMAL);
#endif /* ENABLE_SEPARATE_FREQ_BANDS */
}

uint64_t extract_timestamp(const uint8_t* packet, const uint8_t size)
{
    uint64_t timestamp; // This timestamp should have equal length to CHAOS_TIMESTAMP_LENGTH and GLORIA_TIMESTAMP_LENGTH
    uint8_t* timestamp_ptr;

    switch (packet[0] & PROTOCOL_ID_MASK) {
        case PROTOCOL_ID_CHAOS:
            if (!( ((chaos_header_t*)packet)->sync) || size != (CHAOS_HEADER_LENGTH_WITH_TS + sizeof(vote_packet_t) ) ) {
                // There is no timestamp in the Chaos packet --> ignore
                return 0;
            }

            timestamp_ptr = ((chaos_header_t*)packet)->timestamp;
            break;
        case PROTOCOL_ID_GLORIA:
            // Check the size and whether there is a timestamp in the packet
            if (!( ((gloria_header_t*)packet)->sync) || size < (GLORIA_HEADER_LENGTH_MIN + GLORIA_TIMESTAMP_LENGTH) ) {
                // There is no timestamp in the Gloria packet --> ignore
                return 0;
            }

            timestamp_ptr = ((gloria_header_t*)packet)->min.timestamp;
            break;
        default:
            LOG_ERROR("Unknown protocol encountered");
            return 0;
    }

    memcpy((uint8_t*)&timestamp, timestamp_ptr, sizeof(uint64_t));
    return timestamp;
}

bool get_packet_time_offset(const uint8_t* packet, const uint8_t size, int64_t* time_offset_hs)
{
    // Give up if the packet is too small
    if (size < sizeof(uint64_t)) {
        return 0;
    }

    // The time offset to add to the final timestamp
    int64_t rx_offset_hs = gloria_timings[GLORIA_INTERFACE_MODULATION].txSync;

    switch (packet[0] & PROTOCOL_ID_MASK) {
        case PROTOCOL_ID_CHAOS:
            LOG_VERBOSE("Chaos msg");
            if (!( ((chaos_header_t*)packet)->sync) || size != (CHAOS_HEADER_LENGTH_WITH_TS + sizeof(vote_packet_t) ) ) {
                // There is no timestamp in the Chaos packet --> ignore
                return 0;
            }
            break;
        case PROTOCOL_ID_GLORIA:
            LOG_VERBOSE("Gloria msg");
            // Check the size and whether there is a timestamp in the packet
            if (!( ((gloria_header_t*)packet)->sync) || size < (GLORIA_HEADER_LENGTH_MIN + GLORIA_TIMESTAMP_LENGTH) ) {
                // There is no timestamp in the Gloria packet --> ignore
                return 0;
            }

            rx_offset_hs += ((gloria_header_t*)packet)->slot_index * gloria_calculate_slot_time(GLORIA_INTERFACE_MODULATION, 0, 0, size) + gloria_timings[GLORIA_INTERFACE_MODULATION].floodInitOverhead;
            break;
        default:
            LOG_ERROR("Unknown protocol encountered");
            return 0;
    }

    // Extract the timestamp included in the packet
    uint64_t packet_timestamp_hs = extract_timestamp(packet, size);
    if (packet_timestamp_hs == 0) {
        LOG_WARNING("Failed to extract timestamp");
        return 0;
    } else {
        LOG_VERBOSE("Rcvd pkt timestamp: %llums", HS_TIMER_TICKS_TO_MS(packet_timestamp_hs));
    }

    // Set return values
    *time_offset_hs = (int64_t)(packet_timestamp_hs - radio_get_last_sync_timestamp()) + rx_offset_hs;
    return 1;
}

inline uint8_t get_nr_of_contributors(const vote_packet_t* pkt) {
    uint8_t nr_nodes = 0;
    for (node_id_t node = 0; node < MAX_NR_NODES; node++) {
        if (get_request_value(pkt, node + 1) != SLOT_COUNT_NEUTRAL_ELEMENT) {
            nr_nodes++;
        }
    }

    return nr_nodes;
}

inline uint8_t get_nr_of_set_flags(const uint8_t* flags) {
    uint8_t nr_nodes = 0;
    for (node_id_t node = 0; node < MAX_NR_NODES; node++) {
        if (IS_FLAG_SET(flags, node + 1)) {
            nr_nodes++;
        }
    }

    return nr_nodes;
}

inline uint16_t update_network_size(uint16_t N, uint16_t N_net, uint16_t N_empirical) {
    uint16_t N_new = N;

    // Apply network filter
    N_new =     (PARAM_FILTER_NETWORK  * N_net       + (100 - PARAM_FILTER_NETWORK)  * N_new) / 100;

    // Apply temporal filter
    N_new = MAX((PARAM_FILTER_TEMPORAL * N_empirical + (100 - PARAM_FILTER_TEMPORAL) * N_new) / 100, N_empirical);

    if (N != N_new) {
        LOG_VERBOSE("Updated network size: %hu -> %hu", N, N_new);
    }
    return N_new;
}

bool is_flags_empty(const uint8_t* flags)
{
    // Iterate over all flag bytes and check whether they are not set
    for (uint32_t i = 0; i < NR_NODE_FLAG_BYTES; i++) {
        if (flags[i]) {
            return false;
        }
    }
    return true;
}
