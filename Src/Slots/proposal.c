/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Proposal
 *
 * The leader proposes itself as a candidate in this slot.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static comm_state_t*    comm_state;
static propose_packet_t p_packet;


/* Functions -----------------------------------------------------------------*/

/* Filters packets other than Propose packets. */
bool proposal_filter_fn(const uint8_t* gloria_hdr, uint8_t hdr_len, const uint8_t* payload, uint8_t size)
{
    propose_packet_t* pkt = (propose_packet_t*)payload;

#if DROP_NODE_PACKETS_ENABLE
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(pkt->id_src) && ( (random_rand32() % 100) < DROP_NODE_PACKETS_PROBABILITY)) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (USED_RADIO_TX_POWER == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */

    // Check packet size
    if (size != sizeof(propose_packet_t)) {
        LOG_VERBOSE("Wrong size (%hhuB)", size);
        return false;
    }

    // Check if the packet is a Propose packet
    if (pkt->type != PACKET_TYPE_PROPOSE) {
        LOG_VERBOSE("Wrong type (rcvd type %hhu but expected %hhu)", pkt->type, PACKET_TYPE_PROPOSE);
        return false;
#if FILTER_PACKETS_FROM_WRONG_SENDER
    } else if ( (pkt->id_src != comm_state->p) && (comm_state->p > 0) && (comm_state->e == 1) )  {
        LOG_WARNING("Unexpected sender (rcvd from %hhu but expected %hhu (previous   leader) )", pkt->id_src, comm_state->p);
    } else if ( (pkt->id_src != comm_state->slot_next.sender)         && (comm_state->e  > 1) )  {
        LOG_WARNING("Wrong      sender (rcvd from %hhu but expected %hhu (designated leader) )", pkt->id_src, comm_state->slot_next.sender);
        return false;
#endif /* FILTER_PACKETS_FROM_WRONG_SENDER */
    // Check whether the round and election is correct
    } else if (pkt->r != comm_state->r) {
        LOG_WARNING("Wrong round (rcvd %hu but expected %hu)", pkt->r, comm_state->r);
        return false;
    } else if (pkt->e != comm_state->e) {
        LOG_WARNING("Wrong election (rcvd %hhu but expected %hhu)", pkt->e, comm_state->e);
        return false;
    }

    // Check whether the timestamp of the packet is from the current slot
    uint64_t pkt_timestamp = extract_timestamp(gloria_hdr, hdr_len + size);
    if (pkt_timestamp == 0) {
        LOG_WARNING("Failed to extract timestamp");
        return false;
    } else if ( (pkt_timestamp - LPTIMER_TICKS_TO_HS_TIMER(comm_state->t_slot) ) > LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP) ) {
        if (comm_state->network_state == STATE_RECOVERY) {
            LOG_VERBOSE("Timestamp not yet adjusted");
            // We do not filter such packets, as the node must be able to sync to the network
        } else {
            LOG_VERBOSE("Wrong timestamp");
            return false;
        }
    }

    return true;
}

void prepare_proposal(comm_state_t* state)
{
    // Reset internal state at the beginning of the round
    state->disc_prepared = false;
    state->p_voted       = 0;
    state->t_round       = state->t_slot;

    // Set protocol variables at the beginning of the Consensus phase
    state->N_net = state->N;
    memset(state->D_rcvd, SLOT_COUNT_NEUTRAL_ELEMENT, MAX_NR_NODES);
    memset(state->D_tot,  SLOT_COUNT_NEUTRAL_ELEMENT, MAX_NR_NODES);
    memset(state->V_rcvd, 0,                          NR_NODE_FLAG_BYTES);
}

void run_proposal_slot(comm_state_t* state)
{
    // Store the pointer to the comm_state such that the packet filter function has access to it
    comm_state = state;

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    gloria_set_tx_power(USED_RADIO_TX_POWER);

    // Adjust RxBoosted
    radio_set_rx_gain(USED_RADIO_TX_POWER > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

#if CHANGE_FRQ_BAND_ENABLE
    // Set the frequency band
    gloria_set_band(USED_RADIO_FRQ_BAND(state->freq_band));
#endif /* CHANGE_FRQ_BAND_ENABLE */

    node_id_t leader_id = 0;

    // Check whether we need to initialize variables at the beginning of the round
    if (state->e == 1) {
        leader_id = state->p;

        prepare_proposal(state);

    // Check whether the current election is reserved for the previous leader
    } else if ( (2 <= state->e) && (state->e <= NR_ELECTIONS_PER_ROUND) ) {
        leader_id = get_designated_leader(state->r, state->e);
        if (leader_id == NODE_ID) {
            state->l = true;
        } else {
            state->l = false;
        }

        if (leader_id != state->slot_next.sender) {
            uint64_t global_slot_number = state->t_slot / SLOT_DURATION_LP;
            uint16_t r_scheduler        =  global_slot_number / state->T;
            uint8_t  e_scheduler        = (global_slot_number % state->T) / (NR_SLOTS_P + NR_SLOTS_V) + 1;
            node_id_t leader_scheduler  = get_designated_leader(r_scheduler, e_scheduler);
            LOG_ERROR("Expected %2hhu as designated leader for r=%3hu/e=%hhu but was assigned %2hhu (r=%3hu/e=%hhu -> p=%2hhu)", leader_id, state->r, state->e, state->slot_next.sender, r_scheduler, e_scheduler, leader_scheduler);
        }
    } else {
        LOG_ERROR("Executing election e=%hhu of %hhu elections per round", state->e, NR_ELECTIONS_PER_ROUND);
    }

    LOG_VERBOSE("Round %3hu - Election %hhu: designated leader %2hhu", state->r, state->e, leader_id);

    // Send propose message
    if ( (state->l && state->v) && (state->network_state != STATE_RECOVERY) ){
        // Node is currently designated leader

        // Initialise the Propose packet
        p_packet.type   = PACKET_TYPE_PROPOSE;
        p_packet.id_src = NODE_ID;
        p_packet.r      = state->r;
        p_packet.e      = state->e;
#if PROPOSAL_ADD_NETWORK_SIZE
        p_packet.N      = state->N;
#endif /* PROPOSAL_ADD_NETWORK_SIZE */

        // Start the Gloria flood
        gloria_start(true,
                     (uint8_t*)&p_packet,
                     sizeof(propose_packet_t),
                     GLORIA_MAX_RETRANSMISSIONS,
                     true);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
        gloria_stop();
        LOG_VERBOSE("P sent in election %hhu", state->e);

        // Set proposal value itself
        state->p = NODE_ID;
    } else {
        if (state->l) {
            LOG_INFO("Yielded as designated leader - already cast vote for %hhu", state->p_voted);
        }

        // Receive proposal
        LOG_VERBOSE("Listening for P in election %hhu", state->e);

        gloria_set_pkt_filter(proposal_filter_fn);
        gloria_start(false,
                     (uint8_t*)&p_packet,
                     sizeof(propose_packet_t),
                     GLORIA_MAX_RETRANSMISSIONS,
                     true);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP + GLORIA_GUARD_TIME_LP);

        // Handle proposal if one was received
        if (gloria_stop()) {
            LOG_INFO("P rcvd in election %hhu from %2hhu", p_packet.e, p_packet.id_src);

            // Correct the clock offset
            uint64_t timestamp_rx;
            if (gloria_get_received_timestamp((uint8_t*)&timestamp_rx)) {
                //LOG_VERBOSE("Successfully synced to P");
            } else {
                LOG_WARNING("Failed to sync to P");
            }

            // Adjust HS timer
            double delta;
            if (gloria_get_t_ref_hs() < timestamp_rx) {
                delta =   (double)(timestamp_rx          - gloria_get_t_ref_hs());
            } else {
                delta = - (double)(gloria_get_t_ref_hs() - timestamp_rx);
            }

            hs_timer_adapt_offset(delta);
            if        (ABS(delta) >  SYNC_THRESHOLD_GLORIA_WARN_HS) {
                LOG_WARNING("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t) delta));
            } else if (ABS(delta) >= SYNC_THRESHOLD_GLORIA_INFO_HS) {
                LOG_VERBOSE("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t) delta));
            }

            // Sync LP to HS timer
            update_clock_offset();

            // Update counters based on received packet in case node is out-of-sync
            if (state->r != p_packet.r) {
                LOG_WARNING("Updating r: %hu -> %hu", state->r, p_packet.r);
                state->r = p_packet.r;
            }
            if (state->e != p_packet.e) {
                LOG_WARNING("Updating e: %hhu -> %hhu", state->e, p_packet.e);
                state->e = p_packet.e;
            }

            // Adjust state
            state->rcvd_proposal = true;
            state->p             = p_packet.id_src;

            switch (state->network_state) {
                case STATE_RECOVERY:
                    state->is_elected = false;
                    state->v          = (p_packet.e == 1); // Nodes are allowed to vote if we are in the first election of the round
                    state->r          = p_packet.r;
                    state->e          = p_packet.e;
#if PROPOSAL_ADD_NETWORK_SIZE
                    state->N          = p_packet.N;
#endif /* PROPOSAL_ADD_NETWORK_SIZE */
                    state->l          = 0;

                    LOG_INFO("Rcvd P from %hhu", p_packet.id_src);
                    enter_normal(state);
                    break;
                case STATE_NETWORK_FORMATION:
                    LOG_INFO("Rcvd P from %hhu", p_packet.id_src);
                    enter_normal(state);
                    break;
                default:
                    break;
            }
        } else {
            //LOG_VERBOSE("No P in election %hhu", state->e);

            // No valid proposal has been received
            state->p = 0;
        }
    }

    switch (state->network_state) {
        default:
            break;
    }
}
