/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Communication
 *
 * This file initialises the state and implements the main event loop.
 *
 */

#include "main.h"


/* Global variables ---------------------------------------------------------*/
extern TaskHandle_t xTaskHandle_comm;

extern uint32_t use_q_a;
extern uint32_t use_q_r;
extern uint32_t use_q_a_plus;

/* Static variables ----------------------------------------------------------*/
static comm_state_t comm_state;
static dcstat_t     log_cpu_dc;


/* Functions -----------------------------------------------------------------*/

void task_comm(void const* arg)
{
#ifdef STARTUP_SLEEP_LP
    if (STARTUP_SLEEP_LP > 0) {
        // Sleep directly after start-up, such that the start is also covered in the GPIO tracing
        comm_task_sleep_lp(STARTUP_SLEEP_LP);
    }
#endif /* STARTUP_SLEEP_LP */

#if DELAY_START_ENABLE && FLOCKLAB
    // If DELAY_START_ENABLE is set and the code is running on FlockLab, then the node waits for DELAY_START_PIN to clear before continuing
    while (DELAY_START_PIN_VALUE) {}
#endif /* DELAY_START_ENABLE */

    // Init the CPU duty cycle counter
    rtos_reset_cpu_dc();
    dcstat_reset(&log_cpu_dc);

    LOG_VERBOSE("Started communication task with up to %hhu slots per round (e=%hhu/P=%hhu/V=%hhu/N=%hhu/S=%hhu/C=%hhu/D=%hhu/I=%hhu)",
                NR_SLOTS_PER_ROUND, NR_ELECTIONS_PER_ROUND, NR_SLOTS_P, NR_SLOTS_V, NR_SLOTS_N, NR_SLOTS_S, NR_SLOTS_C, NR_SLOTS_D, NR_SLOTS_I);

    LOG_VERBOSE("Enabled quora: Q_a=%hhu/Q_r=%hhu/Q_a_plus=%hhu", use_q_a, use_q_r, use_q_a_plus);

    // Initialize state
    enter_network_formation(&comm_state, true);

#if SKIP_NETWORK_FORMATION
    // Skip the bootstrapping if necessary

    // Synchronize clocks virtually
    hs_timer_set_counter(0);
    update_clock_offset();
    set_drift_compensation_reference(true);

    enter_normal(&comm_state);

    // Set the correct network size
    comm_state.N     = MAX_NR_NODES;
    comm_state.N_net = MAX_NR_NODES;

#ifdef NODE_AS_LEADER
    comm_state.l = (NODE_ID == NODE_AS_LEADER);
    LOG_INFO("Set node %hhu as leader", NODE_AS_LEADER);

    // As the very first slot is skipped, we need to simulate that the leader sent a Propose packet
    comm_state.p = NODE_AS_LEADER;

    // Set the correct frequency band
    comm_state.freq_band = get_designated_freq_band(comm_state.p);
    set_radio_params(USED_RADIO_TX_POWER, comm_state.freq_band);
#endif /* NODE_AS_LEADER */

#endif /* SKIP_NETWORK_FORMATION */

    for (;;) {

        // Execute the current slot
        switch (comm_state.slot_next.type) {
            case SLOT_TYPE_PROPOSAL:
                SLOT_TYPE_PROPOSAL_ON_IND();
                //LOG_VERBOSE("Proposal");
                run_proposal_slot(&comm_state);
                SLOT_TYPE_PROPOSAL_OFF_IND();
                break;
            case SLOT_TYPE_VOTING:
                SLOT_TYPE_VOTING_ON_IND();
                //LOG_VERBOSE("Voting");
                run_voting_slot(&comm_state);
                SLOT_TYPE_VOTING_OFF_IND();
                break;
            case SLOT_TYPE_NOTIFICATION:
                SLOT_TYPE_NOTIFICATION_ON_IND();
                //LOG_VERBOSE("Notification");
                run_notification_slot(&comm_state);
                SLOT_TYPE_NOTIFICATION_OFF_IND();
                break;
            case SLOT_TYPE_SCHEDULING:
                SLOT_TYPE_SCHEDULING_ON_IND();
                //LOG_VERBOSE("Scheduling");
                run_scheduling_slot(&comm_state);
                SLOT_TYPE_SCHEDULING_OFF_IND();
                break;
            case SLOT_TYPE_COMMUNICATION:
                SLOT_TYPE_COMMUNICATION_ON_IND();
                //LOG_VERBOSE("Communication slot for %hhu", comm_state.slot_next.sender);
                run_communication_slot(&comm_state);
                SLOT_TYPE_COMMUNICATION_OFF_IND();
                break;
            case SLOT_TYPE_DISCOVERY:
                SLOT_TYPE_DISCOVERY_ON_IND();
                //LOG_VERBOSE("Discovery with TX=%hhu", (comm_state.slot_next.sender & D_SCHEDULE_MASK_TX) > 0);
                run_discovery_slot(&comm_state);
                SLOT_TYPE_DISCOVERY_OFF_IND();
                break;
            case SLOT_TYPE_IDLE:
                break;
            default:
                LOG_ERROR("Invalid slot type %i", comm_state.slot_next.type);
                break;
        }

        // Add RX and TX duty cycle to the corresponding phase
        rtos_add_dc_to_phase(comm_state.slot_next.type);

        // Set the earliest starting time of the next slot
        comm_state.t_slot = MAX(comm_state.t_slot + SLOT_DURATION_LP, lptimer_now_corrected() + MAX_SCHEDULE_LOOKUP_TIME_LP);

        // Determine the next slot
        comm_state.slot_next = get_next_used_slot_information(&comm_state);

        //uint8_t slot_number = (comm_state.t_slot / SLOT_DURATION_LP) % comm_state.T;
        //LOG_VERBOSE("Next slot: Slot %2hhu of type %hhu for sender %3hhu", slot_number, comm_state.slot_next.type, comm_state.slot_next.sender);

#if !CHANGE_ROUND_PERIOD_ENABLE
        // Perform house-keeping at the end of a round; ATTENTION: For accurate DC measurements, this must be executed using a fixed round period
        if ( (comm_state.slot_next.type == SLOT_TYPE_PROPOSAL) && (comm_state.slot_next.sender == 0) ) {
            // Execute End of Round
            run_end_of_round(&comm_state);
        }
#endif

        // Schedule wake-up
        uint64_t wakeup_time = comm_state.t_slot;

        // Modify wake-up time to include the guard time
        if ( (comm_state.slot_next.type == SLOT_TYPE_PROPOSAL)     ||
             (comm_state.slot_next.type == SLOT_TYPE_NOTIFICATION) ||
             (comm_state.slot_next.type == SLOT_TYPE_SCHEDULING)   ||
             (comm_state.slot_next.type == SLOT_TYPE_COMMUNICATION)  ) {
            // If the current node is the sender: Start slightly later, such that even nodes with a slower clock can receive the first transmission
            if ( (  comm_state.slot_next.sender == NODE_ID)                                                        ||
                 ( (comm_state.slot_next.type   == SLOT_TYPE_PROPOSAL)     && (comm_state.e == 1) && comm_state.l) ||
                 ( (comm_state.slot_next.type   == SLOT_TYPE_NOTIFICATION) && (comm_state.net_discovered) )          ) {
                wakeup_time += GLORIA_GUARD_TIME_LP;
            }
        }

#if !LOG_PRINT_IMMEDIATELY
        // Flush the log if enough time is left
        if (wakeup_time > (lptimer_now_corrected() + LOG_FLUSH_THRESHOLD_LP) ) {
            // Start a duty cycle counter in order to measure how much of the CPU on-time is due to log printing
            dcstat_start(&log_cpu_dc);

            // Flush the log
            uint64_t time_before = lptimer_now_corrected();
            if (!log_flush()) {
                LOG_ERROR("Failed to flush log in %llums", LPTIMER_TICKS_TO_MS(lptimer_now_corrected() - time_before));
            }

            // Check whether flushing the log took too long
            uint64_t time_now = lptimer_now_corrected();
            if (time_now < wakeup_time) {
                uint32_t time_left = wakeup_time - time_now;
#ifdef LOG_FLUSH_WARN_THRESHOLD_LP
                if (time_left < LOG_FLUSH_WARN_THRESHOLD_LP) {
                    LOG_WARNING("Finished flushing log in %2llums with only %llums left", LPTIMER_TICKS_TO_MS(time_now - time_before), LPTIMER_TICKS_TO_MS(time_left));
                } else {
#else
                {
#endif /* LOG_FLUSH_WARN_THRESHOLD_LP */
                    //LOG_VERBOSE("Finished flushing log in %2llums with %4llums left",     LPTIMER_TICKS_TO_MS(time_now - time_before), LPTIMER_TICKS_TO_MS(time_left));
                }
            } else {
                LOG_ERROR("Flushing log took %llums and ended %llums too late", LPTIMER_TICKS_TO_MS(time_now - time_before), LPTIMER_TICKS_TO_MS(time_now - wakeup_time));
            }

            // Stop the duty cycle counter
            dcstat_stop(&log_cpu_dc);
        }
#endif /* !LOG_PRINT_IMMEDIATELY */

        // Wait for next slot
        comm_task_sleep_until_lp(wakeup_time);

#if CHANGE_ROUND_PERIOD_ENABLE
        // Perform house-keeping at the end of a round; ATTENTION: For accurate DC measurements, this must be executed at the correct point in time, i.e. when the round just finished (AFTER waiting for the round to end)
        if ( (comm_state.slot_next.type == SLOT_TYPE_PROPOSAL) && (comm_state.slot_next.sender == 0) ) {
            // Execute End of Round
            run_end_of_round(&comm_state);
        }
#endif
    }
}

// Some helper functions:

inline void comm_task_sleep_lp(uint64_t duration)
{
    lptimer_set(lptimer_now() + duration, comm_task_unblock_callback);
    comm_task_block();
}

void comm_task_sleep_until_lp(uint64_t time)
{
    uint64_t now = lptimer_now_corrected();
    //LOG_VERBOSE("Going to sleep: %llums", LPTIMER_TICKS_TO_MS(now);

    if (time > (now + SLEEP_THRESHOLD_LP) ) {
        lptimer_set(corrected_to_uncorrected_lp_timestamp(time), comm_task_unblock_callback);
        comm_task_block();
    } else if (time > now) {
        // LOG_VERBOSE("Insufficient time left to use lptimer (start required in %lluus)", LPTIMER_TICKS_TO_US(time - now));
        delay_us(LPTIMER_TICKS_TO_US(time - now));
    } else {
        LOG_WARNING("Sleep until has been called %4lluus too late (expected starttime was %llums)", LPTIMER_TICKS_TO_US(now - time), LPTIMER_TICKS_TO_MS(time));
    }

    //LOG_VERBOSE("Wake-up time: %llums", LPTIMER_TICKS_TO_MS(lptimer_now_corrected());
}

void comm_task_sleep_until_hs(uint64_t time)
{
    uint64_t now = hs_timer_get_current_timestamp();

    if (time > (now + SLEEP_THRESHOLD_HS) ) {
        hs_timer_timeout_start(time, comm_task_unblock_callback);
        comm_task_block();
    } else if (time > now) {
        // LOG_VERBOSE("Insufficient time to use hstimer (start required in %lluus)", HS_TIMER_TICKS_TO_US(time - now));
        delay_us(HS_TIMER_TICKS_TO_US(time - now));
    } else {
        LOG_WARNING("Sleep until has been called %4lluus too late (expected starttime was %llums)", HS_TIMER_TICKS_TO_US(now - time), HS_TIMER_TICKS_TO_MS(time));
    }
}

inline void comm_task_end_sleep()
{
    // De-activate timer
    lptimer_set(0, 0);

    // Unblock task - simulate firing of timer
    comm_task_unblock_callback();
}

inline void comm_task_block()
{
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
}

/* Unblocks the comm task. */
inline void comm_task_unblock_callback(void)
{
    if (IS_INTERRUPT()) {
        vTaskNotifyGiveFromISR(xTaskHandle_comm, NULL);
    } else {
        xTaskNotifyGive(xTaskHandle_comm);
    }
}

void comm_task_update_dc()
{
    // Print phase information
    rtos_print_phase_dc();

#if LOG_ENABLE && (LOG_LEVEL > LOG_LEVEL_INFO)
    // Print the duty cycle information of the last round
    uint32_t cpu_dc     = rtos_get_cpu_dc();
    uint32_t cpu_log_dc = PPM_TO_HUNDREDS_OF_PERC(dcstat_get_dc(&log_cpu_dc));
    LOG_VERBOSE("DC - CPU (total/log):  %u.%02u%%/%u.%02u%%", cpu_dc / 100, cpu_dc % 100, cpu_log_dc / 100, cpu_log_dc % 100);
#endif /* LOG_LEVEL > LOG_LEVEL_INFO */

    // Reset the duty cycle counters
    rtos_reset_cpu_dc();
    dcstat_reset(&log_cpu_dc);
    radio_dc_counter_reset();
}
