/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#ifndef CPU_OFF_IND
#define CPU_OFF_IND()
#define CPU_ON_IND()
#endif /* CPU_OFF_IND */
#ifndef IDLE_TASK_RESUMED
#define IDLE_TASK_RESUMED()
#define IDLE_TASK_SUSPENDED()
#endif /* IDLE_TASK_RESUMED */
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* RTOS Task Handles */
#if BOLT_ENABLE
TaskHandle_t xTaskHandle_bolt = NULL;
#endif /* BOLT_ENABLE */
TaskHandle_t xTaskHandle_idle = NULL;
TaskHandle_t xTaskHandle_comm = NULL;

/* Variables */
uint64_t active_time                  = 0;
uint64_t wakeup_timestamp             = 0;
uint64_t last_cpu_reset               = 0;
uint64_t phase_rx_time[NR_SLOT_TYPES] = {0};
uint64_t phase_tx_time[NR_SLOT_TYPES] = {0};
uint64_t last_phase_reset             = 0;

/* USER CODE END Variables */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

extern void task_bolt(void* argument);
extern void task_comm(void* argument);

/* USER CODE END FunctionPrototypes */

/* Hook prototypes */
void vApplicationIdleHook(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 2 */
void vApplicationIdleHook( void )
{
    /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
    to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
    task. It is essential that code added to this hook function never attempts
    to block in any way (for example, call xQueueReceive() with a block time
    specified, or call vTaskDelay()). If the application makes use of the
    vTaskDelete() API function (as this demo application does) then it is also
    important that vApplicationIdleHook() is permitted to return to its calling
    function, because it is the responsibility of the idle task to clean up
    memory allocated by the kernel to any task that has since been deleted. */

    IDLE_TASK_RESUMED();

    if (!xTaskHandle_idle) {
        xTaskHandle_idle = xTaskGetCurrentTaskHandle();
    }

    /* If the application ends up in the RESET state, something went wrong -> reset the MCU */
    if (lpm_get_opmode() == OP_MODE_RESET) {
        NVIC_SystemReset();
    }

    IDLE_TASK_SUSPENDED();
}
/* USER CODE END 2 */

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
    /* Run time stack overflow checking is performed if configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.
     * This hook function is called if a stack overflow is detected. */

    (void)pcTaskName;
    (void)xTask;
    FATAL_ERROR("--- stack overflow detected! ---\r\n");
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void)
{
    /* vApplicationMallocFailedHook() will only be called if
    configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
    function that will get called if a call to pvPortMalloc() fails.
    pvPortMalloc() is called internally by the kernel whenever a task, queue,
    timer or semaphore is created. It is also called by various parts of the
    demo application. If heap_1.c or heap_2.c are used, then the size of the
    heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
    FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
    to query the size of free heap space that remains (although it does not
    provide information on how the remaining heap might be fragmented). */
    FATAL_ERROR("--- malloc failed ---\r\n");
}
/* USER CODE END 5 */

/* USER CODE BEGIN PREPOSTSLEEP */
void PreSleepProcessing(uint32_t ulExpectedIdleTime)
{
    /* Note: for tickless idle, the HAL tick needs to be suspended! */
    lpm_prepare();

    /* Duty cycle measurement */
    active_time += lptimer_now() - wakeup_timestamp;
    CPU_OFF_IND();
}

void PostSleepProcessing(uint32_t ulExpectedIdleTime)
{
    CPU_ON_IND();
    wakeup_timestamp = lptimer_now();    /* Reset duty cycle timer */

    lpm_resume();
}
/* USER CODE END PREPOSTSLEEP */

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
/* RTOS functions ------------------------------------------------------------*/
void rtos_init(void)
{
    /* Create RTOS tasks */
    /* Max. priority is (configMAX_PRIORITIES - 1), higher numbers = higher priority; idle task has priority 0 */
#if BOLT_ENABLE
    if (xTaskCreate(task_bolt,
                  "boltTask",
                  BOLT_TASK_STACK_SIZE,
                  NULL,
                  tskIDLE_PRIORITY + 1,
                  &xTaskHandle_bolt) != pdPASS) { Error_Handler(); }
#endif /* BOLT_ENABLE */
    if (xTaskCreate(task_comm,
                    "commTask",
                    COMM_TASK_STACK_SIZE,
                    NULL,
                    tskIDLE_PRIORITY + 2,
                    &xTaskHandle_comm) != pdPASS) { Error_Handler(); }
}

uint32_t rtos_get_cpu_dc(void)
{
    uint32_t elapsed = (lptimer_now() - last_cpu_reset);
    if (elapsed > 0) {
        return (uint32_t)(( (active_time + (lptimer_now() - wakeup_timestamp) ) * 10000) / elapsed);
    } else {
        return 10000;
    }
}

void rtos_reset_cpu_dc(void)
{
    last_cpu_reset   = lptimer_now();
    wakeup_timestamp = lptimer_now();
    active_time      = 0;
}

void rtos_add_dc_to_phase(uint8_t phase)
{
    if (phase < NR_SLOT_TYPES) {
        /* Store active time of previous phase */
        phase_rx_time[phase] += radio_get_rx_time();
        phase_tx_time[phase] += radio_get_tx_time();
        radio_dc_counter_reset();
    } else {
        LOG_ERROR("Tried adding to invalid phase %hhu", phase);
    }
}

void rtos_print_phase_dc(void)
{
    /* Switch to IDLE to finish previous phase */
    rtos_add_dc_to_phase(SLOT_TYPE_IDLE);

#if CHANGE_ROUND_PERIOD_ENABLE
    uint64_t delta = lptimer_now() - last_phase_reset;
#else
    uint64_t delta = NR_SLOTS_PER_ROUND * SLOT_DURATION_LP;
#endif
    LOG_INFO("Phase Rx duty cycle (P/V/N/S/C/D): %luppm/%luppm/%luppm/%luppm/%luppm/%luppm",
             (uint32_t)(phase_rx_time[SLOT_TYPE_PROPOSAL]      * 1000000 / delta),
             (uint32_t)(phase_rx_time[SLOT_TYPE_VOTING]        * 1000000 / delta),
             (uint32_t)(phase_rx_time[SLOT_TYPE_NOTIFICATION]  * 1000000 / delta),
             (uint32_t)(phase_rx_time[SLOT_TYPE_SCHEDULING]    * 1000000 / delta),
             (uint32_t)(phase_rx_time[SLOT_TYPE_COMMUNICATION] * 1000000 / delta),
             (uint32_t)(phase_rx_time[SLOT_TYPE_DISCOVERY]     * 1000000 / delta) );
    LOG_INFO("Phase Tx duty cycle (P/V/N/S/C/D): %luppm/%luppm/%luppm/%luppm/%luppm/%luppm",
             (uint32_t)(phase_tx_time[SLOT_TYPE_PROPOSAL]      * 1000000 / delta),
             (uint32_t)(phase_tx_time[SLOT_TYPE_VOTING]        * 1000000 / delta),
             (uint32_t)(phase_tx_time[SLOT_TYPE_NOTIFICATION]  * 1000000 / delta),
             (uint32_t)(phase_tx_time[SLOT_TYPE_SCHEDULING]    * 1000000 / delta),
             (uint32_t)(phase_tx_time[SLOT_TYPE_COMMUNICATION] * 1000000 / delta),
             (uint32_t)(phase_tx_time[SLOT_TYPE_DISCOVERY]     * 1000000 / delta) );

    if (phase_rx_time[SLOT_TYPE_IDLE] > 0 || phase_tx_time[SLOT_TYPE_IDLE] > 0) {
        LOG_ERROR("RX or TX during invalid phase");
    }

    /* Reset counter */
    last_phase_reset = lptimer_now();
    memset(phase_rx_time, 0, sizeof(phase_rx_time));
    memset(phase_tx_time, 0, sizeof(phase_tx_time));
}

bool rtos_check_stack_usage(void) {
    uint32_t temp;

    if (xTaskGetSchedulerState() != taskSCHEDULER_RUNNING) {
        return false;
    }

#if BOLT_ENABLE
    temp = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_bolt) * 100 / BOLT_TASK_STACK_SIZE);
    if (temp > RTOS_STACK_WATERMARK_WARNING)
    {
        LOG_WARNING("Task BOLT stack usage: %2u%%", temp);
        return false;
    } else {
        // LOG_VERBOSE("Task BOLT stack usage: %2u%%", temp);
    }
#endif /* BOLT_ENABLE */

    temp = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_comm) * 100 / COMM_TASK_STACK_SIZE);
    if (temp > RTOS_STACK_WATERMARK_WARNING)
    {
        LOG_WARNING("Task COM stack usage:  %2u%%", temp);
        return false;
    } else {
        // LOG_VERBOSE("Task COM stack usage:  %2u%%", temp);
    }

    /* NOTE: uxTaskGetStackHighWaterMark() returns the number of unused/free words on the stack */
    temp = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_idle) * 100 / configMINIMAL_STACK_SIZE);
    if (temp > RTOS_STACK_WATERMARK_WARNING)
    {
        LOG_WARNING("Task Idle stack usage: %2u%%", temp);
        return false;
    } else {
        // LOG_VERBOSE("Task Idle stack usage: %2u%%", temp);
    }

    return true;
}

/* USER CODE END Application */
