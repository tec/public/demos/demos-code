/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Schedule
 *
 * This file contains the functions related to creating and using the schedule.
 *
 */

#include "main.h"


/* Functions -----------------------------------------------------------------*/

slot_info_t get_next_used_slot_information(comm_state_t* state)
{
    // Slot number of the next complete slot (with increasing slot numbers across rounds)
    uint64_t global_slot_number = (state->t_slot + SLOT_DURATION_LP - 1UL) / SLOT_DURATION_LP;
    // Number of the current round
    uint64_t round_number       = global_slot_number / state->T;
    // Number of the slot in the current round
    uint64_t local_slot_number  = global_slot_number % state->T;

    // Round up to start of next slot
    state->t_slot = global_slot_number * SLOT_DURATION_LP;

    if (local_slot_number < (NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) ) ) {
        // We are still in the Consensus phase
        if        ( (local_slot_number % (NR_SLOTS_P + NR_SLOTS_V) ) == 0) {
            // The current slot is a Proposal slot
            return (slot_info_t) {SLOT_TYPE_PROPOSAL, get_designated_leader(round_number, (local_slot_number / (NR_SLOTS_P + NR_SLOTS_V) ) + 1)};
        } else if ( (local_slot_number % (NR_SLOTS_P + NR_SLOTS_V) ) == NR_SLOTS_P) {
            // The current slot is a Voting slot

            if (state->network_state == STATE_RECOVERY) {
                // We skip everything except for Proposal slots -> we simulate the finish of a Voting slot so that the designated leader remains correct
                state->e = (local_slot_number / (NR_SLOTS_P + NR_SLOTS_V) ) + 1 + 1;

                state->t_slot += (NR_SLOTS_P + NR_SLOTS_V - (local_slot_number % (NR_SLOTS_P + NR_SLOTS_V) ) ) * SLOT_DURATION_LP;
                return get_next_used_slot_information(state);
            } else {
                return (slot_info_t) {SLOT_TYPE_VOTING, 0};
            }
        } else {
            // The current slot would be a Voting slot.
            // However, the node would start in the middle of the block of Voting slots.
            // To prevent the timing issues this would create, the node skips the current Voting slots.
            state->t_slot += (NR_SLOTS_P + NR_SLOTS_V - (local_slot_number % (NR_SLOTS_P + NR_SLOTS_V) ) ) * SLOT_DURATION_LP;
            return get_next_used_slot_information(state);
        }
    } else if (local_slot_number == (NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) ) || (state->network_state == STATE_RECOVERY) ) {
        // The current slot is a Notification slot

        if (state->network_state == STATE_RECOVERY) {
#if ENABLE_RECOVERY_ON_SCHEDULING
            if (local_slot_number <= (NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) + NR_SLOTS_N) ) {
                // We skip everything except for Proposal and Scheduling slots -> we directly jump to the start of the next Scheduling slot
                state->t_slot += (NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) + NR_SLOTS_N - local_slot_number) * SLOT_DURATION_LP;
                return (slot_info_t) {SLOT_TYPE_SCHEDULING, 0};
            }
#endif /* ENABLE_RECOVERY_ON_SCHEDULING */
            // We skip everything except for Proposal slots -> we directly jump to the start of the next round
            state->t_slot += (state->T - local_slot_number) * SLOT_DURATION_LP;
            return (slot_info_t) {SLOT_TYPE_PROPOSAL, 0};
        } else {
            return (slot_info_t) {SLOT_TYPE_NOTIFICATION, 0};
        }
    } else if (local_slot_number == (NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) + NR_SLOTS_N) ) {
        // The current slot is a Scheduling slot
        if (state->is_elected) {
            return (slot_info_t) {SLOT_TYPE_SCHEDULING, NODE_ID};
        } else {
            return (slot_info_t) {SLOT_TYPE_SCHEDULING, 0};
        }
    } else if (local_slot_number < (NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) + NR_SLOTS_N + NR_SLOTS_S + NR_SLOTS_C) ) {
        // The current slot is a Communication slot
        uint8_t comm_slot_nr = local_slot_number - NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) - NR_SLOTS_N - NR_SLOTS_S;

        if (state->S[comm_slot_nr] != 0) {
            // The next slot is used
            return (slot_info_t) {SLOT_TYPE_COMMUNICATION, state->S[comm_slot_nr]};
        } else {
            // Determine the next used Communication slot or Idle slot

            // Iterate over the schedule to find the next assigned Communication slot, if there is one
            for (uint32_t i = comm_slot_nr + 1; i < NR_SLOTS_C; i++) {
                if (state->S[i] != 0) {
                    // Set the starttime according to the computed offset of the slot in the schedule
                    //LOG_VERBOSE("Skipping %hhu unused slots during Communication phase", i - comm_slot_nr);
                    state->t_slot += (i - comm_slot_nr) * SLOT_DURATION_LP;
                    return (slot_info_t) {SLOT_TYPE_COMMUNICATION, state->S[i]};
                }
            }

            // The Communication phase is over - skip to the next phase
            //LOG_VERBOSE("Skipping %hhu unused slots during Communication phase", NR_SLOTS_C - comm_slot_nr);
            state->t_slot += (NR_SLOTS_C - comm_slot_nr) * SLOT_DURATION_LP;
            return get_next_used_slot_information(state);
        }
    } else if (local_slot_number < (state->T - NR_SLOTS_I) ) {
        // The current slot is a Discovery slot
        uint8_t disc_slot_nr = local_slot_number - NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) - NR_SLOTS_N - NR_SLOTS_S - NR_SLOTS_C;

        if (!state->disc_prepared) {
            prepare_discovery(state);
        }

        if ( (state->S_disc[disc_slot_nr] & D_SCHEDULE_MASK_ID) == NODE_ID) {
            // The next slot is used
            return (slot_info_t) {SLOT_TYPE_DISCOVERY, state->S_disc[disc_slot_nr]};
        } else {
            // Determine the next used Discovery slot assigned to this node or Idle slot

#if CHANGE_ROUND_PERIOD_ENABLE
            uint8_t nr_discovery_slots = state->T - NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) - NR_SLOTS_N - NR_SLOTS_S - NR_SLOTS_C - NR_SLOTS_I;
#else
            uint8_t nr_discovery_slots = NR_SLOTS_D;
#endif /* CHANGE_ROUND_PERIOD_ENABLE */

            // Iterate over the schedule to find the next assigned Discovery slot, if there is one
            for (uint32_t i = disc_slot_nr + 1; i < nr_discovery_slots; i++) {
                if ( (state->S_disc[i] & D_SCHEDULE_MASK_ID) == NODE_ID) {
                    // Set the starttime according to the computed offset of the slot in the schedule
                    //LOG_VERBOSE("Skipping %hhu unused slots during Discovery phase", i - disc_slot_nr);
                    state->t_slot += (i - disc_slot_nr) * SLOT_DURATION_LP;
                    return (slot_info_t) {SLOT_TYPE_DISCOVERY, state->S_disc[i]};
                }
            }

            // The Discovery phase is over - skip to the next phase
            //LOG_VERBOSE("Skipping %hhu unused slots during Discovery phase", nr_discovery_slots - disc_slot_nr);
            state->t_slot += (nr_discovery_slots - disc_slot_nr) * SLOT_DURATION_LP;
            return get_next_used_slot_information(state);
        }
    } else {
        // The current slot is an Idle slot -> we directly jump to the start of the next round
        //LOG_VERBOSE("Skipping %hhu unused slots during Idle phase", NR_ELECTIONS_PER_ROUND - local_slot_number);
        state->t_slot += (NR_ELECTIONS_PER_ROUND - local_slot_number) * SLOT_DURATION_LP;
        return (slot_info_t) {SLOT_TYPE_PROPOSAL, 0};
    }
}

/* Updates the schedule.
 * For every node: If the node requested a reduction in the number of slots by k, then the last k slots assigned to that node are freed.
 * If a node requested additional k slots, then the first free slots are assigned to that node in increasing order of node ID. */
bool schedule_compute(comm_state_t* state, int8_t* requests, schedule_t* schedule_next)
{
#if S_SCHEDULE_REQUEST_DELTAS
    // Initialize next schedule with current one
    memcpy(schedule_next, state->S, NR_SLOTS_C);
#else
    // Initialize next schedule with empty schedule
    memset(schedule_next, 0, NR_SLOTS_C);
#endif /* SCHEDULE_REQUEST_DELTAS */

#if VOTING_LOG_REQUEST_VALUES
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
        LOG_VERBOSE("Rq %hhu -> %hhi", i, requests[i]);
    }
#endif /* VOTING_LOG_REQUEST_VALUES */

    bool all_added = false;

    // Free the slots as requested by the nodes - start from the back (slot NR_SLOTS_C - 1)
    for (int32_t slot = NR_SLOTS_C - 1; slot > 0; slot--) {
        node_id_t owner_current = schedule_next[slot];
        if ( (owner_current > 0) && (requests[owner_current - 1] < 0) ) {
            schedule_next[slot] = 0;

            // Only decrease the number of slots still needed if the value does not correspond to SLOT_COUNT_NEUTRAL_ELEMENT (as such a node is not in the network anymore)
            if (requests[owner_current - 1] != SLOT_COUNT_NEUTRAL_ELEMENT) {
                requests[owner_current - 1]++;
            }
        }
    }

    // Assign the free slots as requested - start from the start (slot 0)
    node_id_t last_id = 0;
    for (uint32_t slot = 0; (slot < NR_SLOTS_C) && !all_added; slot++) {
        // If the slot is not used anymore: reassign it
        if (schedule_next[slot] == 0) {
            bool found = false;

            // Iterate over all node IDs - fulfill requests from the lowest node ID first
            for (node_id_t owner_new = last_id; (owner_new < MAX_NR_NODES) && !found; owner_new++) {
                // If the node still needs more slots and it is considered a part of the network:
                // Assign the current slot to it
                if ( (requests[owner_new] > 0)           &&
                     IS_FLAG_SET(state->M, owner_new + 1)  ) {
                    // Update schedule
                    schedule_next[slot] = owner_new + 1;
                    requests[owner_new]--;

                    // Update variables used to improve performance of schedule computation
                    found   = true;
                    last_id = owner_new;
                }
            }

            // If no node was found: All nodes have enough slots --> No need to continue
            if (!found) {
                all_added = true;
            }
        }
        else if (slot == (NR_SLOTS_C - 1) ) {
            LOG_WARNING("Some requests could not be fulfilled, as no free slots are remaining");
            return false;
        }
    }

    return true;
}

#if S_SCHEDULE_COMPRESS
bool schedule_pack(const schedule_t* schedule, schedule_packet_t* packet)
{
    memset(packet->S, 0, S_PACKET_NR_SCHEDULE_BYTES);

    uint32_t ofs  = 0;
    uint32_t slot = 0;
    for (uint32_t i = 0; i < S_PACKET_NR_SCHEDULE_BYTES; i++) {
        while (ofs < 8) {
            uint16_t curr_slot  = (schedule[slot] & ( (1 << S_SCHEDULE_COMPRESSION_BITS) - 1) ) << ofs;
            packet->S[i]       |= curr_slot & 0xFF;
            if (curr_slot & 0xFF00) {
                // Fill lower part of next byte
                packet->S[i + 1] |= curr_slot >> 8;
            }

            ofs += S_SCHEDULE_COMPRESSION_BITS;
            slot++;
        }

        // Prepare the offset for the next byte
        ofs %= 8;
    }

    return true;
}
#endif /* SD_SCHEDULE_COMPRESS */

bool schedule_update(comm_state_t* state, schedule_packet_t* packet)
{
    // Copy schedule from packet
#if S_SCHEDULE_COMPRESS
    uint32_t ofs = 0;
    for (uint32_t i = 0; i < NR_SLOTS_C; i++) {
        state->S[i] = (packet->S[ofs / 8] >> (ofs % 8) ) & ( (1 << S_SCHEDULE_COMPRESSION_BITS) - 1);
        if ( ( (ofs % 8) + S_SCHEDULE_COMPRESSION_BITS) > 8) {
            // Fetch lower part of next byte
            uint32_t next_ofs  = (ofs % 8) + S_SCHEDULE_COMPRESSION_BITS - 8;
            state->S[i]       += (packet->S[(ofs / 8) + 1] & ( (1 << next_ofs) - 1) ) << (S_SCHEDULE_COMPRESSION_BITS - next_ofs);
        }

        ofs += S_SCHEDULE_COMPRESSION_BITS;
    }
#else
    memcpy(state->S, packet->S, NR_SLOTS_C);
#endif /* SD_SCHEDULE_COMPRESS */

#if FL_TRACE_SCHEDULING
    // Set indication if transmitting
    if ( (schedule_get_nr_slots(state->S, NODE_ID) > 0) && (get_nr_of_set_flags(state->M) > 1) ) {
        STATE_TRANSMITTING_IND();
    } else if (state->network_state != STATE_NETWORK_FORMATION) {
        STATE_RECEIVING_IND();
    }
#endif /* FL_TRACE_SCHEDULING */

    return true;
}

bool schedule_update_history(const int8_t* D_new, uint8_t* D_validity, int8_t* D_hist)
{
    for(uint8_t i = 0; i < MAX_NR_NODES; i++) {
        if (D_new[i] != SLOT_COUNT_NEUTRAL_ELEMENT) {
            // Enter new value
            D_hist[i]     = D_new[i];
            D_validity[i] = S_SCHEDULE_USE_HISTORY;
        } else if (D_validity[i] > 0) {
            // Decrease validity
            D_validity[i]--;

            if (D_validity[i] == 0) {
                // Remove from history
                D_hist[i] = SLOT_COUNT_NEUTRAL_ELEMENT;
            }
        }
    }

    return true;
}

/* Returns true if the schedule is empty.
 *   schedule: Pointer to the schedule to check
 */
bool is_schedule_empty(const schedule_t* schedule)
{
    for (uint32_t i = 0; i < NR_SLOTS_C; i++) {
        if (schedule[i] != 0) {
            return false;
        }
    }
    return true;
}

uint8_t schedule_get_nr_slots(const schedule_t* schedule, node_id_t node_id)
{
    uint8_t nr_slots = 0;

    for (uint32_t i = 0; i < NR_SLOTS_C; i++) {
        if (schedule[i] == node_id) {
            nr_slots++;
        }
    }

    return nr_slots;
}

void discovery_distribute(comm_state_t* state)
{
    // Verify that at least one node is set
    uint8_t nr_nodes = get_nr_of_set_flags(state->M);
    if (nr_nodes == 0) {
        LOG_ERROR("Discovery schedule cannot be distributed over an empty set of nodes");
        return;
    }

#ifndef HAL_RNG_MODULE_ENABLED
    // Setup random generator - sample RNG once so state can be randomized again afterwards and seed again
    uint32_t prev_state = random_rand32();
#endif /* HAL_RNG_MODULE_ENABLED */
    uint16_t seed_round = (uint16_t)state->p * (1 + state->r);

    // Use srand and rand to generate a pseudo-random sequence with a known seed shared across nodes
    srand(SEED_RAND_DISCOVERY + seed_round);

    uint8_t curr_node = 1;
    for (uint32_t i = 0; i < NR_SLOTS_D; i++) {
        // Determine whether to transmit, listen or idle
        uint8_t curr_sample = rand() % 100;

        if        (curr_sample >= (D_PROBABILITY_LISTEN + D_PROBABILITY_TRANSMIT) ) {
            // Idle slot
            break;
        } else if (curr_sample >= D_PROBABILITY_LISTEN) {
            // Transmit slot
            state->S_disc[i] = D_SCHEDULE_MASK_TX;
        }   // Listen slot

        // Assign slot to the next node in the network
#if D_RANDOMIZE_DISCOVERY_ORDER
        // Choose random node index out of interval [1, nr_nodes] so that sample probability is equal and independent of the node ID
        uint8_t node_idx = (rand() % nr_nodes) + 1;
        for (uint32_t j = 0; j < node_idx; j++) {
            curr_node = (curr_node % MAX_NR_NODES) + 1;
#else
        {
#endif /* D_RANDOMIZE_DISCOVERY_ORDER */
            while (!IS_FLAG_SET(state->M, curr_node)) { curr_node = (curr_node % MAX_NR_NODES) + 1; }
        }

        state->S_disc[i] |= (curr_node & D_SCHEDULE_MASK_ID);
        curr_node         = (curr_node % MAX_NR_NODES) + 1;
    }

#ifndef HAL_RNG_MODULE_ENABLED
    // Randomize RNG again
    srand(prev_state);
#endif /* HAL_RNG_MODULE_ENABLED */

    LOG_VERBOSE("Generated Discovery S: %8x (r=%3hu/p=%2hhu)", crc32(state->S_disc, NR_SLOTS_D, 0), state->r, state->p);
}

slot_count_t demand_update(comm_state_t* state)
{
    // Choose a random value in [RQ_NR_DATA_SLOTS_MIN, RQ_NR_DATA_SLOTS_MAX]
    state->d = RQ_NR_DATA_SLOTS_MIN + (random_rand32() % (RQ_NR_DATA_SLOTS_MAX - RQ_NR_DATA_SLOTS_MIN + 1) );
    //LOG_VERBOSE("Currently desired slots: %hhu", state->d);

    return state->d;
}
