/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * States
 *
 * This file provides the helper functions used to change the network state.
 *
 */

#include "main.h"


/* Functions -----------------------------------------------------------------*/

void enter_network_formation(comm_state_t* state, bool reset_radio)
{
    // Reset radio
    if (reset_radio) {
        LOG_INFO("No node left in the network -> performing a reset\n--------------------------");
        log_flush();

        // Reset the radio
        radio_init();
    }

#if !LOG_PRINT_IMMEDIATELY
    // Always flush the log to prevent build-up during bootstrapping (log DC is not affected during bootstrapping)
    uint64_t time_before = lptimer_now_corrected();
    if (!log_flush()) {
        LOG_ERROR("Failed to flush log in %llums", LPTIMER_TICKS_TO_MS(lptimer_now_corrected() - time_before));
    }
#endif /* !LOG_PRINT_IMMEDIATELY */

    // Re-initialize random values
    generate_random_seq(MAX_NR_NODES);
#ifdef SEED_RAND_PROTOCOL
    // Note that the randomness is partly based on the randomness of the HS timer and hence limited - DO NOT RESET BEFORE THIS CALL
    srand(SEED_RAND_PROTOCOL);
#endif /* SEED_RAND_PROTOCOL */

    // Reset clocks
    hs_timer_set_counter(0);
    hs_timer_set_drift(1);
    hs_timer_set_offset(0);
    update_clock_offset();
    set_drift_compensation_reference(true);

    // Add current DC costs to previous slot
    rtos_add_dc_to_phase(state->slot_next.type);

    // Reset the comm_state - only variables with a potential to be initialized to unequal 0 are set individually afterwards
    memset(state, 0, sizeof(comm_state_t));
    demand_update(state);
    memset(state->D_tot, SLOT_COUNT_NEUTRAL_ELEMENT, MAX_NR_NODES);
#if S_SCHEDULE_USE_HISTORY
    memset(state->D_hist, SLOT_COUNT_NEUTRAL_ELEMENT, MAX_NR_NODES);
#endif /* S_SCHEDULE_USE_HISTORY */
    state->slot_next.type = SLOT_TYPE_IDLE;

    state->v     = true;
    state->e     = 1;
    state->N     = 1;
    state->N_net = state->N;
#if CHANGE_ROUND_PERIOD_ENABLE
    state->T     = CHANGE_ROUND_PERIOD_PIN_LOW;
#else
    state->T     = NR_SLOTS_PER_ROUND;
#endif

    // The node will skip the first (Proposal) slot - make the node believe it sent a proposal so it can elect itself and start discovery
    state->l = 1;
    state->p = NODE_ID;

    // Enable RxBoosted
    radio_set_rx_gain(true);

    // Adapt radio parameters
    state->freq_band = get_designated_freq_band(state->p);
    set_radio_params(USED_RADIO_TX_POWER, state->freq_band);

    // Enter the NETWORK_FORMATION state
    state->network_state = STATE_NETWORK_FORMATION;
    STATE_NETWORK_FORMATION_IND();
    IS_FOLLOWING_IND();

    LOG_INFO("Network formation on Ch %2hhu", state->freq_band);
}

void enter_normal(comm_state_t* state)
{
    // Enter the NORMAL state
    state->network_state = STATE_NORMAL;
    STATE_NORMAL_IND();
    STATE_RECEIVING_IND();

    LOG_INFO("Joined  with clock at %llums", HS_TIMER_TICKS_TO_MS(hs_timer_get_current_timestamp()));
}

void enter_recovery(comm_state_t* state, bool recover_discovered)
{
    if (recover_discovered) {
        // Adjust round start time according to discovered network
        uint64_t time_since_last_round_lp = state->t_d;
        uint64_t round_time_lp            = (uint64_t)state->T_d * SLOT_DURATION_LP;

        int64_t delta_rx = (int64_t)lptimer_now() - state->t_d_rx;
#if D_LOG_TIMESTAMPS
        if ( (delta_rx < 0) || (delta_rx > (NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) ) {
            LOG_ERROR("Expected (small) positive delta but encountered %lli for lp=%llu/t_rx=%llu (%lluus)", delta_rx, lptimer_now(), state->t_d_rx, LPTIMER_TICKS_TO_US(state->t_d_rx));
        } else if (state->t_d > (2 * NR_SLOTS_PER_ROUND * SLOT_DURATION_LP) ) {
            LOG_ERROR("Corrupted timestamp t=%llu", state->t_d);
        } else {
#else
        {
#endif /* D_LOG_TIMESTAMPS */
            time_since_last_round_lp += delta_rx;
        }

        // Take over parameters
        state->T = state->T_d;
        state->r = state->r_d + time_since_last_round_lp / round_time_lp;
#if DISCOVERY_ADD_NETWORK_SIZE
        state->N = state->N_d;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */

        // Set clock to offset inside round of discovered network
        uint64_t time_in_curr_round_lp = time_since_last_round_lp % round_time_lp;
        uint64_t time_in_curr_round_hs = LPTIMER_TICKS_TO_HS_TIMER(time_in_curr_round_lp);

        // Adjust t_slot to currently active slot
        state->t_slot                  = time_in_curr_round_lp - (time_in_curr_round_lp % SLOT_DURATION_LP);

#if D_LOG_TIMESTAMPS
        LOG_VERBOSE("Join info: t=%lluus/t_rx=%lluus/T=%hhu/r=%hu -> t_lp=%lluus/t_slot=%llu", LPTIMER_TICKS_TO_US(time_since_last_round_lp), LPTIMER_TICKS_TO_US(state->t_d_rx), state->T_d, state->r_d, LPTIMER_TICKS_TO_US(time_in_curr_round_lp), state->t_slot);
#endif /* D_LOG_TIMESTAMPS */

        // Add round offset to globally sync clocks
        time_in_curr_round_hs += (uint64_t)state->r * LPTIMER_TICKS_TO_HS_TIMER(round_time_lp);
        state->t_slot         += (uint64_t)state->r *                           round_time_lp;

#if D_LOG_TIMESTAMPS
        LOG_VERBOSE("Adjusting clocks by %hu rounds to t_hs=%llu(%lluus)/t_slot=%llu", state->r, time_in_curr_round_hs, HS_TIMER_TICKS_TO_US(time_in_curr_round_hs), state->t_slot);
#endif /* D_LOG_TIMESTAMPS */

        // Adjust HS timer
        double delta;
        if (hs_timer_get_current_timestamp() < time_in_curr_round_hs) {
            delta =   (double)(time_in_curr_round_hs            - hs_timer_get_current_timestamp());
        } else {
            delta = - (double)(hs_timer_get_current_timestamp() - time_in_curr_round_hs);
        }
        hs_timer_adapt_offset(delta);

        // Sync LP to HS timer
        update_clock_offset();

#if ENABLE_SEPARATE_FREQ_BANDS
        // Set leader ID so that frequency band is correctly set
        state->p = state->id_discovered;

        state->freq_band = get_designated_freq_band(state->p);
        set_radio_params(USED_RADIO_TX_POWER, state->freq_band);
#endif /* ENABLE_SEPARATE_FREQ_BANDS */

        uint8_t local_slot = (state->t_slot / SLOT_DURATION_LP) % state->T;
        if (local_slot < (NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) ) ) {
            // Consensus phase: We need to set the election so that the next slot which will be executed has the correct e
            state->e = (local_slot / (NR_SLOTS_P + NR_SLOTS_V)) + 1 + 1;
        }

        LOG_INFO("Recovering network of leader %2hhu at round %3hu in slot %hhu in %llums", state->id_discovered, state->r, local_slot, LPTIMER_TICKS_TO_MS(round_time_lp - time_in_curr_round_lp));
    }

    state->net_discovered = false;
    state->r_recovering   = 0;

    // Invalidate schedule so no old information will be printed when disbanding network
    memset(state->S, 0, NR_SLOTS_C);

    // Enter the RECOVERY state
    state->network_state = STATE_RECOVERY;
    STATE_RECOVERY_IND();
    STATE_RECEIVING_IND();
    IS_FOLLOWING_IND();

    LOG_INFO("Recovering with clock at %llums", HS_TIMER_TICKS_TO_MS(hs_timer_get_current_timestamp()));
}

bool set_radio_params(const int8_t tx_power, const uint8_t freq_band)
{
    bool changed = false;

    if (tx_power != chaos_get_tx_power()) {
        // Set the correct TX power
        gloria_set_tx_power(tx_power);
        chaos_set_tx_power(tx_power);

        changed = true;
    }

    if (freq_band != chaos_get_band()) {
        // Set the correct frequency band
        gloria_set_band(freq_band);
        chaos_set_band(freq_band);

        changed = true;
    }

    if (changed) {
        LOG_INFO("Set TX power = %hhu / frequency band = %hhu", tx_power, freq_band);
    }
    return changed;
}
