/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Utilities
 *
 * This file contains helper functions concerning time calculations.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/

static int64_t  clock_offset_lp                 = 0;
static double   drift_compensation_reference_hs = 0;
static uint64_t drift_compensation_timestamp_hs = 0;
static bool     skip_next_drift_compensation    = true;


/* Functions -----------------------------------------------------------------*/

void adjust_timers(uint64_t* t_slot, uint64_t* t_round, uint8_t T_prev, uint8_t T_new)
{
    uint64_t t_slot_prev = *t_slot;

    // Compute global slot number
    uint64_t global_slot_number = *t_slot / SLOT_DURATION_LP;
    uint64_t round_number       = (global_slot_number / T_prev) * T_new;
    uint64_t local_slot_number  = global_slot_number % T_prev;

    // Update t_slot
    *t_slot = (round_number + local_slot_number) * SLOT_DURATION_LP;

    // Update HS Timer
    int64_t delta;
    if (*t_slot > t_slot_prev) {
        delta =   (int64_t)(*t_slot     - t_slot_prev);
    } else {
        delta = - (int64_t)(t_slot_prev - *t_slot    );
    }
    hs_timer_adapt_offset(LPTIMER_TICKS_TO_HS_TIMER(delta));

    // Update LP Timer
    update_clock_offset();

    // Update t_round so that the difference to t_slot remains constant - NOTE: as both are 64bit values, signed gets converted to unsigned if not treated separately
    if (delta > 0) {
        *t_round +=              delta;
    } else {
        *t_round -= (uint64_t)(- delta);
    }

    //LOG_VERBOSE("Current clks: t_slot_prev=%llu/t_slot=%llu/delta=%lli/HS=%llu/LP=%llu", t_slot_prev, *t_slot, delta, hs_timer_get_current_timestamp(), lptimer_now_corrected());
    LOG_VERBOSE("Changed round period: %hhu -> %hhu", T_prev, T_new);
}

// Lptimer offset compensation:

void update_clock_offset()
{
    uint64_t hs_timestamp, lp_timestamp;
    if (lptimer_now_synced(&lp_timestamp, &hs_timestamp)) {
        int64_t delta    = HS_TIMER_TICKS_TO_LPTIMER((uint64_t) round(hs_timestamp * hs_timer_get_drift() + hs_timer_get_offset() ) ) - lptimer_now_corrected();
        clock_offset_lp += delta;
        if        (ABS(delta) >  DRIFT_THRESHOLD_WARN_LP) {
            LOG_WARNING("LP offset adjusted by %llius", delta * 1000000UL / LPTIMER_SECOND); // Do not use LPTIMER_TICKS_TO_US for conversion to preserve sign
        } else if (ABS(delta) >= DRIFT_THRESHOLD_INFO_LP) {
            LOG_VERBOSE("LP offset adjusted by %llius", delta * 1000000UL / LPTIMER_SECOND);
        }
    } else {
        LOG_ERROR("Could not update clock offset");
    }
}

uint64_t lptimer_now_corrected()
{
    return lptimer_now() + clock_offset_lp;
}


// Time unit conversations:

uint64_t corrected_to_uncorrected_lp_timestamp(uint64_t lp_timestamp)
{
    return lp_timestamp - clock_offset_lp;
}


// Drift compensation:

void set_drift_compensation_reference(bool skip_next)
{
    drift_compensation_reference_hs = hs_timer_get_offset();
    drift_compensation_timestamp_hs = hs_timer_get_current_timestamp();

    if (skip_next) {
        skip_next_drift_compensation = true;
    }
}

double get_accumulated_hstimer_correction()
{
    return drift_compensation_reference_hs - hs_timer_get_offset();
}

uint64_t get_drift_measurement_duration_hs()
{
    return hs_timer_get_current_timestamp() - drift_compensation_timestamp_hs;
}

double compute_new_drift(double offset_delta, uint64_t duration)
{
    if (duration <= DRIFT_MEASUREMENT_TIME_MIN_HS) {
        // Return the previous value if the measurement period is too small
        LOG_INFO("Skipping drift measurement since measurement period is too small");
        return (1.0 / hs_timer_get_drift() ) - 1.0;
    }

    // Estimate the drift of the last round
    double measured_drift = offset_delta / (double)duration;

    // Bound the result to avoid issues based on wrong measurements
    measured_drift = MAX(MIN(measured_drift, (double)DRIFT_MAX_PPM / S_TO_US(1)), - (double)DRIFT_MAX_PPM / S_TO_US(1));
    LOG_VERBOSE("Measured drift: %6.2fppm", measured_drift * S_TO_US(1));
    return measured_drift;
}

void update_drift_compensation(double computed_drift, double min_drift, double max_drift)
{
    if (!skip_next_drift_compensation) {
        double current_drift_compensation = hs_timer_get_drift();

        // The computed drift is divided by the mean of the minimum and the maximum computed drift in the network to avoid getting faster and faster
        double avg_drift = (min_drift + max_drift) / 2;
        //LOG_VERBOSE("Drift without network: %6.2fppm", computed_drift * S_TO_US(1));
        //LOG_VERBOSE("Drift in      network: %6.2fppm", avg_drift      * S_TO_US(1));

        // Compute new drift compensation factor - divide by network drift to avoid out-of-bounds drift
        double new_drift_compensation = (1.0 + avg_drift) / (1.0 + computed_drift);

        // Avoid changing the drift compensation too fast by using a weighted sum of the new and the old value
        new_drift_compensation = DRIFT_COMPENSATION_CHANGE * new_drift_compensation + (1.0 - DRIFT_COMPENSATION_CHANGE) * current_drift_compensation;

        // Change the drift. The offset is adapted to avoid a time jump due to the modified drift
        uint64_t timestamp  = hs_timer_get_counter();
        timestamp          |= (uint64_t)hs_timer_get_counter_extension() << 32;

        hs_timer_adapt_offset((double)timestamp * (current_drift_compensation - new_drift_compensation));
        hs_timer_set_drift(new_drift_compensation);

        //LOG_VERBOSE("New      drift compensation: %f", new_drift_compensation);
    } else {
        LOG_VERBOSE("Skipped drift compensation");

        // Only skip the drift compensation once
        skip_next_drift_compensation = false;
    }

    // Initialize the variables for the next drift compensation factor update
    set_drift_compensation_reference(false);
}
