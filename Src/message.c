/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Message
 *
 * Functions related to DPP message handling / processing
 *
 */

#include "main.h"


/* Static variables ---------------------------------------------------------*/
static dpp_message_t      msg_buffer;
static event_msg_level_t  event_msg_level  = EVENT_MSG_LEVEL;
static event_msg_target_t event_msg_target = EVENT_MSG_TARGET;
static uint32_t           rcvd_msg_cnt     = 0;
static uint8_t            tx_pkt_dropped   = 0;


/* Functions -----------------------------------------------------------------*/

bool process_command(const dpp_command_t* cmd, const dpp_header_t* hdr)
{
    bool successful  = false;
    bool cfg_changed = false;

    if (!cmd) {
        return false;
    }

    switch (cmd->type) {

        case DPP_COMMAND_RESET:
        case CMD_SX1262_RESET:
            LOG_WARNING("Resetting node");
            NVIC_SystemReset();
            successful = true;
            break;
        default:
            // Unknown command -> command processing failed
            break;
    }

    /* command successfully executed? */
    if (successful) {
        LOG_INFO("Cmd 0x%x processed", cmd->type & 0xff);
        EVENT_INFO(EVENT_SX1262_CMD_EXECUTED, cmd->type & 0xff);
    } else if ( (cmd->type >> 8) == DPP_COMPONENT_ID_SX1262) {
        LOG_WARNING("Failed to process cmd %u", cmd->type & 0xff);
        EVENT_WARNING(EVENT_SX1262_INV_CMD, cmd->type & 0xff);
    }

    if (cfg_changed) {
#if NVCFG_ENABLE
        if (nvcfg_save(&config)) {
            LOG_INFO("Config saved to NV memory");
        } else {
            LOG_ERROR("Failed to save config");
        }
#endif /* NVCFG_ENABLE */
    }

    return successful;
}


/* Returns true if message is valid */
bool process_message(dpp_message_t* msg, bool rcvd_from_bolt)
{
    /* check message type, length and CRC */
    if (!ps_validate_msg(msg)) {
        LOG_ERROR("Msg with invalid len or CRC (type: %u  src: %u  len: %ub)", msg->header.type, msg->header.device_id, msg->header.payload_len);
        return false;
    }
    LOG_VERBOSE("Msg type: %u  src: %u  target: %u  len: %uB", msg->header.type, msg->header.device_id, msg->header.target_id, msg->header.payload_len);

    /* only process the message if target ID matched the node ID */
    bool forward = (msg->header.target_id == DPP_DEVICE_ID_BROADCAST);

    if ((msg->header.target_id == NODE_ID) || (msg->header.target_id == DPP_DEVICE_ID_BROADCAST)) {
        rcvd_msg_cnt++;

        if (msg->header.type == DPP_MSG_TYPE_CMD) {
            // Command successfully executed?
            return process_command(&msg->cmd, &msg->header);

#if BOLT_ENABLE
        } else if (msg->header.type == DPP_MSG_TYPE_TIMESYNC) {
            LOG_VERBOSE("Timestamp %llu rcvd", msg->timestamp);
            set_time(msg->timestamp);
            return true;
#endif /* BOLT_ENABLE */

        /* unknown message type */
        } else {
            forward = true;
        }

        /* target ID is not this node -> forward message */
    } else {
        forward = true;
    }

    /* forward the message */
    if (forward) {
        if (rcvd_from_bolt) {
            /* forward to network */
            // TODO: Handle packet queue
            tx_pkt_dropped++;
        } else {
#if BOLT_ENABLE
            /* forward to BOLT */
            uint8_t msg_len = DPP_MSG_LEN(msg);
            if (!bolt_write((uint8_t*)msg, msg_len)) {
                LOG_ERROR("Failed to write message to BOLT");
            } else {
                LOG_VERBOSE("Msg forwarded to BOLT (type: %u, len: %uB)", msg->header.type, msg_len);
            }
#endif /* BOLT_ENABLE */
        }
    }

    return false;
}

bool send_message(uint16_t recipient, dpp_message_type_t type, const uint8_t* data, uint8_t len, interface_t target)
{
    /* separate sequence number for each interface */
    static uint16_t seq_no_network = 0;
    static uint16_t seq_no_bolt    = 0;

    /* check if in interrupt context */
    if (IS_INTERRUPT()) {
        LOG_WARNING("Cannot transmit from ISR (msg of type %u dropped)", type);
        return false;
    }

    /* check message length */
    if (len > DPP_MSG_PAYLOAD_LEN) {
        LOG_WARNING("Invalid msg len (%u)", len);
        EVENT_WARNING(EVENT_SX1262_INV_MSG, ( ((uint32_t)type) << 16) | len);
        return false;
    }

    /* compose the message */
    uint8_t msg_buffer_len = ps_compose_msg(recipient, type, data, len, &msg_buffer);
    if (!msg_buffer_len) {
        return false;
    }

    /* adjust the sequence number (per interface) */
    if ((type & DPP_MSG_TYPE_MIN) == 0) {
        if (target == INTERFACE_BOLT) {
            msg_buffer.header.seqnr = seq_no_bolt++;
        } else if (target == INTERFACE_NETWORK) {
            msg_buffer.header.seqnr = seq_no_network++;
        }
        ps_update_msg_crc(&msg_buffer);
    }

    /* forward the message either to BOLT or the network */
    switch (target) {

        case INTERFACE_BOLT:
#if BOLT_ENABLE
            if (bolt_write((uint8_t*)&msg_buffer, msg_buffer_len)) {
                LOG_VERBOSE("Msg written to BOLT");
                return true;
            }
            LOG_WARNING("Msg dropped (BOLT queue full)");
#endif /* BOLT_ENABLE */
            break;

        case INTERFACE_NETWORK:
            // TODO: Handle packet queue
            tx_pkt_dropped++;
            LOG_ERROR("Msg dropped (TX queue full)");
            break;

        default:
            LOG_WARNING("Invalid target (msg dropped)");
            break;
    }

    return false;
}

void send_node_info(void)
{
    uint32_t cfg_field = 0;
    ps_compose_nodeinfo(&msg_buffer, config.rst_cnt, cfg_field);

    LOG_VERBOSE("Node info msg generated");
    send_message(DPP_DEVICE_ID_SINK, DPP_MSG_TYPE_NODE_INFO, 0, 0, INTERFACE_NETWORK);
}

void send_node_health(void)
{
    // TODO: Generate health packet
}

void send_event(event_msg_level_t level, dpp_event_type_t type, uint32_t val)
{
    if (event_msg_level < level) {
        return;
    }

    if (event_msg_target == EVENT_MSG_TARGET_UART) {
        if        (level == EVENT_MSG_LEVEL_INFO) {
            LOG_INFO(   "Event 0x%02x occurred (value: 0x%02lx)", type, val);
        } else if (level == EVENT_MSG_LEVEL_WARNING) {
            LOG_WARNING("Event 0x%02x occurred (value: 0x%02lx)", type, val);
        } else if (level == EVENT_MSG_LEVEL_ERROR) {
            LOG_ERROR(  "Event 0x%02x occurred (value: 0x%02lx)", type, val);
        } else if (level == EVENT_MSG_LEVEL_VERBOSE) {
            LOG_VERBOSE("Event 0x%02x occurred (value: 0x%02lx)", type, val);
        }
    } else {
        // Transmit event as packet
        dpp_event_t event;
        event.type  = type;
        event.value = val;

        if (event_msg_target == EVENT_MSG_TARGET_BOLT) {
#if BOLT_ENABLE
            /* do not report errors about BOLT via BOLT */
            if (type != EVENT_SX1262_BOLT_ERROR) {
                if (!send_message(DPP_DEVICE_ID_SINK, DPP_MSG_TYPE_EVENT, (uint8_t*)&event, 0, INTERFACE_BOLT)) {
                    LOG_ERROR("Failed to send event of type %u via BOLT", type);
                }
            }
#endif /* BOLT_ENABLE */
        } else if (event_msg_target == EVENT_MSG_TARGET_NETWORK) {
            if (!send_message(DPP_DEVICE_ID_SINK, DPP_MSG_TYPE_EVENT, (uint8_t*)&event, 0, INTERFACE_NETWORK)) {
                LOG_ERROR("Failed to send event of type %u via network", type);
            }
        } else {
            LOG_WARNING("Invalid event target (%u)", event_msg_target);
        }
    }
}

void set_event_level(event_msg_level_t level)
{
    event_msg_level = level;
}

void set_event_target(event_msg_target_t target)
{
    event_msg_target = target;
}
