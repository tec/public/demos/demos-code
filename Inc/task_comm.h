/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Communication structures
 *
 * This file contains the definition of the important structs.
 * It also contains the declaration of some helper functions and of the state change and slot type indicator macros.
 *
 */

#ifndef __TASK_COMM_H
#define __TASK_COMM_H


/* --- definitions --- */

/* Length of one round in number of slots */
#define NR_SLOTS_PER_ROUND        (NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) + NR_SLOTS_N + NR_SLOTS_S + NR_SLOTS_C + NR_SLOTS_D + NR_SLOTS_I)

/* Number of bytes used for the node flags */
#define NR_NODE_FLAG_BYTES        ((MAX_NR_NODES + 7) / 8)

/* Determines whether a bit is set in the node flags. */
#define IS_FLAG_SET(flags, bit)   (((flags)[((bit) - 1) / 8] &   (1 << ( ((bit) - 1) % 8) ) ) > 0)

/* Sets a bit in the node flags. */
#define SET_FLAG(flags, bit)      ( (flags)[((bit) - 1) / 8] |=  (1 << ( ((bit) - 1) % 8) ) )

/* Clear a bit in the node flags. */
#define CLEAR_FLAG(flags, bit)    ( (flags)[((bit) - 1) / 8] &= ~(1 << ( ((bit) - 1) % 8) ) )

/* Determines whether the node flags are identical. */
#define IS_SAME_FLAGS(flags1, flags2) (memcmp(flags1, flags2, NR_NODE_FLAG_BYTES) == 0)


/* --- typedefs --- */

/* Type of a node ID.
 * Note: The node ID 0 is reserved for "no node". */
typedef uint8_t node_id_t;

/* Type for storing the number of assigned slots. */
typedef uint8_t slot_count_t;

/* Type for storing the schedule, which assigns a sender to every data slot.
 * This must be instantiated as an array with NR_SLOTS_C elements. */
typedef node_id_t schedule_t;

/* Different slot types. */
typedef enum {
    SLOT_TYPE_IDLE,
    SLOT_TYPE_PROPOSAL,
    SLOT_TYPE_VOTING,
    SLOT_TYPE_NOTIFICATION,
    SLOT_TYPE_SCHEDULING,
    SLOT_TYPE_COMMUNICATION,
    SLOT_TYPE_DISCOVERY,
    NR_SLOT_TYPES
} slot_type_t;

/* Network states a node can be in. */
typedef enum {
    STATE_NETWORK_FORMATION,
    STATE_NORMAL,
    STATE_RECOVERY
} network_state_t;

/* Struct containing the information of a specific slot. */
typedef struct {
    slot_type_t type;
    node_id_t   sender;
} slot_info_t;

/* Struct containing the state of the communication task. */
typedef struct {
    // Current network state
    network_state_t network_state;
    // Current frequency band
    uint8_t         freq_band;
    // Indicates the number of slots the node would like to have
    slot_count_t    d;
    // Contains information about the next slot
    slot_info_t     slot_next;
    // Start times
    uint64_t        t_round;
    uint64_t        t_slot;
    // Schedule for communication and discovery
    schedule_t      S[NR_SLOTS_C];
    schedule_t      S_disc[NR_SLOTS_D];
    // Requests used to compute the next schedule
    int8_t          D[MAX_NR_NODES];
    int8_t          D_rcvd[MAX_NR_NODES];
    // Requests used to determine the empirical network size
    int8_t          D_tot[MAX_NR_NODES];
#if S_SCHEDULE_USE_HISTORY
    // Historical demand
    uint8_t         D_validity[MAX_NR_NODES];
    int8_t          D_hist[MAX_NR_NODES];
#endif /* S_SCHEDULE_USE_HISTORY */
    // Bit flags for node flags and votes
    uint8_t         M[NR_NODE_FLAG_BYTES];
    uint8_t         V_rcvd[NR_NODE_FLAG_BYTES];
    // Discovery information
    bool            disc_prepared;
    bool            net_discovered;
    uint8_t         id_discovered;
    uint8_t         N_d;
    uint8_t         T_d;
    uint16_t        r_d;
    uint64_t        t_d;
    uint64_t        t_d_rx;
    // Network sizes
    uint8_t         N;
    uint8_t         N_net;
    // Round state
    uint8_t         T;
    uint16_t        r;
    uint8_t         e;
    uint8_t         r_unsynced;
    uint8_t         r_recovering;
    node_id_t       p;
    node_id_t       p_voted;
    bool            l;
    bool            v;
    bool            rcvd_proposal;
    bool            is_elected;
} comm_state_t;


/* --- function prototypes --- */

/* log_flush is only defined if LOG_PRINT_IMMEDIATELY is 0.
 * It is however also needed when LOG_PRINT_IMMEDIATELY is 1; this define allows the application to compile in both cases. */
#if LOG_PRINT_IMMEDIATELY
#define log_flush()
#endif

/* Pause the execution after a fixed duration.
 *  duration: Duration to sleep. (Unit: lptimer ticks)
 */
void comm_task_sleep_lp(uint64_t duration);

/* Pause the execution until a fixed point in time.
 *  time: Time to wake up. (Unit: lptimer ticks)
 */
void comm_task_sleep_until_lp(uint64_t time);

/* Pause the execution using the hstimer until a fixed point in time.
 *  time: Time to wake up. (Unit: hstimer ticks)
 */
void comm_task_sleep_until_hs(uint64_t time);

/* Stop the sleep timer. */
void comm_task_end_sleep();

/* Pause the execution of the communication task until a callback unblocks it. */
void comm_task_block();

/* Unblock the execution of the communication task. */
void comm_task_unblock_callback();

/* Update the duty cycle counters and print statistics. */
void comm_task_update_dc();


/* --- macros --- */

/* Macros indicating the current state. */
#ifndef STATE_NETWORK_FORMATION_IND
#define STATE_NETWORK_FORMATION_IND()
#endif

#ifndef STATE_NORMAL_IND
#define STATE_NORMAL_IND()
#endif

#ifndef STATE_RECOVERY_IND
#define STATE_RECOVERY_IND()
#endif

#ifndef STATE_RECEIVING_IND
#define STATE_RECEIVING_IND()
#endif

#ifndef STATE_TRANSMITTING_IND
#define STATE_TRANSMITTING_IND()
#endif

#ifndef IS_FOLLOWING_IND
#define IS_FOLLOWING_IND()
#endif

#ifndef IS_ELECTED_IND
#define IS_ELECTED_IND()
#endif

#ifndef HAS_YIELDED_IND
#define HAS_YIELDED_IND()
#endif

/* Macro indicating that a Proposal slot is active. */
#ifndef SLOT_TYPE_PROPOSAL_ON_IND
#define SLOT_TYPE_PROPOSAL_ON_IND()
#define SLOT_TYPE_PROPOSAL_OFF_IND()
#endif

/* Macro indicating that a Voting slot is active. */
#ifndef SLOT_TYPE_VOTING_ON_IND
#define SLOT_TYPE_VOTING_ON_IND()
#define SLOT_TYPE_VOTING_OFF_IND()
#endif

/* Macro indicating that a Notification slot is active. */
#ifndef SLOT_TYPE_NOTIFICATION_ON_IND
#define SLOT_TYPE_NOTIFICATION_ON_IND()
#define SLOT_TYPE_NOTIFICATION_OFF_IND()
#endif

/* Macro indicating that a Scheduling slot is active. */
#ifndef SLOT_TYPE_SCHEDULING_ON_IND
#define SLOT_TYPE_SCHEDULING_ON_IND()
#define SLOT_TYPE_SCHEDULING_OFF_IND()
#endif

/* Macro indicating that a Communication slot is active. */
#ifndef SLOT_TYPE_COMMUNICATION_ON_IND
#define SLOT_TYPE_COMMUNICATION_ON_IND()
#define SLOT_TYPE_COMMUNICATION_OFF_IND()
#endif

/* Macro indicating that a Discovery slot is active. */
#ifndef SLOT_TYPE_DISCOVERY_ON_IND
#define SLOT_TYPE_DISCOVERY_ON_IND()
#define SLOT_TYPE_DISCOVERY_OFF_IND()
#endif

#endif /* __TASK_COMM_H */
