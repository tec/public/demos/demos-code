/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slots
 *
 * This file contains the definition of the structures used by functions implementing the different slot types.
 * It also contains the declaration of these functions and of some helper functions.
 *
 */

#ifndef __SLOTS_H
#define __SLOTS_H


/* --- definitions --- */

/* The minimum and maximum slot count which can be used in the Vote packet. */
#if S_SCHEDULE_REQUEST_DELTAS
#define SLOT_COUNT_REQ_MAX                  ((1 << (V_NR_REQ_BITS - 1) ) - 1)
#define SLOT_COUNT_REQ_MIN                  (- SLOT_COUNT_REQ_MAX)
#define SLOT_COUNT_NEUTRAL_ELEMENT          (SLOT_COUNT_REQ_MIN - 1)
#else
/* If the count is absolute, we can have up to 2^bits - 1 (neutral element) - 1 (zero) slots.
 * ATTENTION: Due to int8_t being used for computation, maximally 7 bits can be used for unsigned requests. */
#define SLOT_COUNT_REQ_MAX                  ((1 << V_NR_REQ_BITS) - 1 - 1)
#define SLOT_COUNT_NEUTRAL_ELEMENT          (-1)
#endif /* S_SCHEDULE_REQUEST_DELTAS */

/* The header size of a Communication packet */
#define C_HEADER_LENGTH                     ((1 + 1 + NR_NODE_FLAG_BYTES) * sizeof(uint8_t))

/* The number of bytes used to store the request values in the Vote packet */
#define V_PACKET_NR_REQ_BYTES               ((MAX_NR_NODES * V_NR_REQ_BITS + 7) / 8)

/* Prints the flags for an assumed network using X bytes for its flags. */
#if   NR_NODE_FLAG_BYTES == 4
#define PRINT_FLAGS(identifier, flags)      LOG_VERBOSE("Flags - %15s: %08lu | %08lu | %08lu | %08lu", identifier, BINARY(flags[3]), BINARY(flags[2]), BINARY(flags[1]), BINARY(flags[0]))
#elif NR_NODE_FLAG_BYTES == 3
#define PRINT_FLAGS(identifier, flags)      LOG_VERBOSE("Flags - %15s: %08lu | %08lu | %08lu", identifier, BINARY(flags[2]), BINARY(flags[1]), BINARY(flags[0]))
#elif NR_NODE_FLAG_BYTES == 2
#define PRINT_FLAGS(identifier, flags)      LOG_VERBOSE("Flags - %15s: %08lu | %08lu", identifier, BINARY(flags[1]), BINARY(flags[0]))
#elif NR_NODE_FLAG_BYTES == 1
#define PRINT_FLAGS(identifier, flags)      LOG_VERBOSE("Flags - %15s: %08lu", identifier, BINARY(flags[0]))
#else
#define PRINT_FLAGS(identifier, flags)
#endif /* NR_NODE_FLAG_BYTES */

#if ENABLE_SEPARATE_FREQ_BANDS
#ifndef FREQ_BANDS
#define FREQ_BANDS                          ((uint8_t[]){FRQ_BAND_NORMAL})
#endif /* FREQ_BANDS */
#endif /* ENABLE_SEPARATE_FREQ_BANDS */

#if D_USE_NETWORK_SIZE_FOR_MERGING
#define IS_DOMINATED(id_A, size_A, id_B, size_B)    ( ((size_A) < (size_B)) || ( ((size_A) == (size_B)) && ((id_A) < (id_B)) ) )
#else
#define IS_DOMINATED(id_A, size_A, id_B, size_B)    ((id_A) < (id_B))
#endif /* D_USE_NETWORK_SIZE_FOR_MERGING */

/* --- typedefs --- */

typedef enum {
    PACKET_TYPE_INVALID,
    PACKET_TYPE_PROPOSE,
    PACKET_TYPE_VOTE,
    PACKET_TYPE_SCHEDULE,
    PACKET_TYPE_COMMUNICATION,
    PACKET_TYPE_DISCOVER,
    NR_PACKET_TYPES
} packet_t;

typedef struct __attribute__((__packed__, __aligned__(1))){
    // Packet type
    uint8_t   type   : 3;
    // Source ID
    uint8_t   id_src : 5;
    // Round counter
    uint16_t  r;
    // Election counter
    uint8_t   e;
#if PROPOSAL_ADD_NETWORK_SIZE
    uint8_t   N;
#endif /* PROPOSAL_ADD_NETWORK_SIZE */
} propose_packet_t;

typedef struct __attribute__((__packed__, __aligned__(1))){
    // Packet type
    uint8_t  type   : 3;
    // Source ID
    uint8_t  id_src : 5;
    // Proposer ID
    uint8_t  p;
    // One bit for every node indicating whether the node has voted
    uint8_t  V[NR_NODE_FLAG_BYTES];
    // S_NR_REQ_BITS bits per node indicating the requests
    uint8_t  D[V_PACKET_NR_REQ_BYTES];
#if VOTING_ADD_NETWORK_SIZE
    uint8_t N_net;
#endif /* VOTING_ADD_NETWORK_SIZE */
#if VOTING_ADD_DRIFT_COMPENSATION
    // The minimum and maximum drift compensation factors computed in the network
    int16_t  drift_min;
    int16_t  drift_max;
#endif /* VOTING_ADD_DRIFT_COMPENSATION */
#if VOTING_ADD_PAYLOAD_CRC
    uint32_t crc32;
#endif /* VOTING_ADD_PAYLOAD_CRC */
} vote_packet_t;

typedef struct __attribute__((__packed__, __aligned__(1))){
    // Packet type
    uint8_t    type   : 3;
    // Source ID
    uint8_t    id_src : 5;
    uint8_t    T;
#if SCHEDULING_ADD_NETWORK_SIZE
    uint8_t    N;
#endif /* SCHEDULING_ADD_NETWORK_SIZE */
    uint8_t    M[NR_NODE_FLAG_BYTES];
    schedule_t S[S_PACKET_NR_SCHEDULE_BYTES];
} schedule_packet_t;

typedef struct __attribute__((__packed__, __aligned__(1))){
    // Packet type
    uint8_t   type   : 3;
    // Source ID
    uint8_t   id_src : 5;
    // Leader ID
    uint8_t   id_leader;
    uint8_t   M[NR_NODE_FLAG_BYTES];
    uint8_t   payload[GLORIA_INTERFACE_MAX_PAYLOAD_LEN - C_HEADER_LENGTH];
} communication_packet_t;

typedef struct __attribute__((__packed__, __aligned__(1))){
    // Packet type
    uint8_t   type   : 3;
    // Source ID
    uint8_t   id_src : 5;
    uint8_t   T;
    uint16_t  r;
    uint64_t  t;
#if DISCOVERY_ADD_NETWORK_SIZE
    uint8_t   N;
#endif /* DISCOVERY_ADD_NETWORK_SIZE */
} discover_packet_t;

/* --- function prototypes --- */

/* Prepare a new round and the execution of a Proposal slot. */
void prepare_proposal(comm_state_t* state);

/* Perform a Proposal slot. */
void run_proposal_slot(comm_state_t* state);

/* Perform a Voting slot. */
void run_voting_slot(comm_state_t* state);

/* Perform a Notification slot. */
void run_notification_slot(comm_state_t* state);

/* Perform a Scheduling slot. */
void run_scheduling_slot(comm_state_t* state);

/* Perform a Communication slot. */
void run_communication_slot(comm_state_t* state);

/* Prepare the execution of a Discovery slot. */
void prepare_discovery(comm_state_t* state);

/* Perform a Discovery slot. */
void run_discovery_slot(comm_state_t* state);

/* Perform the End of Round updates. */
void run_end_of_round(comm_state_t* state);


/* Helper functions */

/* Stores a non-repeating sequence of IDs.
 *   len:       Length of the random sequence
 */
void generate_random_seq(uint8_t len);

/* Generates a non-repeating random number within the range [1, MAX_NR_NODES].
 *   r:         Round counter
 *   e:         Election counter
 */
node_id_t get_designated_leader(uint16_t r, uint8_t e);

/* Maps the leader ID to the assigned frequency band.
 *   leader:    ID of the leader
 */
uint8_t get_designated_freq_band(node_id_t leader);

/* Extracts the timestamp of a packet.
 *   packet:    Pointer to the packet
 *   size:      Size of the packet
 */
uint64_t extract_timestamp(const uint8_t* packet, uint8_t size);

/* Computes the time offset of the sender to the local clock.
 * Only packets which contain a timestamp in the last 8 bytes are accepted.
 * It is assumed that the first byte of the packet contains a type ID (in the four least significant bits).
 * Packets with unknown Type IDs are ignored.
 *   packet:          Pointer to the packet
 *   size:            Size of the packet
 *   time_offset_hs:  Pointer to a uint64_t, where the resulting offset will be stored. (Unit: hstimer ticks)
 *
 *   Return value: 1 if successful, 0 otherwise
 *                 The time_offset won't be overwritten if 0 is returned
 */
bool get_packet_time_offset(const uint8_t* packet, uint8_t size, int64_t* time_offset_hs);

/* This function returns the request value of a specific node stored in a Vote packet.
 *   pkt:     Pointer to the Vote packet to look into.
 *   node_id: ID of the node of which the request value is returned.
 */
int8_t get_request_value(const vote_packet_t* pkt, node_id_t node_id);

/* Merge two arrays of request values.
 *  array_result: Pointer to an array of size MAX_NR_NODES which will store the results
 *  array_added:  Pointer to an array of size MAX_NR_NODES whose values should be copied
 */
bool merge_request_arrays(int8_t* array_result, const int8_t* array_added);

/* Returns the number of flags which are either initialized or set. */
uint8_t get_nr_of_contributors(const vote_packet_t* pkt);
uint8_t get_nr_of_set_flags(const uint8_t* flags);

/* Updates the network size depending on the network and temporal filter. */
uint16_t update_network_size(uint16_t N, uint16_t N_net, uint16_t N_empirical);

/* Checks if the given flags contain only zeros.
 *   flags: The flags to check
 */
bool is_flags_empty(const uint8_t* flags);

#endif /* __SLOTS_H */
