/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/*
 * Demos config
 *
 */

#ifndef __APP_CONFIG_H
#define __APP_CONFIG_H


/*** General Parameters **************************************************/

#define FW_NAME                                 "DPP2Demo"  /* max. 8 chars */
#define FW_VERSION_MAJOR                        1           /* 0..6 */
#define FW_VERSION_MINOR                        0           /* 0..99 */
#define FW_VERSION_PATCH                        0           /* 0..99 */

/* This define indicates whether the code is running on FlockLab or not. */
#define FLOCKLAB                                1

/* Set the Node ID. */
#if FLOCKLAB
#define NODE_ID                                 ((uint8_t)FLOCKLAB_NODE_ID)
#define DEMOS_FL_NODE_LIST                      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 19, 20, 21, 22, 23, 24, 26, 27, 28, 29, 31, 32  /* nodes to use on FlockLab */
#else /* FLOCKLAB */
#define NODE_ID                                 23
#endif /* FLOCKLAB */

/* Maximum number of nodes (<= 2⁵ due to ID in packets) */
#define MAX_NR_NODES                            24

/* Low-power mode to use between rounds during periods of inactivity */
#define LOW_POWER_MODE                          LP_MODE_STOP2

/* Set to 1 to disable GPIO clocks in low-power mode (-> no GPIO tracing possible). */
#define LPM_DISABLE_GPIO_CLOCKS                 0

/* Random seeds */
#define SEED_RAND_SEQUENCE                      831
#define SEED_RAND_DISCOVERY                     143
#ifndef HAL_RNG_MODULE_ENABLED
#define SEED_RAND_PROTOCOL                      (12345 * NODE_ID + hs_timer_get_counter())
#endif /* HAL_RNG_MODULE_ENABLED */


/*** Evaluation **************************************************/

/* If DROP_NODE_PACKETS_ENABLE is true, then all packets which fulfill DROP_NODE_PACKETS_FROM_ID are dropped with probability DROP_NODE_PACKETS_PROBABILITY when DROP_NODE_PACKETS_PIN_VALUE is high. */
#define DROP_NODE_PACKETS_ENABLE                0
#define DROP_NODE_PACKETS_PIN_VALUE             PIN_GET(FLOCKLAB_SIG1)
#define DROP_NODE_PACKETS_PROBABILITY           0
#define DROP_NODE_PACKETS_FROM_ID(x)            (true)

/* If CHANGE_TX_POWER_ENABLE is true, then the TX power is set depending on whether CHANGE_TX_POWER_PIN_VALUE is high or low.
 *  TX_POWER_NORMAL:               TX power when CHANGE_TX_POWER_PIN is low
 *  CHANGE_TX_POWER_PIN_HIGH:      TX power when CHANGE_TX_POWER_PIN is high
 */
#define CHANGE_TX_POWER_ENABLE                  0
#define CHANGE_TX_POWER_PIN_VALUE               PIN_GET(FLOCKLAB_SIG1)
#define CHANGE_TX_POWER_PIN_HIGH                RADIO_MIN_POWER

/* If CHANGE_FRQ_BAND_ENABLE is true, then the used radio band is set depending on whether CHANGE_FRQ_BAND_PIN_VALUE is high or low.
 *  FRQ_BAND_NORMAL:               Radio band when CHANGE_FRQ_BAND_PIN is low
 *  CHANGE_FRQ_BAND_PIN_HIGH:      Radio band when CHANGE_FRQ_BAND_PIN is high
 */
#define CHANGE_FRQ_BAND_ENABLE                  0
#define CHANGE_FRQ_BAND_PIN_VALUE               PIN_GET(FLOCKLAB_SIG1)
#define CHANGE_FRQ_BAND_PIN_HIGH                48

/* If CHANGE_ROUND_PERIOD_ENABLE is true, then the used round period of the leader (Unit: slots) is set depending on whether CHANGE_ROUND_PERIOD_PIN_VALUE is high or low.
 *  CHANGE_ROUND_PERIOD_PIN_LOW:   Round period when CHANGE_ROUND_PERIOD_PIN is low
 *  CHANGE_ROUND_PERIOD_PIN_HIGH:  Round period when CHANGE_ROUND_PERIOD_PIN is high
 */
#define CHANGE_ROUND_PERIOD_ENABLE              0
#define CHANGE_ROUND_PERIOD_PIN_VALUE           PIN_GET(FLOCKLAB_SIG1)
#define CHANGE_ROUND_PERIOD_PIN_LOW             100
#define CHANGE_ROUND_PERIOD_PIN_HIGH            150

/* If DELAY_START_ENABLE is true, then the node will wait upon startup until the DELAY_START_PIN_VALUE is low. */
#define DELAY_START_ENABLE                      0
#define DELAY_START_PIN_VALUE                   PIN_GET(FLOCKLAB_SIG1)

/* The network formation will be skipped if this evaluates to true, and the node starts in STATE_NORMAL. */
#define SKIP_NETWORK_FORMATION                  0

/* If SKIP_NETWORK_FORMATION and NODE_AS_LEADER is set, then the node with ID NODE_AS_LEADER is set as leader. */
#define NODE_AS_LEADER                          2

/* Upon start-up, the node waits for this many lptimer ticks before continuing.
 * This can for example be used to delay the start-up such that the start is also covered for GPIO tracing.
 * This sleep is performed before waiting for DELAY_START_PIN_VALUE. */
#define STARTUP_SLEEP_LP                        LPTIMER_MS_TO_TICKS(1000)


/*** Communication Parameters **************************************************/

/* E: Number of elections per round */
#define NR_ELECTIONS_PER_ROUND                  2

/* Number of Proposal slots per election */
#define NR_SLOTS_P                              1

/* Number of Voting slots per election */
#define NR_SLOTS_V                              5

/* Number of Notification slots per round */
#define NR_SLOTS_N                              1

/* Number of Scheduling slots per round */
#define NR_SLOTS_S                              1

/* Number of Communication slots per round */
#define NR_SLOTS_C                              30

/* Number of Discovery slots per round */
#if CHANGE_ROUND_PERIOD_ENABLE
#define NR_SLOTS_D                              (MAX(CHANGE_ROUND_PERIOD_PIN_LOW, CHANGE_ROUND_PERIOD_PIN_HIGH) - NR_ELECTIONS_PER_ROUND * (NR_SLOTS_P + NR_SLOTS_V) - NR_SLOTS_N - NR_SLOTS_S - NR_SLOTS_C - NR_SLOTS_I)
#else
#define NR_SLOTS_D                              56
#endif /* CHANGE_ROUND_PERIOD_ENABLE */

/* Number of Idle slots per round */
#define NR_SLOTS_I                              0

/* R_u: Number of times no connectivity to a network must have been determined to switch to recovery */
#define NR_ROUNDS_TO_UNSYNC                     (MAX_NR_NODES/3)

/* R_r: Number of times no connectivity to a network must have been determined to switch to network formation */
#define NR_ROUNDS_TO_RECOVER                    3


/* The radio modulation, transmit power (in dBm), and frequency band that are used by the protocol (see radio_constants.c in the Flora lib). */
#define USED_RADIO_MODULATION                   10

#define TX_POWER_NORMAL                         (7)
#define TX_POWER_DISCOVERY                      TX_POWER_NORMAL

#if CHANGE_TX_POWER_ENABLE
#define USED_RADIO_TX_POWER                     (CHANGE_TX_POWER_PIN_VALUE ? CHANGE_TX_POWER_PIN_HIGH : TX_POWER_NORMAL)
#else
#define USED_RADIO_TX_POWER                     TX_POWER_NORMAL
#endif /* CHANGE_TX_POWER_ENABLE */


/* Whether separate frequency band depending on the leader are enabled */
#define ENABLE_SEPARATE_FREQ_BANDS              (!SKIP_NETWORK_FORMATION)

#if ENABLE_SEPARATE_FREQ_BANDS
#define FREQ_BANDS                              ((uint8_t[]){16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42})
#endif /* ENABLE_SEPARATE_FREQ_BANDS */

#define FRQ_BAND_NORMAL                         51
#define FRQ_BAND_DISCOVERY                      49

#if CHANGE_FRQ_BAND_ENABLE
#define USED_RADIO_FRQ_BAND(x)                  (CHANGE_FRQ_BAND_PIN_VALUE ? CHANGE_FRQ_BAND_PIN_HIGH : (x))
#else
#define USED_RADIO_FRQ_BAND(x)                  FRQ_BAND_NORMAL
#endif /* CHANGE_FRQ_BAND_ENABLE */


/* Slot duration. (Unit: lptimer ticks) */
#define SLOT_DURATION_LP                        LPTIMER_MS_TO_TICKS(30)

/* Minimum number of data slots that the node requests */
#define RQ_NR_DATA_SLOTS_MIN                    1

/* Maximum number of data slots that the node requests */
#define RQ_NR_DATA_SLOTS_MAX                    1

/* The payload size of the Communication packet. (Unit: bytes) */
#define C_PAYLOAD_LENGTH                        (18 - C_HEADER_LENGTH)

/* Number of bits to compress. Notice that ID 0 is reserved for an unallocated slot. */
#define S_SCHEDULE_COMPRESSION_BITS             5

/* Number of bytes used when distributing the schedule */
#define S_PACKET_NR_SCHEDULE_BYTES              (S_SCHEDULE_COMPRESS ? ( (NR_SLOTS_C * S_SCHEDULE_COMPRESSION_BITS + 7) / 8) : NR_SLOTS_C)

/* alpha: Parameter for updating the network size based on a network filter. (Unit: %) */
#define PARAM_FILTER_NETWORK                    50

/* beta: Parameter for updating the network size based on a temporal filter. (Unit: %) */
#define PARAM_FILTER_TEMPORAL                   33

/* gamma_o: minimal success rate of one-to-all. (Unit: %) */
/* gamma_a: minimal success rate of all-to-all. (Unit: %) */
#if   MAX_NR_NODES == 24
#define PARAM_SUCCESS_O2A                       90
#define PARAM_SUCCESS_A2A                       90
#elif MAX_NR_NODES == 18
#define PARAM_SUCCESS_O2A                       90
#define PARAM_SUCCESS_A2A                       90
#elif MAX_NR_NODES == 12
#define PARAM_SUCCESS_O2A                       90
#define PARAM_SUCCESS_A2A                       90
#else
#define PARAM_SUCCESS_O2A                       90
#define PARAM_SUCCESS_A2A                       90
#endif /* MAX_NR_NODES */

/* Minimal fraction of positive votes to be elected */
#define V_ELECT_POS_FRAC_THRESHOLD              ( ( (double)100 * 100 * 100) / ((double)2 * PARAM_SUCCESS_O2A * PARAM_SUCCESS_A2A) )


/* Whether to filter packets from the wrong network */
#define FILTER_PACKETS_FROM_WRONG_NETWORK       1

/* Whether to filter packets from the correct network but from the wrong sender */
#define FILTER_PACKETS_FROM_WRONG_SENDER        1

/* Whether to include the network size in the Propose packet */
#define PROPOSAL_ADD_NETWORK_SIZE               1

/* Whether to include the network size in the Vote packet */
#define VOTING_ADD_NETWORK_SIZE                 1

/* Whether to skip the remaining elections in case the node has voted */
#define V_SKIP_ELECTION_AFTER_VOTING            1

/* Whether to skip the remaining elections in case the majority of votes has been cast */
#define V_SKIP_ELECTION_AFTER_MAJORITY_VOTING   0

/* Whether an elected leader should yield if it encounters a new proposal */
#define V_YIELD_AFTER_NEW_PROPOSAL              0

/* Whether a designated leader should yield if it encounters cast votes. If set, the percentage of cast votes to yield. (Unit: %) */
#define V_YIELD_AFTER_CAST_VOTES                50

/* Whether a node can still commit and cast a vote after reception */
#define V_ENABLE_LATE_COMMIT                    1

/* Whether electing a leader should be based on the majority of the estimated network size (Q_a) */
#define V_ELECT_BASED_ON_MAJORITY               0

/* Whether electing a leader should be based on the number of positive votes received (part of Q_a+) */
#define V_ELECT_BASED_ON_POSITIVE_VOTES_NUM     0

/* Whether electing a leader should be based on the fraction of positive votes received (Q_r) */
#define V_ELECT_BASED_ON_POSITIVE_VOTES_FRAC    1

/* Whether to include the network size in the Schedule packet */
#define SCHEDULING_ADD_NETWORK_SIZE             1

/* Whether the requests denote changes to the schedule or an absolute number of desired slots */
#define S_SCHEDULE_REQUEST_DELTAS               0

/* Whether to compress the schedule to a maximum number of bits per node */
#define S_SCHEDULE_COMPRESS                     1

/* Whether historical values should be used. If set, the number of rounds for which a demand is considered valid. (Unit: rounds) */
#define S_SCHEDULE_USE_HISTORY                  10

/* Whether to include the network size in the Discover packet */
#define DISCOVERY_ADD_NETWORK_SIZE              D_USE_NETWORK_SIZE_FOR_MERGING

/* Whether the order in which the slots in the Discovery schedule are distributed is randomized or round-robin */
#define D_RANDOMIZE_DISCOVERY_ORDER             1

/* Whether the best received network should be forwarded. If not set, the last discovered network is forwarded. */
#define D_NOTIFY_ABOUT_BEST_DISCOVERED_NET      1


/*** Task: Communication **************************************************/

/* If the time left to wait until the next slot is bigger than this constant, then the lptimer is used to time the wakeup. (Unit: lptimer ticks) */
#define SLEEP_THRESHOLD_LP                      LPTIMER_MS_TO_TICKS(1)

/* A busy loop is used instead of the hstimer if the amount of time to sleep is smaller than this constant. (Unit: hstimer ticks) */
#define SLEEP_THRESHOLD_HS                      HS_TIMER_US_TO_TICKS(500)

/* If the time left until the next used slot is bigger than this constant, then the log is flushed. (Unit: lptimer ticks) */
#define LOG_FLUSH_THRESHOLD_LP                  LPTIMER_MS_TO_TICKS(200)

/* Print a warning in case flushing got dangerously close to not finishing in time. (Unit: lptimer ticks) */
#define LOG_FLUSH_WARN_THRESHOLD_LP             LPTIMER_MS_TO_TICKS(50)

/* Time reserved to determine the information of the next slot. (Unit: lptimer ticks) */
#define MAX_SCHEDULE_LOOKUP_TIME_LP             LPTIMER_US_TO_TICKS(100)


/*** Protocol: Discovery *************************************************/

/* Number of successive beacons per slot */
#define D_NR_BEACONS_PER_SLOT                   3

/* Additional clock offset for aligning clocks upon discovery. (Unit: lptimer ticks) */
#define D_TX_DELAY_LP(retrans_idx)              (LPTIMER_US_TO_TICKS(50))

/* Probability that a node will transmit during discovery. (Unit: %) */
#define D_PROBABILITY_TRANSMIT                  50

/* Probability that a node will listen during discovery. (Unit: %) */
#define D_PROBABILITY_LISTEN                    50

/* Additional slot duration compared to normal Glossy slots. (Unit: lptimer ticks) */
#define D_EXTRA_LISTEN_LP                       LPTIMER_US_TO_TICKS(4800)

/* Whether the timestamps of discovered networks should be printed to the console. The log level needs to be LOG_LEVEL_VERBOSE. */
#define D_LOG_TIMESTAMPS                        0

/* Whether the network size is included in deciding whether a network dominates. */
#define D_USE_NETWORK_SIZE_FOR_MERGING          0


/*** Protocol: Recovery **************************************************/

/* Permit to also recover during the Scheduling slot. */
#define ENABLE_RECOVERY_ON_SCHEDULING           1


/*** Protocol: Gloria **************************************************/

/* Maximum number of retransmissions used in a Gloria flood */
#define GLORIA_MAX_RETRANSMISSIONS              3

/* Maximum duration of a Gloria flood. (Unit: lptimer ticks) */
#define GLORIA_MAX_DURATION_LP                  LPTIMER_MS_TO_TICKS(20)

/* Duration which a sender should wait before broadcasting. (Unit: lptimer ticks) */
#define GLORIA_GUARD_TIME_LP                    LPTIMER_US_TO_TICKS(250)

/* Maximum time required to prepare the Scheduling slot. (Unit: lptimer ticks) */
#define S_INIT_TIME_LP                          LPTIMER_MS_TO_TICKS(1)


/* The radio modulation, transmit power (in dBm), and frequency band that are used by Gloria (see radio_constants.c in the Flora lib). */
#define GLORIA_INTERFACE_MODULATION             USED_RADIO_MODULATION
#define GLORIA_INTERFACE_POWER                  TX_POWER_NORMAL
#define GLORIA_INTERFACE_RF_BAND                FRQ_BAND_NORMAL

/* Whether to append a timestamp at the end of every Gloria packet. This is necessary to synchronize and determine the clock offset of discovered neighbours. */
#define GLORIA_INTERFACE_APPEND_TIMESTAMP       1

/* When stopping Gloria, wait for the last transmission to finish. */
#define GLORIA_INTERFACE_WAIT_TX_FINISHED       1


/*** Protocol: Chaos **************************************************/

/* The number of bits used to encode the request of assigned number of slots of a node.
 * Needs to be at least 2 and at most 8. One bit will be used for the sign. */
#define V_NR_REQ_BITS                           2

/* Whether the request value of all the nodes should be printed to the console. The log level needs to be LOG_LEVEL_VERBOSE. */
#define VOTING_LOG_REQUEST_VALUES               0

/* Whether drifts should be exchanged in the network */
#define VOTING_ADD_DRIFT_COMPENSATION           0

/* Whether an additional CRC32 should be added to detect corrupted requests */
#define VOTING_ADD_PAYLOAD_CRC                  1


/* The radio modulation, transmit power (in dBm), and frequency band that are used by Chaos (see radio_constants.c in the Flora lib). */
#define CHAOS_INTERFACE_MODULATION              USED_RADIO_MODULATION
#define CHAOS_INTERFACE_POWER                   TX_POWER_NORMAL
#define CHAOS_INTERFACE_RF_BAND                 FRQ_BAND_NORMAL

/* Maximum duration of one Chaos aggregation. (Unit: lptimer ticks) */
#define CHAOS_DURATION_MAX_LP                   LPTIMER_MS_TO_TICKS(125)

/* Time reserved to prepare for the Chaos slot. (Unit: hstimer ticks) */
#define CHAOS_SLOT_START_OFFSET_HS              HS_TIMER_MS_TO_TICKS(5)

/* Sending period of Chaos messages inside a Chaos slot. (Unit: hstimer ticks) */
#define CHAOS_MESSAGE_INTERVAL_HS               HS_TIMER_US_TO_TICKS(3500)

/* Minimum time left to restart the receiver if a message was discarded. (Unit: hstimer ticks) */
#define CHAOS_RADIO_MIN_RECEIVE_TIME_HS         HS_TIMER_US_TO_TICKS(1800)

/* Duration a node should wait for a message in STATE_NORMAL before giving up. (Unit: hstimer ticks) */
#define CHAOS_RADIO_RECEIVE_TIMEOUT_HS          HS_TIMER_US_TO_TICKS(2000)

/* Duration a node should wait for a message in STATE_RECOVERY before giving up. (Unit: hstimer ticks) */
#define CHAOS_RADIO_RECEIVE_TIMEOUT_LONG_HS     LPTIMER_TICKS_TO_HS_TIMER(CHAOS_DURATION_MAX_LP / 4)

/* Time reserved to prepare for the next slot. (Unit: hstimer ticks) */
#define CHAOS_RADIO_TIME_BUFFER_HS              HS_TIMER_US_TO_TICKS(350)

/* Duration a node should start receiving earlier than it would start sending. (Unit: hstimer ticks) */
#define CHAOS_RADIO_RECEIVE_OFFSET_HS           HS_TIMER_US_TO_TICKS(25)


/*** Drift Compensation **************************************************/

/* Maximum drift which is still considered correct */
#define DRIFT_MAX_PPM                           40

/* When updating the drift compensation factor, not the calculated value "comp_value" is taken directly, but rather
 * new_value = DRIFT_COMPENSATION_CHANGE * comp_value + (1 - DRIFT_COMPENSATION_CHANGE) * old_value. */
#define DRIFT_COMPENSATION_CHANGE               0.25

/* A drift measurement is only considered valid if the measurement duration is bigger than this constant. (Unit: hstimer ticks) */
#define DRIFT_MEASUREMENT_TIME_MIN_HS           (NR_SLOTS_C * LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP))

/* The minimum and maximum observed network drift are included into the Vote packet in order to be able to prevent
 * the drift of the network to consistently drift in one direction due to feedback loops.
 * The computed drift compensation is included as a difference from one.
 * This value is then multiplied by this constant and converted to a 16-bit integer to decrease the size of the Vote packet. */
#define V_PACKET_DRIFT_SCALE                    100000000

/* Whether the Voting slots should be used as a time reference */
#define V_USE_AS_REFERENCE                      0

/* Whether the Notification slot should be used as a time reference */
#define N_USE_AS_REFERENCE                      0

/* Whether the Communication slots should be used as a time reference */
#define C_USE_AS_REFERENCE                      0


/*** Memory **************************************************/

/* The stack size of the individual tasks. (Unit: 4-byte words) */
#define BOLT_TASK_STACK_SIZE                    256
#define COMM_TASK_STACK_SIZE                    512

/* Maximal number of messages to read from BOLT at once */
#define BOLT_MAX_READ_COUNT                     100


/*** Flora Library **************************************************/

/* Enable the drift compensation, which is required for the time synchronisation. */
#define HS_TIMER_COMPENSATE_DRIFT               1

/* Do not initialise the hstimer from the RTC. */
#define HS_TIMER_INIT_FROM_RTC                  0

/* Reset the watchdog upon a lptimer counter overflow and when the lptimer expires. */
#define LPTIMER_RESET_WDG_ON_OVF                1
#define LPTIMER_RESET_WDG_ON_EXP                1
#define LPTIMER_CHECK_EXP_TIME                  1

/* Not used, set to 1 byte to reduce memory usage. */
#define UART_FIFO_BUFFER_SIZE                   1

/* Disable the command line interface since it is not used. */
#define CLI_ENABLE                              0

/* Disable logging using SWO. */
#define SWO_ENABLE                              0

/* Disable the BOLT interface. */
#define BOLT_ENABLE                             0

/* Use a software timeout instead of the ones of the radio chip. */
#define RADIO_USE_HW_TIMEOUT                    0

/* Do not check the compiler version */
#define FLORA_COMPILER_VER_CHECK                0


/*** Logging **************************************************/

/* If true, some debug logs are printed during the operation. */
#define LOG_ENABLE                              1

/* Parameter indicating how verbose the logging should be
 * Possible values:
 *   LOG_LEVEL_VERBOSE
 *   LOG_LEVEL_INFO
 *   LOG_LEVEL_WARNING
 *   LOG_LEVEL_ERROR
 *   LOG_LEVEL_QUIET
 */
#define LOG_LEVEL                               LOG_LEVEL_VERBOSE

/* Whether the debug output should be printed immediately (using UART) or
 * whether it should be buffered and then later be printed by the debug task to avoid causing timing issues. */
#define LOG_PRINT_IMMEDIATELY                   0

/* Size of the buffer used store the debug output before printing it. This is only used if LOG_PRINT_IMMEDIATELY is zero. */
#define LOG_BUFFER_SIZE                         32768

/* Maximum logging time until the log is discarded */
#ifdef LOG_FLUSH_THRESHOLD_LP
#define UART_TX_TIMEOUT_MS                      LPTIMER_TICKS_TO_MS(LOG_FLUSH_THRESHOLD_LP)
#endif


/*** FlockLab **************************************************/
#if FLOCKLAB

/* Trace the occurrence of interrupts */
//#define ISR_ON_IND()                          PIN_TOGGLE(FLOCKLAB_INT1)
//#define ISR_OFF_IND()                         PIN_TOGGLE(FLOCKLAB_INT1)

/* Trace sending and receiving data */
#define RADIO_TX_START_IND()                    PIN_SET(FLOCKLAB_LED3)
#define RADIO_TX_STOP_IND()                     PIN_CLR(FLOCKLAB_LED3)
#define RADIO_RX_START_IND()                    PIN_SET(FLOCKLAB_LED2)
#define RADIO_RX_STOP_IND()                     PIN_CLR(FLOCKLAB_LED2)

/* Trace the network state of the node */
#define FL_TRACE_NETWORK_STATE                  0
#define FL_TRACE_SCHEDULING                     0
#define FL_TRACE_ELECTION                       0

#if FL_TRACE_NETWORK_STATE
#define STATE_NETWORK_FORMATION_IND()           PIN_SET(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define STATE_NORMAL_IND()                      PIN_SET(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define STATE_RECOVERY_IND()                    PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#elif FL_TRACE_SCHEDULING
#define STATE_NETWORK_FORMATION_IND()           PIN_SET(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define STATE_RECEIVING_IND()                   PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define STATE_TRANSMITTING_IND()                PIN_SET(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#elif FL_TRACE_ELECTION
#define IS_FOLLOWING_IND()                      PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define IS_ELECTED_IND()                        PIN_SET(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define HAS_YIELDED_IND()                       PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#else
#define SLOT_TYPE_PROPOSAL_ON_IND()             PIN_SET(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_VOTING_ON_IND()               PIN_SET(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_NOTIFICATION_ON_IND()         PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define SLOT_TYPE_SCHEDULING_ON_IND()           PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define SLOT_TYPE_COMMUNICATION_ON_IND()        PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define SLOT_TYPE_DISCOVERY_ON_IND()            PIN_SET(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define SLOT_TYPE_PROPOSAL_OFF_IND()            PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_VOTING_OFF_IND()              PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_NOTIFICATION_OFF_IND()        PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_SCHEDULING_OFF_IND()          PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_COMMUNICATION_OFF_IND()       PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_DISCOVERY_OFF_IND()           PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#endif /* FL_TRACE_NETWORK_STATE */

#endif /* FLOCKLAB */


/*** Parameter Checks **************************************************/

#if BOLT_ENABLE && (BOLT_MAX_MSG_LEN < DPP_MSG_PKT_LEN)
#error "BOLT_MAX_MSG_LEN is too small"
#endif

#if V_NR_REQ_BITS < 2
#error "V_NR_REQ_BITS needs to be at least 2"
#endif

#if V_NR_REQ_BITS > 8
#error "V_NR_REQ_BITS can be at most 8"
#endif

#if !S_SCHEDULE_REQUEST_DELTAS && (V_NR_REQ_BITS > 7)
#error "Exceed maximum number of request bits for unsigned requests"
#endif

#if !S_SCHEDULE_REQUEST_DELTAS && (RQ_NR_DATA_SLOTS_MIN > ( (1 << V_NR_REQ_BITS) - 1 - 1) )
#error "Cannot include minimum number of slots in the absolute requests"
#endif

#if !S_SCHEDULE_REQUEST_DELTAS && (RQ_NR_DATA_SLOTS_MAX > ( (1 << V_NR_REQ_BITS) - 1 - 1) )
#warning "Cannot include maximum number of slots in the absolute requests"
#endif

#if (DROP_NODE_PACKETS_ENABLE + CHANGE_TX_POWER_ENABLE + CHANGE_FRQ_BAND_ENABLE + CHANGE_ROUND_PERIOD_ENABLE + DELAY_START_ENABLE) > 1
#error "Multiple usages defined for SIG1"
#endif

#if S_SCHEDULE_COMPRESS && ( ( (1 << S_SCHEDULE_COMPRESSION_BITS) - 1) < MAX_NR_NODES)
#error "Cannot fit all node IDs for MAX_NR_NODES into the compressed schedule"
#endif

#if (V_ELECT_BASED_ON_MAJORITY + V_ELECT_BASED_ON_POSITIVE_VOTES_FRAC + V_ELECT_BASED_ON_POSITIVE_VOTES_NUM) < 1
#error "Must define at least one quorum"
#endif

#ifndef HAL_RNG_MODULE_ENABLED
//#warning "Randomness might not be working correctly with the current newlib version"
#endif

#endif /* __APP_CONFIG_H */
