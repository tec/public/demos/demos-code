/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Time utilities
 *
 * This file contains helper functions concerning clocks and timer calculations.
 *
 */

#ifndef __TIME_UTILITIES_H
#define __TIME_UTILITIES_H


/* --- definitions --- */

/* Conversions */
#define S_TO_MS(t)                  ((t) * 1000)
#define S_TO_US(t)                  ((t) * 1000000)
#define MS_TO_S(t)                  ((t) / 1000)
#define US_TO_S(t)                  ((t) / 1000000)

#define PPM_TO_HUNDREDS_OF_PERC(x)  ((x) / 100)  // convert from PPM to 10⁻² * %

/* Minimum drift correction factor */
#ifndef DRIFT_COMPENSATION_MIN
#define DRIFT_COMPENSATION_MIN      0.999995
#endif

/* Maximum drift correction factor */
#ifndef DRIFT_COMPENSATION_MAX
#define DRIFT_COMPENSATION_MAX      1.000005
#endif

/* When updating the drift compensation factor, not the calculated value "comp_value" is taken but rather
 * new_value = DRIFT_COMPENSATION_CHANGE * comp_value + (1 - DRIFT_COMPENSATION_CHANGE) * old_value. */
#ifndef DRIFT_COMPENSATION_CHANGE
#define DRIFT_COMPENSATION_CHANGE   0.1
#endif

/* A drift measurement is only considered if the measurement duration is bigger than this constant. */
#ifndef DRIFT_MEASUREMENT_TIME_MIN_HS
#define DRIFT_MEASUREMENT_TIME_MIN_HS          0
#endif

#ifndef DRIFT_THRESHOLD_INFO_LP
#define DRIFT_THRESHOLD_INFO_LP                LPTIMER_US_TO_TICKS(50)
#endif

#ifndef DRIFT_THRESHOLD_WARN_LP
#define DRIFT_THRESHOLD_WARN_LP                LPTIMER_US_TO_TICKS(75)
#endif

#ifndef SYNC_THRESHOLD_GLORIA_INFO_HS
#define SYNC_THRESHOLD_GLORIA_INFO_HS          HS_TIMER_US_TO_TICKS(5)
#endif

#ifndef SYNC_THRESHOLD_GLORIA_WARN_HS
#define SYNC_THRESHOLD_GLORIA_WARN_HS          HS_TIMER_US_TO_TICKS(10)
#endif

#ifndef SYNC_THRESHOLD_CHAOS_INFO_HS
#define SYNC_THRESHOLD_CHAOS_INFO_HS           HS_TIMER_US_TO_TICKS(10)
#endif

#ifndef SYNC_THRESHOLD_CHAOS_WARN_HS
#define SYNC_THRESHOLD_CHAOS_WARN_HS           HS_TIMER_US_TO_TICKS(50)
#endif

/* --- function prototypes --- */

/* Adjust the timers so that the dependent time values stay constant. */
void adjust_timers(uint64_t* t_slot, uint64_t* t_round, uint8_t T_prev, uint8_t T_new);

// Lptimer offset compensation:

/* Set the lptimer clock offset in the comm_state to match the hstimer. */
void update_clock_offset();

/* Get the current lptimer value with the clock offset corrected. */
uint64_t lptimer_now_corrected();


// Time unit conversations:

/* Convert a corrected lptimer timestamp to the corresponding uncorrected lptimer timestamp. */
uint64_t corrected_to_uncorrected_lp_timestamp(uint64_t lp_timestamp);


// Drift compensation:

/* Stores the reference necessary to compute the time drift.
 *   skip_next: Whether the next drift compensation update will be skipped
 */
void set_drift_compensation_reference(bool skip_next);

/* Returns how much the hstimer offset has changed since the last call of set_drift_compensation_reference.
 * 0 is returned if the measurement was cleared using set_drift_compensation_reference(true). */
double get_accumulated_hstimer_correction();

/* Get the time which elapsed since the last time set_drift_compensation_reference was called. (Unit: hstimer ticks) */
uint64_t get_drift_measurement_duration_hs();

/* Computes the new drift. The estimated clock drift is bounded by DRIFT_MAX_PPM.
 * If duration is smaller than DRIFT_MEASUREMENT_TIME_MIN_HS, then the current drift compensation value is returned.
 *   offset:          Observed clock offset
 *   duration:        Time duration over which the clock offset was observed
 *
 *   Return value:    Estimated drift value */
double compute_new_drift(double offset, uint64_t duration);

/* Updates the drift compensation factor used in the hstimer.
 * The new drift compensation factor is the computed one multiplied by the average of the minimum and the maximum computed drift present in the network.
 * The new drift compensation factor will be smoothed using the previous drift compensation factor.
 *   computed_drift:  New drift compensation factor computed in this round
 *   drift_min:       Minimum computed drift compensation factor present in the current network
 *   drift_max:       Maximum computed drift compensation factor present in the current network
 *
 * Note: The first drift compensation update will be skipped since the related measurement might be inaccurate.
 * The last drift estimation will also be cleared in this case for the same reason. */
void update_drift_compensation(double computed_drift, double drift_min, double drift_max);

#endif /* __TIME_UTILITIES_H */
